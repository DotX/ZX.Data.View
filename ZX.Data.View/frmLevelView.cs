﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZX.Data.View
{
    public partial class frmLevelView : Form
    {
        SaveHelper SaveHelper;

        public frmLevelView()
        {
            InitializeComponent();
        }

        public static void Show(SaveHelper saveHelper)
        {
            var frm = new frmLevelView();

            frm.SaveHelper = saveHelper;
            frm.Add_Tab();
            frm.Show();
        }

        private void Add_Tab(string propertyPath = "")
        {
            var name = propertyPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault() ?? "";
            var hashKey = propertyPath.GetHashCode().ToString();

            var tbIndex = this.TC.TabPages.IndexOfKey(hashKey);

            if (tbIndex == -1)
            {
                this.TC.TabPages.Add(hashKey, name);
                var tp = this.TC.TabPages[hashKey];
                tp.Tag = propertyPath;

                var p = this.SaveHelper.GetProperty(propertyPath);
                var dt = this.SaveHelper.GetChildDataTable(p);
                var dgv = new DataGridView()
                {
                    DataSource = dt,
                    RowHeadersVisible = false,
                    Dock = DockStyle.Fill,
                    AllowUserToOrderColumns = false,
                    AllowUserToAddRows = false,
                    AllowUserToDeleteRows = false,
                    ReadOnly = true
                };

                typeof(DataGridView).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)?.SetValue(dgv, true, null);

                dgv.DataBindingComplete += Dgv_DataBindingComplete;
                dgv.CellDoubleClick += Dgv_CellDoubleClick;
                dgv.KeyUp += this.Dgv_KeyUp;
                tp.Controls.Add(dgv);

                tbIndex = this.TC.TabPages.IndexOfKey(hashKey);
            }
            this.TC.SelectedIndex = tbIndex;
        }

        private void Dgv_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                OpenPath(sender as DataGridView);
            }
        }

        private void Dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OpenPath(sender as DataGridView);
        }

        private void OpenPath(DataGridView dgv)
        {
            if (dgv != null && dgv.CurrentCell.RowIndex >= 0)
            {
                var rIndex = dgv.CurrentCell.RowIndex;
                var name = string.Format("{0}", dgv[0, rIndex].Value);
                var propertyType = string.Format("{0}", dgv[1, rIndex].Value);

                var tp = this.TC.SelectedTab;

                if (tp != null)
                {
                    var path = string.Format("{0}", tp.Tag);

                    switch (propertyType)
                    {
                        case "ComplexProperty":
                        case "CollectionProperty":
                        case "DictionaryProperty":
                            this.Add_Tab(path + @"\" + name);
                            break;
                    }
                }
            }
        }

        private void Dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.Reset)
            {
                var dgv = (DataGridView)sender;
                if (dgv.Tag == null)
                {
                    dgv.AutoResizeColumns();
                    dgv.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
                    dgv.Tag = "Resized";
                }
            }
        }

        private void frmLevelView_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                int index = this.TC.SelectedIndex;
                var tp = this.TC.SelectedTab;
                if (tp != null && !string.IsNullOrWhiteSpace(string.Format("{0}", tp.Tag)))
                {
                    this.TC.TabPages.Remove(tp);
                    this.TC.SelectedIndex = index - 1;
                }
            }
        }

        private void TC_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tp = this.TC.SelectedTab;
            if (tp != null)
            {
                this.tsPathText.Text = string.Format("{0}", tp.Tag);
            }
        }
    }
}
