﻿using DXVision;
using DXVision.Serialization.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZX.Data.View
{
    public partial class frmMapView : Form
    {
        SaveHelper SaveHelper;
        MapInfo MapInfo;

        static readonly DataGridViewCellStyle CSytleNotChange = new DataGridViewCellStyle() { BackColor = Color.Silver };

        ///<summary>可绘制</summary>
        bool CanPaint = false;

        ///<summary>绘制地形</summary>
        bool DrawLayerTerrain = true;
        ///<summary>绘制对象</summary>
        bool DrawLayerObjects = true;
        ///<summary>绘制路</summary>
        bool DrawLayerRoads = false;
        ///<summary>绘制僵尸</summary>
        bool DrawLayerZombies = false;

        ///<summary>绘制石油</summary>
        bool DrawOilSource = true;

        //单元格数量 
        int NCells;
        //单元大小
        int CellSize;
        //单元边框大小
        int CellBorderWidth;
        //单元偏移计算大小
        int OffsetSize => this.CellSize + this.CellBorderWidth;

        bool MouseRDown = false;
        Point MouseRDownPoint;

        bool MouseLDown = false;
        Point MouseLDownPoint;

        bool MouseGrap = false;
        ///<summary>抓起待移动实体的鼠标坐标与实体坐标的偏移</summary>
        PointF MouseGrapOffset;
        ///<summary>当前实体的边框区域</summary>
        RectangleF CurrentEntityRect;
        ///<summary>移动实体模式</summary>
        bool MoveEntityMode = false;

        ///<summary>抓起的实体键</summary>
        string MouseGrapKey;
        ///<summary>抓起的实体</summary>
        ComplexProperty MouseGrapEntity;
        ///<summary>悬停的实体</summary>
        ComplexProperty MouseHoverEntity;

        int MouseX;
        int MouseY;

        int CurrentCellX => this.MouseX / this.OffsetSize;
        int CurrentCellY => this.MouseY / this.OffsetSize;
        int CurrentCellWidth;
        int CurrentCellHeight;

        ///<summary>当前工具值</summary>
        int CurrentToolValue;

        ///<summary>边框画笔</summary>
        Pen PBorder;
        ///<summary>当前区域画笔</summary>
        Pen PCurrent;

        Dictionary<string, Brush> DicBrush;
        Dictionary<string, string> DicWord;

        ///<summary>标签字体</summary>
        Font FTTag;
        ///<summary>标签画刷</summary>
        Brush BTag;

        ///<summary>对像字体</summary>
        Font FTObject;
        ///<summary>居中样式</summary>
        StringFormat SFCenter = new StringFormat(StringFormatFlags.NoWrap)
        {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
        };

        TextureBrush TBTileBorder;

        Image imgLayer1;        //地形/资源层
        Image imgLayer2;        //实体层
        Image imgLayer3;        //实体层

        System.Drawing.Imaging.ImageAttributes MaskImageAttributes1;
        System.Drawing.Imaging.ImageAttributes MaskImageAttributes2;


        public frmMapView()
        {
            InitializeComponent();
            this.picMap.MouseWheel += new MouseEventHandler(this.picMap_MouseWheel);
        }

        public static void Show(SaveHelper saveHelper)
        {
            var frm = new frmMapView();

            frm.SaveHelper = saveHelper;

            frm.Init();
            frm.InitTool();
            frm.Show();

            frm.scMap.Panel2Collapsed = true;

            frm.nudCellSize.Value = 16;
            frm.nudCellSize_ValueChanged(null, null);

            frm.Text += " " + saveHelper.FilePath;

            typeof(PictureBox).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)?.SetValue(frm.picMap, true, null);

            //初始位置
            frm.SetScrollPostion((frm.picMap.Width - frm.pnlMap.Width) / 2, (frm.picMap.Height - frm.pnlMap.Height) / 2);
        }

        private void picTerrainResource_Paint(object sender, PaintEventArgs e)
        {
            if (!this.CanPaint)
            {
                return;
            }
            var g = e.Graphics;

            g.DrawImage(this.imgLayer3, new Rectangle(Point.Empty, this.picTerrainResource.Size), new Rectangle(Point.Empty, imgLayer3.Size), GraphicsUnit.Pixel);

            float ratioWidth = this.picMap.Width / this.picTerrainResource.Width;
            float ratioHeight = this.picMap.Height / this.picTerrainResource.Height;

            g.DrawRectangle(Pens.Red, -this.pnlMap.AutoScrollPosition.X / ratioWidth, -this.pnlMap.AutoScrollPosition.Y / ratioHeight, (this.pnlMap.Width - 16) / ratioWidth, (this.pnlMap.Height - 16) / ratioHeight);
        }

        private void picMap_Paint(object sender, PaintEventArgs e)
        {
            if (!this.CanPaint)
            {
                return;
            }
            var g = e.Graphics;
            var clipRect = e.ClipRectangle;

            //Draw Broder
            if (this.chkGridVisible.Checked)
            {
                g.FillRectangle(this.TBTileBorder, clipRect);
            }

            if (!this.MoveEntityMode)
            {
                g.DrawImage(this.imgLayer1
                    , clipRect
                    , clipRect.X, clipRect.Y, clipRect.Width, clipRect.Height
                    , GraphicsUnit.Pixel
                    , this.MaskImageAttributes1
                    );

                if (this.chkLayer2Visible.Checked)
                {
                    g.DrawImage(this.imgLayer2
                        , clipRect
                        , clipRect.X, clipRect.Y, clipRect.Width, clipRect.Height
                        , GraphicsUnit.Pixel
                        , this.MaskImageAttributes2
                        );
                }
            }
            else
            {
                g.DrawImage(this.imgLayer1
                    , clipRect
                    , clipRect.X, clipRect.Y, clipRect.Width, clipRect.Height
                    , GraphicsUnit.Pixel
                    , this.MaskImageAttributes2
                    );

                g.DrawImage(this.imgLayer2
                    , clipRect
                    , clipRect.X, clipRect.Y, clipRect.Width, clipRect.Height
                    , GraphicsUnit.Pixel
                    , this.MaskImageAttributes1
                    );
            }

            if (this.CurrentCellWidth > 0 && this.CurrentCellHeight > 0)
            {   //Draw CurrentRect
                g.DrawRectangle(this.PCurrent
                    , this.CurrentCellX * this.OffsetSize - 1
                    , this.CurrentCellY * this.OffsetSize - 1
                    , this.CurrentCellWidth * this.OffsetSize + 3
                    , this.CurrentCellHeight * this.OffsetSize + 3
                );
            }

            if (this.MoveEntityMode)
            {
                if (this.CurrentEntityRect != RectangleF.Empty)
                {   //Draw EntityRect
                    g.DrawRectangle(this.PCurrent
                        , this.CurrentEntityRect.X * this.OffsetSize - 1
                        , this.CurrentEntityRect.Y * this.OffsetSize - 1
                        , this.CurrentEntityRect.Width * this.OffsetSize + 3
                        , this.CurrentEntityRect.Height * this.OffsetSize + 3
                    );
                }
            }
        }

        private void UpdateLayer1(float rectX, float rectY, float rectWidth, float rectHeight)
        {
            using (var g = Graphics.FromImage(this.imgLayer1))
            {
                RectangleF rectF = new RectangleF(rectX, rectY, rectWidth, rectHeight);
                //清除区域
                this.DrawTile(g, GetCellPixelRect(rectF), 9, 999);


                for (int x = (int)Math.Max(rectX, 0); x < Math.Min(rectX + rectWidth, this.MapInfo.NCells); x++)
                {
                    for (int y = (int)Math.Max(rectY, 0); y < Math.Min(rectY + rectHeight, this.MapInfo.NCells); y++)
                    {
                        if (this.DrawLayerTerrain)
                        {   //Draw LayerTerrain
                            this.DrawTile(g, GetCellPixelRect(x, y, -1), 1, 0);
                            this.DrawTile(g, GetCellPixelRect(x, y, -1), 1, (int)this.MapInfo.LayerTerrain[x, y]);
                        }
                        if (this.DrawLayerZombies)
                        {   //Draw LayerZombies
                            this.DrawTile(g, GetCellPixelRect(x, y, -this.CellSize / 3), 4, (int)this.MapInfo.LayerZombies[x, y]);
                        }
                        if (this.DrawLayerRoads)
                        {   //Draw LayerRoads
                            this.DrawTile(g, GetCellPixelRect(x, y, -1), 3, (int)this.MapInfo.LayerRoads[x, y]);
                        }
                        if (this.DrawLayerObjects)
                        {   //Draw LayerObjects
                            this.DrawTile(g, GetCellPixelRect(x, y, -1), 2, (int)this.MapInfo.LayerObjects[x, y]);
                        }
                    }
                }

                if (this.DrawOilSource)
                {
                    //Draw Oil
                    foreach (var i in this.MapInfo.OilSource)
                    {
                        var entityRect = this.SaveHelper.GetComplexPropertyRectangle(i);
                        if (rectF.IntersectsWith(entityRect))
                        {
                            this.DrawTile(g, GetCellPixelRect(Rectangle.Round(entityRect), -2), 9, 101);
                        }
                    }
                }
            }
        }

        private void UpdateLayer2(float rectX, float rectY, float rectWidth, float rectHeight)
        {
            using (var g = Graphics.FromImage(this.imgLayer2))
            {
                RectangleF rectF = new RectangleF(rectX, rectY, rectWidth, rectHeight);
                //清除区域
                this.DrawTile(g, GetCellPixelRect(rectF), 9, 999);

                //Draw Entity
                foreach (var i in this.MapInfo.EntitySource)
                {
                    var entityRect = i.Rect;

                    entityRect.Width = Math.Max(entityRect.Width, 1);
                    entityRect.Height = Math.Max(entityRect.Height, 1);

                    if (rectF.IntersectsWith(entityRect))
                    {
                        this.DrawTile(g, GetCellPixelRect(entityRect, -2), 9, 0);
                    }
                }
            }
        }

        private void UpdateLayer3(float rectX, float rectY, float rectWidth, float rectHeight)
        {
            using (var g = Graphics.FromImage(this.imgLayer3))
            {
                RectangleF rectF = new RectangleF(rectX, rectY, rectWidth, rectHeight);
                //清除区域
                this.DrawTile(g, GetCellPixelRect(rectF), 9, 1000);

                for (int x = (int)Math.Max(rectX, 0); x < Math.Min(rectX + rectWidth, this.MapInfo.NCells); x++)
                {
                    for (int y = (int)Math.Max(rectY, 0); y < Math.Min(rectY + rectHeight, this.MapInfo.NCells); y++)
                    {
                        this.DrawTile(g, GetCellPixelRect(x, y), 5, (int)this.MapInfo.TerrainResourceType[x, y]);
                    }
                }
            }
        }

        private void DrawTile(Graphics g, RectangleF rectF, int type, int value, string name = "")
        {
            string key, keyBack;

            switch (type)
            {
                case 1:
                    //LayerTerrain
                    key = $"LayerTerrain:{value}";
                    if (this.DicBrush.ContainsKey(key))
                    {
                        g.FillRectangle(this.DicBrush[key], rectF);
                    }
                    break;
                case 2:
                    //LayerObjects
                    key = $"LayerObjects:{value}";
                    if (this.DicBrush.ContainsKey(key))
                    {
                        g.DrawString(this.DicWord[key], this.FTObject, this.DicBrush[key], rectF, this.SFCenter);
                    }
                    break;
                case 3:
                    //LayerRoads
                    key = $"LayerRoads:{value}";
                    if (this.DicBrush.ContainsKey(key))
                    {
                        g.FillRectangle(this.DicBrush[key], rectF);
                    }
                    break;
                case 4:
                    //LayerZombies
                    key = $"LayerZombies:{value}";
                    if (this.DicBrush.ContainsKey(key))
                    {
                        g.FillEllipse(this.DicBrush[key], rectF);
                        g.DrawEllipse(new Pen(this.DicBrush["LayerZombies:Border"], 1), rectF);
                    }
                    break;
                case 5:
                    //TerrainResourceType
                    key = $"TerrainResourceType:{value}";
                    if (this.DicBrush.ContainsKey(key))
                    {
                        g.FillRectangle(this.DicBrush[key], rectF);
                    }
                    break;

                case 9:
                    {
                        key = $"Entity:{value}";
                        keyBack = $"EntityBack:{value}";

                        g.FillRectangle(this.DicBrush[keyBack], rectF);

                        if (string.IsNullOrWhiteSpace(name) && this.DicWord.ContainsKey(key))
                        {
                            name = this.DicWord[key];
                        }
                        if (!string.IsNullOrWhiteSpace(name))
                        {
                            g.DrawString(name, this.FTObject, this.DicBrush[key], rectF, this.SFCenter);
                        }
                    }
                    break;
            }
        }

        ///<summary>取得单元区域(含边框)</summary>
        private RectangleF GetCellPixelRect(RectangleF rect, int expansion = 0)
        {
            return new RectangleF(
                rect.Left * this.OffsetSize - expansion
                , rect.Top * this.OffsetSize - expansion
                , rect.Width * this.OffsetSize + expansion * 2 + 1
                , rect.Height * this.OffsetSize + expansion * 2 + 1
            );
        }
        ///<summary>取得单元区域(含边框)</summary>
        private RectangleF GetCellPixelRect(Point p, int expansion = 0)
        {
            return GetCellPixelRect(p.X, p.Y, expansion);
        }
        ///<summary>取得单元区域(含边框)</summary>
        private RectangleF GetCellPixelRect(int x, int y, int expansion = 0)
        {
            return GetCellPixelRect(new RectangleF(x, y, 1, 1), expansion);
        }

        private void Init()
        {
            this.MapInfo = this.SaveHelper.GetMapInfo();

            this.NCells = this.MapInfo.NCells;

            this.DicBrush = new Dictionary<string, Brush>
            {
                {"LayerTerrain:0", new SolidBrush(Color.White)},
                {"LayerTerrain:1", new SolidBrush(Color.FromArgb(0xC0, Color.DarkBlue))},
                {"LayerTerrain:2", new SolidBrush(Color.FromArgb(0x40, Color.GreenYellow))},
                {"LayerTerrain:3", new SolidBrush(Color.LightSkyBlue)},
                {"LayerTerrain:4", new SolidBrush(Color.DarkRed)},

                {"LayerObjects:1", new SolidBrush(Color.SaddleBrown)},
                {"LayerObjects:2", new SolidBrush(Color.DarkGreen)},
                {"LayerObjects:3", new SolidBrush(Color.Gold)},
                {"LayerObjects:4", new SolidBrush(Color.LightSlateGray)},
                {"LayerObjects:5", new SolidBrush(Color.Purple)},

                {"LayerRoads:1", new SolidBrush(Color.Gray)},

                {"TerrainResourceType:0", new SolidBrush(Color.Black)},
                {"TerrainResourceType:10", new SolidBrush(Color.FromArgb(0xC0, Color.DarkBlue))},
                {"TerrainResourceType:20", new SolidBrush(Color.White)},
                {"TerrainResourceType:30", new SolidBrush(Color.FromArgb(0x40, Color.GreenYellow))},
                {"TerrainResourceType:40", new SolidBrush(Color.LightSlateGray)},
                {"TerrainResourceType:50", new SolidBrush(Color.Purple)},
                {"TerrainResourceType:60", new SolidBrush(Color.Olive)},
                {"TerrainResourceType:70", new SolidBrush(Color.Gold)},
                {"TerrainResourceType:80", new SolidBrush(Color.Black)},
                {"TerrainResourceType:90", new SolidBrush(Color.DarkGreen)},
                {"TerrainResourceType:100", new SolidBrush(Color.SaddleBrown)},
                {"TerrainResourceType:110", new SolidBrush(Color.Black)},

                {"Oil", new SolidBrush(Color.Olive)},

                {"Entity:0", new SolidBrush(Color.White)},
                {"EntityBack:0", new SolidBrush(Color.Blue)},

                {"Entity:100", new SolidBrush(Color.Black)},
                {"EntityBack:100", new SolidBrush(Color.Green)},

                {"Entity:101", new SolidBrush(Color.Black)},
                {"EntityBack:101", new SolidBrush(Color.Olive)},

                {"Entity:999", new SolidBrush(Color.FromArgb(0xFF, 0, 0xFF))},
                {"EntityBack:999", new SolidBrush(Color.FromArgb(0xFF, 0, 0xFF))},
                {"Entity:1000", new SolidBrush(Color.White)},
                {"EntityBack:1000", new SolidBrush(Color.White)},
            };

            for (var f = 1; f <= 12; f++)
            {
                this.DicBrush[$"LayerZombies:{f}"] = new SolidBrush(Color.FromArgb(0x60 + 0xFF / 12 * 1, Color.Red));
            }
            this.DicBrush[$"LayerZombies:Border"] = new SolidBrush(Color.Red);

            this.DicWord = new Dictionary<string, string>
            {
                {"LayerObjects:1", "山"},
                {"LayerObjects:2", "林"},
                {"LayerObjects:3", "金"},
                {"LayerObjects:4", "石"},
                {"LayerObjects:5", "铁"},

                {"Entity:100", "指挥中心"},
                {"Entity:101", "油"},
            };


            this.MaskImageAttributes1 = new System.Drawing.Imaging.ImageAttributes();
            this.MaskImageAttributes1.SetColorKey(Color.FromArgb(0xFF, 0, 0xFF), Color.FromArgb(0xFF, 0, 0xFF));

            this.MaskImageAttributes2 = new System.Drawing.Imaging.ImageAttributes();
            this.MaskImageAttributes2.SetColorKey(Color.FromArgb(0xFF, 0, 0xFF), Color.FromArgb(0xFF, 0, 0xFF));
            this.MaskImageAttributes2.SetColorMatrix(new System.Drawing.Imaging.ColorMatrix { Matrix33 = 0.4f });

            this.tsMapSize.Text = string.Format("MapCell:{0}x{0}", this.NCells);

            string lastName = "";
            TreeNode nd = null;
            foreach (var i in this.MapInfo.EntitySource.OrderBy(a => a.Name))
            {
                if (i.Name != lastName)
                {
                    if (nd != null)
                    {
                        nd.Text = string.Format("{0} ({1})", lastName, nd.Nodes.Count);
                        nd.Checked = true;
                        this.tvEntity.Nodes.Add(nd);
                    }
                    nd = new TreeNode();
                    lastName = i.Name;
                }
                var cnd = nd.Nodes.Add(i.Key, string.Format("{0} ({1:0.00},{2:0.00})", i.Name, i.Rect.X, i.Rect.Y));
                cnd.Tag = i.Key;
                cnd.Checked = i.Visible;
            }

            if (nd != null)
            {
                nd.Text = string.Format("{0} ({1})", lastName, nd.Nodes.Count);
                this.tvEntity.Nodes.Add(nd);
                nd.Checked = true;
            }

            this.AddNode(this.tvSaveData.Nodes, "", this.SaveHelper.RootProperty.Type.Name, this.SaveHelper.RootProperty);
        }

        private void InitTool()
        {
            this.dgvTool.Rows.Clear();

            Dictionary<string, int> dicTool = new Dictionary<string, int>
            {
                {"地面", 1110000},
                {"河"  , 1110001},
                {"草地", 1110002},
                {"天空", 1110003},
                {"深渊", 1110004},

                {"清除[山/林/矿]", 1120000},
                {"山"  , 1120001},
                {"林"  , 1120002},
                {"金矿", 1120003},
                {"石矿", 1120004},
                {"铁矿", 1120005},

                {"石油(2x2)"     , 2290101},
                {"消除石油(2x2)" ,12290101},

                {"移动对象" ,20090000},
            };

            var rect = new Rectangle(0, 0, 16, 16);
            foreach (var i in dicTool)
            {
                var picIcon = new Bitmap(16, 16);

                //初始绘制
                this.CellSize = 16;
                this.CellBorderWidth = 1;
                this.FTObject = new Font("微软雅黑", 15, FontStyle.Bold, GraphicsUnit.Pixel);

                var g = Graphics.FromImage(picIcon);
                g.Clear(Color.White);
                this.DrawTile(g, rect, i.Value / 10000 % 10, i.Value % 10000);
                this.dgvTool.Rows.Add(picIcon, i.Key, i.Value);
            }

            this.dgvTool.CurrentCell = this.dgvTool[0, 5];
        }

        private void SetCellSize(int cellSize, int cellBorderWidth)
        {
            this.CellSize = cellSize;
            this.CellBorderWidth = cellBorderWidth;
            this.FTObject = new Font("微软雅黑", this.CellSize - 1, FontStyle.Bold, GraphicsUnit.Pixel);

            this.PBorder = new Pen(new SolidBrush(Color.FromArgb(0x40, Color.Gray)), this.CellBorderWidth) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot };
            this.PCurrent = new Pen(new SolidBrush(Color.FromArgb(0x80, Color.Red)), 2) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dash };

            this.BTag = new SolidBrush(Color.Red);
            this.FTTag = new Font("微软雅黑", 13, FontStyle.Regular, GraphicsUnit.Pixel);

            var sfCenter = new StringFormat(StringFormatFlags.NoWrap)
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            this.picMap.Size = new Size(this.NCells * this.OffsetSize + 1, this.NCells * this.OffsetSize + 1);

            Graphics g;

            var img = new Bitmap(this.OffsetSize, this.OffsetSize);
            g = Graphics.FromImage(img);
            g.DrawLine(this.PBorder, 0, 0, 0, img.Height);
            g.DrawLine(this.PBorder, 0, 0, img.Width, 0);
            g.Dispose();
            this.TBTileBorder = new TextureBrush(img);
            this.TBTileBorder.WrapMode = System.Drawing.Drawing2D.WrapMode.Tile;


            this.imgLayer1?.Dispose();
            this.imgLayer1 = new Bitmap(this.picMap.Size.Width, this.picMap.Size.Height);
            g = Graphics.FromImage(this.imgLayer1);
            g.Clear(Color.FromArgb(0xFF, 0, 0xFF));
            g.Dispose();

            this.imgLayer2?.Dispose();
            this.imgLayer2 = new Bitmap(this.picMap.Size.Width, this.picMap.Size.Height);
            g = Graphics.FromImage(this.imgLayer2);
            g.Clear(Color.FromArgb(0xFF, 0, 0xFF));
            g.Dispose();

            this.imgLayer3?.Dispose();
            this.imgLayer3 = new Bitmap(this.picMap.Size.Width, this.picMap.Size.Height);
            g = Graphics.FromImage(this.imgLayer3);
            g.Clear(Color.White);
            g.Dispose();


            //this.UpdateLayer0();
            this.UpdateLayer1(0, 0, this.NCells, this.NCells);
            this.UpdateLayer2(0, 0, this.NCells, this.NCells);
            this.UpdateLayer3(0, 0, this.NCells, this.NCells);
        }

        private void picMap_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.MouseRDown = false;
            }
            else if (e.Button == MouseButtons.Left)
            {
                this.MouseLDown = false;

                if (this.MoveEntityMode)
                {
                    this.MoveEntity(1.0f * e.X / this.OffsetSize, 1.0f * e.Y / this.OffsetSize);
                }

                this.MouseGrap = false;

            }
            this.timerTerrainResourceRefresh.Enabled = true;
        }

        private void picMap_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.MouseRDown = true;
                this.MouseRDownPoint = e.Location;
            }
            else if (e.Button == MouseButtons.Left)
            {
                this.MouseLDown = true;
                this.MouseLDownPoint = e.Location;
                if (!this.MoveEntityMode)
                {
                    this.UpdateCell();
                }
                else
                {
                    this.FindEntity(1.0f * e.X / this.OffsetSize, 1.0f * e.Y / this.OffsetSize);
                }
            }
        }

        private void picMap_MouseMove(object sender, MouseEventArgs e)
        {

            var oldCellX = this.CurrentCellX;
            var oldCellY = this.CurrentCellY;

            this.MouseX = e.X;
            this.MouseY = e.Y;

            var newCellX = this.CurrentCellX;
            var newCellY = this.CurrentCellY;

            if (oldCellX != newCellX || oldCellY != newCellY)
            {
                var rgn = new Region();
                rgn.Union(GetCellPixelRect(new Rectangle(oldCellX, oldCellY, this.CurrentCellWidth, this.CurrentCellHeight), 4));
                rgn.Union(GetCellPixelRect(new Rectangle(newCellX, newCellY, this.CurrentCellWidth, this.CurrentCellHeight), 4));
                this.picMap.Invalidate(rgn);
            }

            if (this.MouseRDown)
            {   //拖动画布
                this.pnlMap.AutoScrollPosition = new Point(
                    -(this.pnlMap.AutoScrollPosition.X + (e.X - this.MouseRDownPoint.X))
                    , -(this.pnlMap.AutoScrollPosition.Y + (e.Y - this.MouseRDownPoint.Y))
                    );
            }
            else if (this.MouseLDown)
            {   //绘制
                if (!this.MoveEntityMode)
                {
                    this.UpdateCell();
                }
                else
                {
                    this.MoveEntity(1.0f * e.X / this.OffsetSize, 1.0f * e.Y / this.OffsetSize);
                }
            }
            else
            {
                if (this.MoveEntityMode)
                {   //查找目标
                    this.picMap.Invalidate(Rectangle.Round(GetCellPixelRect(this.CurrentEntityRect, 4)));

                    this.FindEntity(1.0f * e.X / this.OffsetSize, 1.0f * e.Y / this.OffsetSize);
                }
            }
            this.tsMousePostion.Text = string.Format("CurrentCell:({0}, {1})", this.CurrentCellX, this.CurrentCellY);
        }

        private void FindEntity(float x, float y)
        {
            EntityInfo entity = null;

            foreach (var i in this.MapInfo.EntitySource)
            {
                var rect = i.Rect;
                rect.Width = Math.Max(rect.Width, 1);
                rect.Height = Math.Max(rect.Height, 1);

                if (rect.Contains(new PointF(x, y)))
                {
                    entity = i;
                    this.MouseHoverEntity = i.Property;
                    this.MouseGrapKey = entity.Key;
                    break;
                }
            }

            if (entity != null)
            {
                this.CurrentEntityRect = entity.Rect;

                this.CurrentEntityRect.Width = Math.Max(this.CurrentEntityRect.Width, 1);
                this.CurrentEntityRect.Height = Math.Max(this.CurrentEntityRect.Height, 1);

                this.picMap.Invalidate(Rectangle.Round(GetCellPixelRect(this.CurrentEntityRect, 3)));

                if (this.MouseLDown)
                {   //抓取
                    this.MouseGrapEntity = this.MouseHoverEntity;
                    var rect = entity.Rect;

                    this.MouseGrapOffset = new PointF(x - rect.X, y - rect.Y);
                    this.MouseGrap = true;

                    var ndArray = this.tvEntity.Nodes.Find(entity.Key, true);
                    if (ndArray != null && ndArray.Length == 1)
                    {
                        this.tvEntity.SelectedNode = ndArray[0];
                        this.tvEntity.Select();
                    }
                }
                if (this.lblTag.Visible == false)
                {
                    this.lblTag.Visible = true;
                }

                this.lblTag.Text = entity.Name;
                this.lblTag.Location = new Point(this.MouseX + this.pnlMap.AutoScrollPosition.X + 16, this.MouseY + this.pnlMap.AutoScrollPosition.Y - 16);
            }
            else
            {
                this.lblTag.Visible = false;
                this.CurrentEntityRect = RectangleF.Empty;
            }
        }

        private void MoveEntity(float x, float y)
        {
            if (!this.MouseGrap)
            {
                return;
            }

            ComplexProperty entity = this.MouseGrapEntity;

            if (entity != null)
            {   //取得区域
                var oldRect = this.SaveHelper.GetComplexPropertyRectangle(entity);

                var newPoint = new PointF(x - this.MouseGrapOffset.X, y - this.MouseGrapOffset.Y);

                //新区域
                var newRect = this.CurrentEntityRect = new RectangleF(new PointF(x - this.MouseGrapOffset.X, y - this.MouseGrapOffset.Y), oldRect.Size);

                if (!this.MouseLDown)
                {
                    //设定(中点)位置
                    this.EntitySetLocation(newRect.X + newRect.Width / 2, newRect.Y + newRect.Height / 2);

                    oldRect.Width = newRect.Width = this.CurrentEntityRect.Width = Math.Max(this.CurrentEntityRect.Width, 1);
                    oldRect.Height = newRect.Height = this.CurrentEntityRect.Height = Math.Max(this.CurrentEntityRect.Height, 1);

                    newRect = this.SaveHelper.GetComplexPropertyRectangle(entity);

                    //重绘
                    this.UpdateLayer2(oldRect.X, oldRect.Y, oldRect.Width, oldRect.Height);
                    this.UpdateLayer2(newRect.X, newRect.Y, newRect.Width, newRect.Height);

                    this.picMap.Invalidate(Rectangle.Round(GetCellPixelRect(oldRect, 10)));

                }

                this.picMap.Invalidate(Rectangle.Round(GetCellPixelRect(newRect, 10)));
            }
        }

        private void UpdateTerrainResource(int x, int y, int type = 0)
        {
            var vTerrain = this.MapInfo.LayerTerrain[x, y];
            var vObjects = this.MapInfo.LayerObjects[x, y];

            ZXTerrainResourceType vTerrainResource = ZXTerrainResourceType.None;

            switch (vTerrain)
            {
                case ZXMapLayerTerrainType.Earth:
                    vTerrainResource = ZXTerrainResourceType.Earth;
                    break;
                case ZXMapLayerTerrainType.Water:
                    vTerrainResource = ZXTerrainResourceType.Sea;
                    break;
                case ZXMapLayerTerrainType.Grass:
                    vTerrainResource = ZXTerrainResourceType.Grass;
                    break;
                case ZXMapLayerTerrainType.Sky:
                    vTerrainResource = ZXTerrainResourceType.None;
                    break;
                case ZXMapLayerTerrainType.Abyss:
                    vTerrainResource = ZXTerrainResourceType.None;
                    break;
            }

            switch (vTerrain)
            {
                case ZXMapLayerTerrainType.Earth:
                case ZXMapLayerTerrainType.Grass:
                    switch (vObjects)
                    {
                        case ZXMapLayerObjectType.Mountain:
                            vTerrainResource = ZXTerrainResourceType.Mountain;
                            break;
                        case ZXMapLayerObjectType.Tree:
                            vTerrainResource = ZXTerrainResourceType.Wood;
                            break;
                        case ZXMapLayerObjectType.MineralGold:
                            vTerrainResource = ZXTerrainResourceType.Gold;
                            break;
                        case ZXMapLayerObjectType.MineralStone:
                            vTerrainResource = ZXTerrainResourceType.Stone;
                            break;
                        case ZXMapLayerObjectType.MineralIron:
                            vTerrainResource = ZXTerrainResourceType.Iron;
                            break;
                        case ZXMapLayerObjectType.None:
                            if (type == 9)
                            {
                                vTerrainResource = ZXTerrainResourceType.Oil;
                            }
                            break;
                    }
                    break;
            }

            this.MapInfo.TerrainResourceType[x, y] = (int)vTerrainResource;

            if (vTerrainResource == ZXTerrainResourceType.Oil && type == 9 && x < 255 && y < 255)
            {
                this.MapInfo.TerrainResourceType[x + 1, y] = (int)vTerrainResource;
                this.MapInfo.TerrainResourceType[x, y + 1] = (int)vTerrainResource;
                this.MapInfo.TerrainResourceType[x + 1, y + 1] = (int)vTerrainResource;
            }

            if (type == -9)
            {
                if (this.MapInfo.TerrainResourceType[x + 1, y] == (int)ZXTerrainResourceType.Oil)
                {
                    this.UpdateTerrainResource(x + 1, y);
                }
                if (this.MapInfo.TerrainResourceType[x, y + 1] == (int)ZXTerrainResourceType.Oil)
                {
                    this.UpdateTerrainResource(x, y + 1);
                }
                if (this.MapInfo.TerrainResourceType[x + 1, y + 1] == (int)ZXTerrainResourceType.Oil)
                {
                    this.UpdateTerrainResource(x + 1, y + 1);
                }
            }
        }

        private void UpdateCell()
        {
            int l = Math.Max(Math.Min(this.CurrentCellX, this.NCells), 0);
            int t = Math.Max(Math.Min(this.CurrentCellY, this.NCells), 0);
            int r = Math.Max(Math.Min(this.CurrentCellX + this.CurrentCellWidth, this.NCells), 0);
            int b = Math.Max(Math.Min(this.CurrentCellY + this.CurrentCellHeight, this.NCells), 0);

            var type = this.CurrentToolValue / 10000 % 10;
            var value = this.CurrentToolValue % 10000;
            var width = this.CurrentToolValue / 100000 % 10;
            var heigth = this.CurrentToolValue / 1000000 % 10;
            var erase = this.CurrentToolValue / 10000000 % 10;

            switch (type)
            {
                case 1:
                    for (var x = l; x < r; x++)
                    {
                        for (var y = t; y < b; y++)
                        {
                            if (this.MapInfo.LayerTerrain[x, y] != (ZXMapLayerTerrainType)value)
                            {
                                this.MapInfo.LayerTerrain[x, y] = (ZXMapLayerTerrainType)value;
                                this.UpdateLayer1(x, y, 1, 1);
                                this.UpdateTerrainResource(x, y, type);
                                this.UpdateLayer3(x, y, 1, 1);
                            }
                        }
                    }
                    this.picMap.Invalidate(Rectangle.Ceiling(GetCellPixelRect(new Rectangle(l, t, r - l, b - t))));
                    break;
                case 2:
                    for (var x = l; x < r; x++)
                    {
                        for (var y = t; y < b; y++)
                        {
                            if (this.MapInfo.LayerObjects[x, y] != (ZXMapLayerObjectType)value)
                            {
                                this.MapInfo.LayerObjects[x, y] = (ZXMapLayerObjectType)value;
                                this.UpdateLayer1(x, y, 1, 1);
                                this.UpdateTerrainResource(x, y, type);
                                this.UpdateLayer3(x, y, 1, 1);
                            }
                        }
                    }
                    this.picMap.Invalidate(Rectangle.Ceiling(GetCellPixelRect(new Rectangle(l, t, r - l, b - t))));
                    break;
                case 9:
                    if (value == 101)
                    {   //油
                        Rectangle oldRect;
                        var newRect = new Rectangle(this.CurrentCellX, this.CurrentCellY, this.CurrentCellWidth, this.CurrentCellHeight);

                        ComplexProperty entity = null;
                        if (!this.MouseGrap)
                        {   //查找
                            foreach (var i in this.MapInfo.OilSource)
                            {
                                var rect = this.SaveHelper.GetComplexPropertyRectangle(i);
                                if (rect.IntersectsWith(newRect))
                                {
                                    this.MouseGrapEntity = entity = i;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            entity = this.MouseGrapEntity;
                        }

                        if (erase == 1)
                        {   //消除
                            if (entity != null)
                            {
                                var extraEntities = this.SaveHelper.GetProperty(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\ExtraEntities") as CollectionProperty;

                                //取得区域
                                oldRect = Rectangle.Round(this.SaveHelper.GetComplexPropertyRectangle(entity));

                                //删除
                                extraEntities.Items.Remove(entity);
                                this.MapInfo.OilSource.Remove(entity);

                                //重绘
                                this.UpdateLayer1(oldRect.X, oldRect.Y, oldRect.Width, oldRect.Height);
                                this.UpdateTerrainResource(oldRect.X, oldRect.Y, -9);

                                this.picMap.Invalidate(Rectangle.Ceiling(GetCellPixelRect(oldRect)));
                            }
                        }
                        else
                        {   //创建 & 移动
                            if (entity == null)
                            {
                                //创建
                                this.MouseGrapEntity = entity = this.SaveHelper.ComplexPropertyClone(this.MapInfo.OilSourceTemplete);

                                //设定(中点)位置
                                this.SaveHelper.SetComplexPropertyPoint(entity, new PointF(newRect.X + newRect.Width / 2, newRect.Y + newRect.Height / 2));

                                newRect = Rectangle.Round(this.SaveHelper.GetComplexPropertyRectangle(entity));

                                var extraEntities = this.SaveHelper.GetProperty(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\ExtraEntities") as CollectionProperty;

                                //添加
                                extraEntities.Items.Add(entity);
                                this.MapInfo.OilSource.Add(entity);

                                //重绘
                                this.UpdateLayer1(newRect.X, newRect.Y, newRect.Width, newRect.Height);
                                this.picMap.Invalidate(Rectangle.Ceiling(GetCellPixelRect(newRect)));
                            }
                            else
                            {
                                //取得区域
                                oldRect = Rectangle.Round(this.SaveHelper.GetComplexPropertyRectangle(entity));

                                //设定(中点)位置
                                this.SaveHelper.SetComplexPropertyPoint(entity, new PointF(newRect.X + newRect.Width / 2, newRect.Y + newRect.Height / 2));

                                newRect = Rectangle.Round(this.SaveHelper.GetComplexPropertyRectangle(entity));

                                //重绘
                                this.UpdateLayer1(oldRect.X, oldRect.Y, oldRect.Width, oldRect.Height);
                                this.UpdateTerrainResource(oldRect.X, oldRect.Y, -9);
                                this.UpdateLayer1(newRect.X, newRect.Y, newRect.Width, newRect.Height);
                                this.UpdateTerrainResource(newRect.X, newRect.Y, 9);

                                this.picMap.Invalidate(Rectangle.Ceiling(GetCellPixelRect(oldRect)));
                                this.picMap.Invalidate(Rectangle.Ceiling(GetCellPixelRect(newRect)));
                            }
                            this.MouseGrap = true;
                        }
                    }
                    break;
            }
        }

        private void EntitySetLocation(float x, float y)
        {
            var entity = this.MapInfo.EntitySource.FirstOrDefault(a => a.Key == this.MouseGrapKey);

            this.SaveHelper.SetComplexPropertyPoint(this.MouseGrapEntity, new PointF(x, y));

            entity.Rect.Location = this.SaveHelper.GetComplexPropertyRectangle(this.MouseGrapEntity).Location;

            var ndArray = this.tvEntity.Nodes.Find(entity.Key, true);
            if (ndArray != null && ndArray.Length == 1)
            {
                var pointF = (PointF)this.SaveHelper.GetPropertyValue(entity.Property, "Position");
                ndArray[0].Text = string.Format("{0} ({1:0.00},{2:0.00})", entity.Name, pointF.X, pointF.Y);
            }

        }

        private void picMap_MouseWheel(object sender, MouseEventArgs e)
        {
            this.timerTerrainResourceRefresh.Enabled = true;
        }

        private void dgvTool_CurrentCellChanged(object sender, EventArgs e)
        {
            if (this.dgvTool.CurrentCell != null && this.dgvTool.CurrentCell.RowIndex >= 0)
            {
                int.TryParse(string.Format("{0}", this.dgvTool[2, this.dgvTool.CurrentCell.RowIndex].Value), out this.CurrentToolValue);

                //是否为移动模式
                this.MoveEntityMode = this.CurrentToolValue / 10000000 % 10 == 2;

                this.scMap.Panel2Collapsed = !this.MoveEntityMode;
                this.chkLayer2Visible.Enabled = !this.MoveEntityMode;

                var type = this.CurrentToolValue / 10000 % 10;
                if (type == 9)
                {   //油井
                    this.CurrentCellWidth = this.CurrentToolValue / 100000 % 10;
                    this.CurrentCellHeight = this.CurrentToolValue / 1000000 % 10;

                    this.nudToolSize.Enabled = false;
                }
                else
                {   //绘制模式
                    this.CurrentCellHeight = (int)this.nudToolSize.Value;
                    this.CurrentCellWidth = (int)this.nudToolSize.Value;

                    this.nudToolSize.Enabled = true;
                }

                this.picMap.Refresh();
            }
        }

        private void timerCellChange_Tick(object sender, EventArgs e)
        {
            this.timerCellChange.Enabled = false;
            this.SetCellSize((int)this.nudCellSize.Value, 1);
            this.CanPaint = true;
            this.picMap.Refresh();
            this.picTerrainResource.Refresh();
        }

        private void nudCellSize_ValueChanged(object sender, EventArgs e)
        {
            this.CanPaint = false;
            this.timerCellChange.Enabled = true;
        }

        private void nudToolSize_ValueChanged(object sender, EventArgs e)
        {
            var oldW = this.CurrentCellWidth;
            var oldH = this.CurrentCellHeight;

            this.CurrentCellWidth = this.CurrentCellHeight = (int)this.nudToolSize.Value;

            var newW = this.CurrentCellWidth;
            var newH = this.CurrentCellHeight;

            this.picMap.Invalidate(Rectangle.Ceiling(GetCellPixelRect(new Rectangle(this.CurrentCellX, this.CurrentCellY, Math.Max(oldW, newW), Math.Max(oldH, newH)), 4)));
        }



        private void tsSave_Click(object sender, EventArgs e)
        {
            this.SaveHelper.Update(this.MapInfo);
            this.SaveHelper.Save();
            MessageBox.Show("OK");
        }

        private void tsRefresh_Click(object sender, EventArgs e)
        {
            this.nudCellSize_ValueChanged(null, null);
        }

        private void dgvEvent_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.Reset)
            {
                var dgv = (DataGridView)sender;

                if (dgv.Tag == null)
                {
                    dgv.AutoResizeColumns();
                    dgv.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
                    dgv.Tag = "Resized";
                }
            }
            this.RefreshSchedule();
        }


        private void dgvEvent_CurrentCellChanged(object sender, EventArgs e)
        {
            var dgv = (DataGridView)sender;

            if (dgv.CurrentCell != null
                && dgv.CurrentCell.ColumnIndex >= 0
                && dgv.CurrentCell.RowIndex >= 0
                )
            {
                var id = $"{dgv[0, dgv.CurrentCell.RowIndex].Value}";

                var rect = dgv.GetCellDisplayRectangle(dgv.CurrentCell.ColumnIndex, dgv.CurrentCell.RowIndex, true);
                this.cmsEvent.Tag = id;
                this.cmsEvent.Show(dgv, rect.GetBottomLeft());
            }
        }


        private void dgvData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                var name = $"{this.dgvData[0, e.RowIndex].Value}";
                var value = $"{this.dgvData[2, e.RowIndex].Value}";

                var p = this.SaveHelper.GetProperty(this.txtPath.Text);
                this.SaveHelper.SetPropertyValue(p as ComplexProperty, name, value);
            }
        }

        private void dgvData_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.Reset)
            {
                var dgv = (DataGridView)sender;
                dgv.AutoResizeColumns();

                //readonly
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    r.Cells[0].ReadOnly = true;
                    r.Cells[1].ReadOnly = true;

                    var v = $"{r.Cells[2].Value}";
                    if ((v.StartsWith("{") && v.EndsWith("}"))
                        || v.EndsWith("[]")
                        || v.EndsWith("[,]")
                        )
                    {
                        r.Cells[2].ReadOnly = true;
                        r.Cells[2].Style = CSytleNotChange;
                    }

                }
            }
        }

        private void chkLayer2Visible_Click(object sender, EventArgs e)
        {
            this.picMap.Refresh();
        }

        private void chkGridVisible_CheckedChanged(object sender, EventArgs e)
        {
            this.picMap.Refresh();
        }

        private void chkTerrainResource_CheckedChanged(object sender, EventArgs e)
        {
            this.picMap.Refresh();
        }

        private void tvEntity_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != null && !this.MouseLDown)
            {
                var key = string.Format("{0}", e.Node.Tag);
                if (!string.IsNullOrWhiteSpace(key))
                {
                    var item = this.MapInfo.EntitySource.FirstOrDefault(a => a.Key == key);

                    var x = item.Rect.Left + item.Rect.Width / 2;
                    var y = item.Rect.Top + item.Rect.Height / 2;

                    this.SetScrollPostion(x * this.OffsetSize - this.pnlMap.Width / 2, y * this.OffsetSize - this.pnlMap.Height / 2);

                    this.FindEntity(x, y);
                    this.Refresh();
                }
            }
        }

        private void tvEntity_DoubleClick(object sender, EventArgs e)
        {
            if (this.tvEntity.SelectedNode != null)
            {
                var path = string.Format(@"\LevelState\LevelEntities\{0}", this.tvEntity.SelectedNode.Tag);
                this.tvSaveDataSetPath(path);
            }
        }

        private void tvSaveData_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tvSaveData.SelectedNode != null)
            {
                var nd = tvSaveData.SelectedNode;
                var ndPath = nd.Name;
                var p = this.SaveHelper.GetProperty(ndPath);
                var dt = this.SaveHelper.GetChildDataTable(p, true);
                dt.Columns.Remove("Property");
                this.dgvData.DataSource = dt;
                this.txtPath.Text = ndPath;
            }
        }

        private void tvSaveDataSetPath(string path)
        {
            var ndArray = tvSaveData.Nodes.Find(path, true);
            if (ndArray != null && ndArray.Length > 0)
            {
                this.tvSaveData.SelectedNode = ndArray[0];
                this.tvSaveData_AfterSelect(null, null);

                if (this.TC.SelectedTab != this.tpData)
                {
                    this.TC.SelectedTab = this.tpData;
                }
            }
        }

        private int AddNode(TreeNodeCollection nds, string path, string name, Property p)
        {
            if (p is DictionaryProperty)
            {
                var nd = nds.Add(path, string.Format("{0}[{1}]", name, ((DictionaryProperty)p).Items.Count));
                var c = 0;
                foreach (var i in ((DictionaryProperty)p).Items)
                {
                    var key = $"{((SimpleProperty)i.Key).Value}";
                    this.AddNode(nd.Nodes, string.Format("{0}\\{1}", path, c), string.Format("[{0}] {1}", c, i.Value.Type.Name), i.Value);
                    c++;
                }
                return 1;
            }
            else if (p is CollectionProperty)
            {
                var nd = nds.Add(path, string.Format("{0}[{1}]", name, ((CollectionProperty)p).Items.Count));
                var c = 0;
                foreach (var i in ((CollectionProperty)p).Items)
                {
                    this.AddNode(nd.Nodes, string.Format("{0}\\{1}", path, c), string.Format("[{0}] {1}", c, i.Type.Name), i);
                    c++;
                }
                return 1;
            }
            else if (p is SingleDimensionalArrayProperty)
            {
                var nd = nds.Add(path, string.Format("{0}[{1}]", name, ((SingleDimensionalArrayProperty)p).Items.Count));
                var c = 0;
                foreach (var i in ((SingleDimensionalArrayProperty)p).Items)
                {
                    this.AddNode(nd.Nodes, string.Format("{0}\\{1}", path, c), string.Format("[{0}] {1}", c, i.Type.Name), i);
                    c++;
                }
                return 1;
            }
            else if (p is ComplexProperty)
            {
                var nd = nds.Add(path, string.Format("{0}[{1}]", name, ((ComplexProperty)p).Properties.Count));
                var c = 0;
                foreach (var i in ((ComplexProperty)p).Properties)
                {
                    c += this.AddNode(nd.Nodes, string.Format("{0}\\{1}", path, i.Name), i.Name, i);
                }
                return c;
            }
            else if (p is SimpleProperty || p is NullProperty)
            {
                return 0;
            }
            else
            {
                return 0;
            }
        }

        private void TC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.TC.SelectedTab == this.tpMap)
            {
                this.picMap.Refresh();
            }
            else if (this.TC.SelectedTab == this.tpEvent)
            {
                this.dgvEvent.DataSource = this.SaveHelper.ConvertToEventData(this.MapInfo.EventList);
            }
            else if (this.TC.SelectedTab == this.tpData)
            {
                this.tvSaveDataSetPath(this.txtPath.Text);
            }
        }

        private void pnlMap_Scroll(object sender, ScrollEventArgs e)
        {
            this.timerTerrainResourceRefresh.Enabled = true;
        }

        private void picTerrainResource_MouseDown(object sender, MouseEventArgs e)
        {
            float ratioWidth = this.picMap.Width / this.picTerrainResource.Width;
            float ratioHeight = this.picMap.Height / this.picTerrainResource.Height;

            this.SetScrollPostion(e.X * ratioWidth - this.pnlMap.Width / 2, e.Y * ratioHeight - this.pnlMap.Height / 2);
        }

        private void SetScrollPostion(float x, float y)
        {
            this.pnlMap.AutoScrollPosition = new Point((int)x, (int)y);
            this.timerTerrainResourceRefresh.Enabled = true;
        }

        private void timerTerrainResourceRefresh_Tick(object sender, EventArgs e)
        {
            this.timerTerrainResourceRefresh.Enabled = false;
            this.picTerrainResource.Refresh();
        }

        private void tsEditEvent_Click(object sender, EventArgs e)
        {
            if (int.TryParse(string.Format("{0}", this.cmsEvent.Tag), out int id))
            {
                var index = this.MapInfo.EventList.FindIndex(a => a.id == id);
                if (index != -1)
                {
                    this.MapInfo.EventList[index] = frmEventEdit.Show(this.MapInfo.EventList[index]);
                    this.dgvEvent.DataSource = this.SaveHelper.ConvertToEventData(this.MapInfo.EventList);
                }
            }
        }

        private void tsInsertEvent_Click(object sender, EventArgs e)
        {
            if (int.TryParse(string.Format("{0}", this.cmsEvent.Tag), out int id))
            {
                var item = new EventInfo();
                item.id = this.MapInfo.EventList.Count > 0 ? this.MapInfo.EventList.Max(a => a.id) + 1 : 1;
                this.MapInfo.EventList.Add(item);
                this.dgvEvent.DataSource = this.SaveHelper.ConvertToEventData(this.MapInfo.EventList);
            }
        }

        private void tsDeleteEvent_Click(object sender, EventArgs e)
        {
            if (int.TryParse(string.Format("{0}", this.cmsEvent.Tag), out int id))
            {
                var item = this.MapInfo.EventList.FirstOrDefault(a => a.id == id);

                if (item != null)
                {
                    this.MapInfo.EventList.Remove(item);
                    this.dgvEvent.DataSource = this.SaveHelper.ConvertToEventData(this.MapInfo.EventList);
                }
            }
        }

        private void RefreshSchedule()
        {
            var dt = new DataTable();
            dt.Columns.Add("时间", typeof(string));
            dt.Columns.Add("类型", typeof(string));
            dt.Columns.Add("数量估计", typeof(string));

            foreach (var i in this.MapInfo.EventList)
            {
                var starth = i.StartTimeH;
                var repeath = i.RepeatTimeH;

                for (var f = 0; f < i.MaxRepetitions; f++)
                {
                    float min = 0;
                    float max = 0;

                    if (!string.IsNullOrWhiteSpace(i.EntityType1.UnitName))
                    {
                        min += (i.EntityType1.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType1.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType2.UnitName))
                    {
                        min += (i.EntityType2.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType2.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType3.UnitName))
                    {
                        min += (i.EntityType3.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType3.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType4.UnitName))
                    {
                        min += (i.EntityType4.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType4.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType5.UnitName))
                    {
                        min += (i.EntityType5.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType5.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType6.UnitName))
                    {
                        min += (i.EntityType6.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType6.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType7.UnitName))
                    {
                        min += (i.EntityType7.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType7.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType8.UnitName))
                    {
                        min += (i.EntityType8.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType8.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType9.UnitName))
                    {
                        min += (i.EntityType9.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType9.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (!string.IsNullOrWhiteSpace(i.EntityType10.UnitName))
                    {
                        min += (i.EntityType10.Min * (1 + f * i.FactorUnitsNumberPerRepetition));
                        max += (i.EntityType10.Max * (1 + f * i.FactorUnitsNumberPerRepetition));
                    }

                    if (i.Generators.IndexOf("&") != -1)
                    {
                        min *= 4;
                        max *= 4;
                    }

                    dt.Rows.Add(
                        string.Format("{0:000} d {1:00} h", (starth + repeath * f) / 24, (starth + repeath * f) % 24)
                        , i.GameWon ? "[游戏胜利]"
                            : i.FinalSwarm ? "最终进攻"
                            : i.TimeToNotifyInAdvanceH != 0 ? "僵尸进攻"
                            : "生成"
                        , min == 0 && max == 0 ? "" : string.Format("{0:0}-{1:0}", min, max)
                    );
                }

                if (i.MaxRepetitions == 0)
                {
                    dt.Rows.Add(
                        string.Format("{0:000} d {1:00} h", starth / 24, starth % 24)
                        , i.GameWon ? "[游戏胜利]"
                            : i.FinalSwarm ? "最终进攻"
                            : i.TimeToNotifyInAdvanceH != 0 ? "僵尸进攻"
                            : "生成"
                        , ""
                    );
                }
            }

            this.dgvSchedule.DataSource = dt;
            this.dgvSchedule.AutoResizeColumns();
            this.dgvSchedule.Sort(this.dgvSchedule.Columns[0], ListSortDirection.Ascending);
        }

    }
}
