﻿namespace ZX.Data.View
{
    partial class frmLevelView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TSC = new System.Windows.Forms.ToolStripContainer();
            this.TC = new System.Windows.Forms.TabControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsPathText = new System.Windows.Forms.ToolStripTextBox();
            this.tsPath = new System.Windows.Forms.ToolStripMenuItem();
            this.TSC.ContentPanel.SuspendLayout();
            this.TSC.TopToolStripPanel.SuspendLayout();
            this.TSC.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TSC
            // 
            // 
            // TSC.ContentPanel
            // 
            this.TSC.ContentPanel.Controls.Add(this.TC);
            this.TSC.ContentPanel.Size = new System.Drawing.Size(884, 434);
            this.TSC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TSC.Location = new System.Drawing.Point(0, 0);
            this.TSC.Name = "TSC";
            this.TSC.Size = new System.Drawing.Size(884, 461);
            this.TSC.TabIndex = 0;
            this.TSC.Text = "toolStripContainer1";
            // 
            // TSC.TopToolStripPanel
            // 
            this.TSC.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // TC
            // 
            this.TC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TC.Location = new System.Drawing.Point(0, 0);
            this.TC.Name = "TC";
            this.TC.SelectedIndex = 0;
            this.TC.Size = new System.Drawing.Size(884, 434);
            this.TC.TabIndex = 0;
            this.TC.SelectedIndexChanged += new System.EventHandler(this.TC_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsPath,
            this.tsPathText});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(884, 27);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsPathText
            // 
            this.tsPathText.Name = "tsPathText";
            this.tsPathText.Size = new System.Drawing.Size(800, 23);
            // 
            // tsPath
            // 
            this.tsPath.Name = "tsPath";
            this.tsPath.Size = new System.Drawing.Size(45, 23);
            this.tsPath.Text = "Path";
            // 
            // frmLevelView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.TSC);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmLevelView";
            this.Text = "SaveDataView";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmLevelView_KeyUp);
            this.TSC.ContentPanel.ResumeLayout(false);
            this.TSC.TopToolStripPanel.ResumeLayout(false);
            this.TSC.TopToolStripPanel.PerformLayout();
            this.TSC.ResumeLayout(false);
            this.TSC.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer TSC;
        private System.Windows.Forms.TabControl TC;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsPath;
        private System.Windows.Forms.ToolStripTextBox tsPathText;
    }
}