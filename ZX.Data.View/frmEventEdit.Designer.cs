﻿namespace ZX.Data.View
{
    partial class frmEventEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nudStartTimeH = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nudRepeatTimeH = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.nudMaxRepetitions = new System.Windows.Forms.NumericUpDown();
            this.nudTimeToNotifyInAdvanceH = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkFinalSwarm = new System.Windows.Forms.CheckBox();
            this.nudFactorUnitsNumberPerRepetition = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nudMaxFactorUnitsNumberPerRepetition = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.cboEntityType1 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cboEntityType2 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboEntityType4 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboEntityType3 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboEntityType5 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboEntityType10 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cboEntityType9 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cboEntityType8 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cboEntityType7 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboEntityType6 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.nudEntityType1Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType1Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType2Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType2Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType3Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType3Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType6Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType6Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType5Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType5Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType4Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType4Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType9Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType9Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType8Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType8Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType7Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType7Min = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType10Max = new System.Windows.Forms.NumericUpDown();
            this.nudEntityType10Min = new System.Windows.Forms.NumericUpDown();
            this.chkGenerators = new System.Windows.Forms.CheckBox();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.chkGameWon = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudStartTimeH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepeatTimeH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxRepetitions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeToNotifyInAdvanceH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFactorUnitsNumberPerRepetition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxFactorUnitsNumberPerRepetition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType6Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType6Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType5Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType5Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType9Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType9Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType8Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType8Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType7Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType7Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType10Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType10Min)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "事件触发时间";
            // 
            // nudStartTimeH
            // 
            this.nudStartTimeH.Location = new System.Drawing.Point(139, 14);
            this.nudStartTimeH.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudStartTimeH.Name = "nudStartTimeH";
            this.nudStartTimeH.Size = new System.Drawing.Size(120, 21);
            this.nudStartTimeH.TabIndex = 1;
            this.nudStartTimeH.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "再次触发间隔时间";
            // 
            // nudRepeatTimeH
            // 
            this.nudRepeatTimeH.Location = new System.Drawing.Point(139, 40);
            this.nudRepeatTimeH.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudRepeatTimeH.Name = "nudRepeatTimeH";
            this.nudRepeatTimeH.Size = new System.Drawing.Size(120, 21);
            this.nudRepeatTimeH.TabIndex = 1;
            this.nudRepeatTimeH.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "触发次数";
            // 
            // nudMaxRepetitions
            // 
            this.nudMaxRepetitions.Location = new System.Drawing.Point(139, 66);
            this.nudMaxRepetitions.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudMaxRepetitions.Name = "nudMaxRepetitions";
            this.nudMaxRepetitions.Size = new System.Drawing.Size(120, 21);
            this.nudMaxRepetitions.TabIndex = 4;
            this.nudMaxRepetitions.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudTimeToNotifyInAdvanceH
            // 
            this.nudTimeToNotifyInAdvanceH.Location = new System.Drawing.Point(139, 144);
            this.nudTimeToNotifyInAdvanceH.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudTimeToNotifyInAdvanceH.Name = "nudTimeToNotifyInAdvanceH";
            this.nudTimeToNotifyInAdvanceH.Size = new System.Drawing.Size(120, 21);
            this.nudTimeToNotifyInAdvanceH.TabIndex = 6;
            this.nudTimeToNotifyInAdvanceH.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "事件发生前提示时间";
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(139, 171);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(252, 46);
            this.txtMessage.TabIndex = 7;
            this.txtMessage.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "事件提示信息";
            // 
            // chkFinalSwarm
            // 
            this.chkFinalSwarm.AutoSize = true;
            this.chkFinalSwarm.Location = new System.Drawing.Point(118, 227);
            this.chkFinalSwarm.Name = "chkFinalSwarm";
            this.chkFinalSwarm.Size = new System.Drawing.Size(216, 16);
            this.chkFinalSwarm.TabIndex = 9;
            this.chkFinalSwarm.Text = "[最终波]引发所有僵尸进击指挥中心";
            this.chkFinalSwarm.UseVisualStyleBackColor = true;
            this.chkFinalSwarm.CheckedChanged += new System.EventHandler(this.Changed);
            // 
            // nudFactorUnitsNumberPerRepetition
            // 
            this.nudFactorUnitsNumberPerRepetition.DecimalPlaces = 2;
            this.nudFactorUnitsNumberPerRepetition.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudFactorUnitsNumberPerRepetition.Location = new System.Drawing.Point(139, 92);
            this.nudFactorUnitsNumberPerRepetition.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudFactorUnitsNumberPerRepetition.Name = "nudFactorUnitsNumberPerRepetition";
            this.nudFactorUnitsNumberPerRepetition.Size = new System.Drawing.Size(120, 21);
            this.nudFactorUnitsNumberPerRepetition.TabIndex = 10;
            this.nudFactorUnitsNumberPerRepetition.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "重复触发单位增量";
            // 
            // nudMaxFactorUnitsNumberPerRepetition
            // 
            this.nudMaxFactorUnitsNumberPerRepetition.Location = new System.Drawing.Point(139, 118);
            this.nudMaxFactorUnitsNumberPerRepetition.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudMaxFactorUnitsNumberPerRepetition.Name = "nudMaxFactorUnitsNumberPerRepetition";
            this.nudMaxFactorUnitsNumberPerRepetition.Size = new System.Drawing.Size(120, 21);
            this.nudMaxFactorUnitsNumberPerRepetition.TabIndex = 12;
            this.nudMaxFactorUnitsNumberPerRepetition.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(56, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "最大单位增量";
            // 
            // cboEntityType1
            // 
            this.cboEntityType1.FormattingEnabled = true;
            this.cboEntityType1.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType1.Location = new System.Drawing.Point(485, 14);
            this.cboEntityType1.Name = "cboEntityType1";
            this.cboEntityType1.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType1.TabIndex = 14;
            this.cboEntityType1.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(418, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "生成单位1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(418, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 17;
            this.label9.Text = "生成单位2";
            // 
            // cboEntityType2
            // 
            this.cboEntityType2.FormattingEnabled = true;
            this.cboEntityType2.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType2.Location = new System.Drawing.Point(485, 40);
            this.cboEntityType2.Name = "cboEntityType2";
            this.cboEntityType2.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType2.TabIndex = 16;
            this.cboEntityType2.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(418, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 12);
            this.label10.TabIndex = 21;
            this.label10.Text = "生成单位4";
            // 
            // cboEntityType4
            // 
            this.cboEntityType4.FormattingEnabled = true;
            this.cboEntityType4.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType4.Location = new System.Drawing.Point(485, 92);
            this.cboEntityType4.Name = "cboEntityType4";
            this.cboEntityType4.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType4.TabIndex = 20;
            this.cboEntityType4.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(418, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 12);
            this.label11.TabIndex = 19;
            this.label11.Text = "生成单位3";
            // 
            // cboEntityType3
            // 
            this.cboEntityType3.FormattingEnabled = true;
            this.cboEntityType3.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType3.Location = new System.Drawing.Point(485, 66);
            this.cboEntityType3.Name = "cboEntityType3";
            this.cboEntityType3.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType3.TabIndex = 18;
            this.cboEntityType3.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(418, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 12);
            this.label13.TabIndex = 23;
            this.label13.Text = "生成单位5";
            // 
            // cboEntityType5
            // 
            this.cboEntityType5.FormattingEnabled = true;
            this.cboEntityType5.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType5.Location = new System.Drawing.Point(485, 118);
            this.cboEntityType5.Name = "cboEntityType5";
            this.cboEntityType5.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType5.TabIndex = 22;
            this.cboEntityType5.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(418, 252);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 33;
            this.label12.Text = "生成单位10";
            // 
            // cboEntityType10
            // 
            this.cboEntityType10.FormattingEnabled = true;
            this.cboEntityType10.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType10.Location = new System.Drawing.Point(485, 248);
            this.cboEntityType10.Name = "cboEntityType10";
            this.cboEntityType10.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType10.TabIndex = 32;
            this.cboEntityType10.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(418, 226);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 12);
            this.label14.TabIndex = 31;
            this.label14.Text = "生成单位9";
            // 
            // cboEntityType9
            // 
            this.cboEntityType9.FormattingEnabled = true;
            this.cboEntityType9.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType9.Location = new System.Drawing.Point(485, 222);
            this.cboEntityType9.Name = "cboEntityType9";
            this.cboEntityType9.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType9.TabIndex = 30;
            this.cboEntityType9.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(418, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 12);
            this.label15.TabIndex = 29;
            this.label15.Text = "生成单位8";
            // 
            // cboEntityType8
            // 
            this.cboEntityType8.FormattingEnabled = true;
            this.cboEntityType8.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType8.Location = new System.Drawing.Point(485, 196);
            this.cboEntityType8.Name = "cboEntityType8";
            this.cboEntityType8.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType8.TabIndex = 28;
            this.cboEntityType8.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(418, 174);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 12);
            this.label16.TabIndex = 27;
            this.label16.Text = "生成单位7";
            // 
            // cboEntityType7
            // 
            this.cboEntityType7.FormattingEnabled = true;
            this.cboEntityType7.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType7.Location = new System.Drawing.Point(485, 170);
            this.cboEntityType7.Name = "cboEntityType7";
            this.cboEntityType7.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType7.TabIndex = 26;
            this.cboEntityType7.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(418, 148);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 12);
            this.label17.TabIndex = 25;
            this.label17.Text = "生成单位6";
            // 
            // cboEntityType6
            // 
            this.cboEntityType6.FormattingEnabled = true;
            this.cboEntityType6.Items.AddRange(new object[] {
            "",
            "ZombieWeakA",
            "ZombieWeakB",
            "ZombieWeakC",
            "ZombieWorkerA",
            "ZombieWorkerB",
            "ZombieMediumA",
            "ZombieMediumB",
            "ZombieDressedA",
            "ZombieStrongA",
            "ZombieGiant",
            "ZombieHarpy",
            "ZombieVenom",
            "ZombieLeader"});
            this.cboEntityType6.Location = new System.Drawing.Point(485, 144);
            this.cboEntityType6.Name = "cboEntityType6";
            this.cboEntityType6.Size = new System.Drawing.Size(149, 20);
            this.cboEntityType6.TabIndex = 24;
            this.cboEntityType6.TextChanged += new System.EventHandler(this.Changed);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(700, 274);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 35);
            this.button1.TabIndex = 34;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // nudEntityType1Min
            // 
            this.nudEntityType1Min.Location = new System.Drawing.Point(640, 14);
            this.nudEntityType1Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType1Min.Name = "nudEntityType1Min";
            this.nudEntityType1Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType1Min.TabIndex = 35;
            this.nudEntityType1Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType1Max
            // 
            this.nudEntityType1Max.Location = new System.Drawing.Point(717, 14);
            this.nudEntityType1Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType1Max.Name = "nudEntityType1Max";
            this.nudEntityType1Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType1Max.TabIndex = 36;
            this.nudEntityType1Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType2Max
            // 
            this.nudEntityType2Max.Location = new System.Drawing.Point(717, 40);
            this.nudEntityType2Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType2Max.Name = "nudEntityType2Max";
            this.nudEntityType2Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType2Max.TabIndex = 38;
            this.nudEntityType2Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType2Min
            // 
            this.nudEntityType2Min.Location = new System.Drawing.Point(640, 40);
            this.nudEntityType2Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType2Min.Name = "nudEntityType2Min";
            this.nudEntityType2Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType2Min.TabIndex = 37;
            this.nudEntityType2Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType3Max
            // 
            this.nudEntityType3Max.Location = new System.Drawing.Point(717, 66);
            this.nudEntityType3Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType3Max.Name = "nudEntityType3Max";
            this.nudEntityType3Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType3Max.TabIndex = 40;
            this.nudEntityType3Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType3Min
            // 
            this.nudEntityType3Min.Location = new System.Drawing.Point(640, 66);
            this.nudEntityType3Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType3Min.Name = "nudEntityType3Min";
            this.nudEntityType3Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType3Min.TabIndex = 39;
            this.nudEntityType3Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType6Max
            // 
            this.nudEntityType6Max.Location = new System.Drawing.Point(717, 143);
            this.nudEntityType6Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType6Max.Name = "nudEntityType6Max";
            this.nudEntityType6Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType6Max.TabIndex = 46;
            this.nudEntityType6Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType6Min
            // 
            this.nudEntityType6Min.Location = new System.Drawing.Point(640, 143);
            this.nudEntityType6Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType6Min.Name = "nudEntityType6Min";
            this.nudEntityType6Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType6Min.TabIndex = 45;
            this.nudEntityType6Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType5Max
            // 
            this.nudEntityType5Max.Location = new System.Drawing.Point(717, 117);
            this.nudEntityType5Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType5Max.Name = "nudEntityType5Max";
            this.nudEntityType5Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType5Max.TabIndex = 44;
            this.nudEntityType5Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType5Min
            // 
            this.nudEntityType5Min.Location = new System.Drawing.Point(640, 117);
            this.nudEntityType5Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType5Min.Name = "nudEntityType5Min";
            this.nudEntityType5Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType5Min.TabIndex = 43;
            this.nudEntityType5Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType4Max
            // 
            this.nudEntityType4Max.Location = new System.Drawing.Point(717, 91);
            this.nudEntityType4Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType4Max.Name = "nudEntityType4Max";
            this.nudEntityType4Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType4Max.TabIndex = 42;
            this.nudEntityType4Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType4Min
            // 
            this.nudEntityType4Min.Location = new System.Drawing.Point(640, 91);
            this.nudEntityType4Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType4Min.Name = "nudEntityType4Min";
            this.nudEntityType4Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType4Min.TabIndex = 41;
            this.nudEntityType4Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType9Max
            // 
            this.nudEntityType9Max.Location = new System.Drawing.Point(717, 222);
            this.nudEntityType9Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType9Max.Name = "nudEntityType9Max";
            this.nudEntityType9Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType9Max.TabIndex = 52;
            this.nudEntityType9Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType9Min
            // 
            this.nudEntityType9Min.Location = new System.Drawing.Point(640, 222);
            this.nudEntityType9Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType9Min.Name = "nudEntityType9Min";
            this.nudEntityType9Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType9Min.TabIndex = 51;
            this.nudEntityType9Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType8Max
            // 
            this.nudEntityType8Max.Location = new System.Drawing.Point(717, 196);
            this.nudEntityType8Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType8Max.Name = "nudEntityType8Max";
            this.nudEntityType8Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType8Max.TabIndex = 50;
            this.nudEntityType8Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType8Min
            // 
            this.nudEntityType8Min.Location = new System.Drawing.Point(640, 196);
            this.nudEntityType8Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType8Min.Name = "nudEntityType8Min";
            this.nudEntityType8Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType8Min.TabIndex = 49;
            this.nudEntityType8Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType7Max
            // 
            this.nudEntityType7Max.Location = new System.Drawing.Point(717, 170);
            this.nudEntityType7Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType7Max.Name = "nudEntityType7Max";
            this.nudEntityType7Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType7Max.TabIndex = 48;
            this.nudEntityType7Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType7Min
            // 
            this.nudEntityType7Min.Location = new System.Drawing.Point(640, 170);
            this.nudEntityType7Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType7Min.Name = "nudEntityType7Min";
            this.nudEntityType7Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType7Min.TabIndex = 47;
            this.nudEntityType7Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType10Max
            // 
            this.nudEntityType10Max.Location = new System.Drawing.Point(717, 247);
            this.nudEntityType10Max.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType10Max.Name = "nudEntityType10Max";
            this.nudEntityType10Max.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType10Max.TabIndex = 54;
            this.nudEntityType10Max.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // nudEntityType10Min
            // 
            this.nudEntityType10Min.Location = new System.Drawing.Point(640, 247);
            this.nudEntityType10Min.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudEntityType10Min.Name = "nudEntityType10Min";
            this.nudEntityType10Min.Size = new System.Drawing.Size(72, 21);
            this.nudEntityType10Min.TabIndex = 53;
            this.nudEntityType10Min.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // chkGenerators
            // 
            this.chkGenerators.AutoSize = true;
            this.chkGenerators.Location = new System.Drawing.Point(118, 249);
            this.chkGenerators.Name = "chkGenerators";
            this.chkGenerators.Size = new System.Drawing.Size(120, 16);
            this.chkGenerators.TabIndex = 56;
            this.chkGenerators.Text = "所有方向同时进攻";
            this.chkGenerators.UseVisualStyleBackColor = true;
            this.chkGenerators.CheckedChanged += new System.EventHandler(this.Changed);
            // 
            // txtInfo
            // 
            this.txtInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInfo.BackColor = System.Drawing.Color.White;
            this.txtInfo.Location = new System.Drawing.Point(12, 321);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ReadOnly = true;
            this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInfo.Size = new System.Drawing.Size(777, 128);
            this.txtInfo.TabIndex = 57;
            this.txtInfo.WordWrap = false;
            // 
            // chkGameWon
            // 
            this.chkGameWon.AutoSize = true;
            this.chkGameWon.Location = new System.Drawing.Point(118, 271);
            this.chkGameWon.Name = "chkGameWon";
            this.chkGameWon.Size = new System.Drawing.Size(96, 16);
            this.chkGameWon.TabIndex = 58;
            this.chkGameWon.Text = "游戏胜利事件";
            this.chkGameWon.UseVisualStyleBackColor = true;
            this.chkGameWon.CheckedChanged += new System.EventHandler(this.Changed);
            // 
            // frmEventEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 461);
            this.Controls.Add(this.chkGameWon);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.chkGenerators);
            this.Controls.Add(this.nudEntityType10Max);
            this.Controls.Add(this.nudEntityType10Min);
            this.Controls.Add(this.nudEntityType9Max);
            this.Controls.Add(this.nudEntityType9Min);
            this.Controls.Add(this.nudEntityType8Max);
            this.Controls.Add(this.nudEntityType8Min);
            this.Controls.Add(this.nudEntityType7Max);
            this.Controls.Add(this.nudEntityType7Min);
            this.Controls.Add(this.nudEntityType6Max);
            this.Controls.Add(this.nudEntityType6Min);
            this.Controls.Add(this.nudEntityType5Max);
            this.Controls.Add(this.nudEntityType5Min);
            this.Controls.Add(this.nudEntityType4Max);
            this.Controls.Add(this.nudEntityType4Min);
            this.Controls.Add(this.nudEntityType3Max);
            this.Controls.Add(this.nudEntityType3Min);
            this.Controls.Add(this.nudEntityType2Max);
            this.Controls.Add(this.nudEntityType2Min);
            this.Controls.Add(this.nudEntityType1Max);
            this.Controls.Add(this.nudEntityType1Min);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cboEntityType10);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cboEntityType9);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cboEntityType8);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.cboEntityType7);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cboEntityType6);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cboEntityType5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cboEntityType4);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cboEntityType3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cboEntityType2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cboEntityType1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.nudMaxFactorUnitsNumberPerRepetition);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nudFactorUnitsNumberPerRepetition);
            this.Controls.Add(this.chkFinalSwarm);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.nudTimeToNotifyInAdvanceH);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nudMaxRepetitions);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nudRepeatTimeH);
            this.Controls.Add(this.nudStartTimeH);
            this.Controls.Add(this.label1);
            this.Name = "frmEventEdit";
            this.Text = "事件编辑";
            ((System.ComponentModel.ISupportInitialize)(this.nudStartTimeH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepeatTimeH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxRepetitions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeToNotifyInAdvanceH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFactorUnitsNumberPerRepetition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxFactorUnitsNumberPerRepetition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType6Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType6Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType5Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType5Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType9Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType9Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType8Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType8Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType7Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType7Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType10Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEntityType10Min)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudStartTimeH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudRepeatTimeH;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudMaxRepetitions;
        private System.Windows.Forms.NumericUpDown nudTimeToNotifyInAdvanceH;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkFinalSwarm;
        private System.Windows.Forms.NumericUpDown nudFactorUnitsNumberPerRepetition;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudMaxFactorUnitsNumberPerRepetition;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboEntityType1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboEntityType2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboEntityType4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboEntityType3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboEntityType5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cboEntityType10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboEntityType9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboEntityType8;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboEntityType7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboEntityType6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown nudEntityType1Min;
        private System.Windows.Forms.NumericUpDown nudEntityType1Max;
        private System.Windows.Forms.NumericUpDown nudEntityType2Max;
        private System.Windows.Forms.NumericUpDown nudEntityType2Min;
        private System.Windows.Forms.NumericUpDown nudEntityType3Max;
        private System.Windows.Forms.NumericUpDown nudEntityType3Min;
        private System.Windows.Forms.NumericUpDown nudEntityType6Max;
        private System.Windows.Forms.NumericUpDown nudEntityType6Min;
        private System.Windows.Forms.NumericUpDown nudEntityType5Max;
        private System.Windows.Forms.NumericUpDown nudEntityType5Min;
        private System.Windows.Forms.NumericUpDown nudEntityType4Max;
        private System.Windows.Forms.NumericUpDown nudEntityType4Min;
        private System.Windows.Forms.NumericUpDown nudEntityType9Max;
        private System.Windows.Forms.NumericUpDown nudEntityType9Min;
        private System.Windows.Forms.NumericUpDown nudEntityType8Max;
        private System.Windows.Forms.NumericUpDown nudEntityType8Min;
        private System.Windows.Forms.NumericUpDown nudEntityType7Max;
        private System.Windows.Forms.NumericUpDown nudEntityType7Min;
        private System.Windows.Forms.NumericUpDown nudEntityType10Max;
        private System.Windows.Forms.NumericUpDown nudEntityType10Min;
        private System.Windows.Forms.CheckBox chkGenerators;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.CheckBox chkGameWon;
    }
}