﻿using DXVision.Serialization;
using DXVision.Serialization.Advanced;
using DXVision.Serialization.Advanced.Xml;
using DXVision.Serialization.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ZX.Data.View
{
    //反序列化
    public partial class TestCore
    {
        private DefaultXmlReader _reader;
        private DefaultXmlWriter _writer;

        public Property Deserialize()
        {
            PropertyTag propertyTag = getPropertyTag(this._reader.ReadElement());
            if (propertyTag == PropertyTag.Unknown)
            {
                return null;
            }
            return this.deserialize(propertyTag, null);
        }

        private enum PropertyTag
        {
            Unknown,
            Simple,
            Complex,
            Collection,
            Dictionary,
            SingleArray,
            MultiArray,
            Null
        }

        private static PropertyTag getPropertyTag(string name)
        {
            if (name == "Simple")
            {
                return PropertyTag.Simple;
            }
            if (name == "Complex")
            {
                return PropertyTag.Complex;
            }
            if (name == "Collection")
            {
                return PropertyTag.Collection;
            }
            if (name == "SingleArray")
            {
                return PropertyTag.SingleArray;
            }
            if (name == "Null")
            {
                return PropertyTag.Null;
            }
            if (name == "Dictionary")
            {
                return PropertyTag.Dictionary;
            }
            if (name == "MultiArray")
            {
                return PropertyTag.MultiArray;
            }
            return PropertyTag.Unknown;
        }

        private Property deserialize(PropertyTag propertyTag, Type expectedType)
        {
            string attributeAsString = this._reader.GetAttributeAsString("name");

            Type attributeAsType = this._reader.GetAttributeAsType("type");
            if (attributeAsType == null)
            {
                attributeAsType = expectedType ?? typeof(object);
                if (expectedType != null && expectedType.IsAbstract)
                {
                    attributeAsType = expectedType = typeof(object);
                }
            }
            Property property = createProperty(propertyTag, attributeAsString, attributeAsType);

            string typeName = this._reader.GetAttributeAsString("type");
            if (!string.IsNullOrWhiteSpace(typeName) && typeName.IndexOf("TheyAreBillions") != -1 && property is ComplexProperty)
            {   //无法创建的类型, 记录原始类型
                ((ComplexProperty)property).Properties.Add(new SimpleProperty("oType", typeof(string)) { Value = typeName });
            }
            typeName = this._reader.GetAttributeAsString("elementType");
            if (!string.IsNullOrWhiteSpace(typeName) && property is ComplexProperty)
            {   //记录原始类型
                ((ComplexProperty)property).Properties.Add(new SimpleProperty("oElementType", typeof(string)) { Value = typeName });
            }

            if (property == null)
            {
                return null;
            }
            NullProperty property2 = property as NullProperty;
            if (property2 != null)
            {
                return property2;
            }
            SimpleProperty property3 = property as SimpleProperty;
            if (property3 != null)
            {
                this.parseSimpleProperty(this._reader, property3);
                return property3;
            }
            MultiDimensionalArrayProperty property4 = property as MultiDimensionalArrayProperty;
            if (property4 != null)
            {
                this.parseMultiDimensionalArrayProperty(property4);
                return property4;
            }
            SingleDimensionalArrayProperty property5 = property as SingleDimensionalArrayProperty;
            if (property5 != null)
            {
                this.parseSingleDimensionalArrayProperty(property5);
                return property5;
            }
            DictionaryProperty property6 = property as DictionaryProperty;
            if (property6 != null)
            {
                this.parseDictionaryProperty(property6);
                return property6;
            }
            CollectionProperty property7 = property as CollectionProperty;
            if (property7 != null)
            {
                this.parseCollectionProperty(property7);
                return property7;
            }
            ComplexProperty property8 = property as ComplexProperty;
            if (property8 != null)
            {
                this.parseComplexProperty(property8);
                return property8;
            }
            return property;
        }

        private static Property createProperty(PropertyTag tag, string propertyName, Type propertyType)
        {
            switch (tag)
            {
                case PropertyTag.Simple:
                    return new SimpleProperty(propertyName, propertyType);

                case PropertyTag.Complex:
                    return new ComplexProperty(propertyName, propertyType);

                case PropertyTag.Collection:
                    return new CollectionProperty(propertyName, propertyType);

                case PropertyTag.Dictionary:
                    return new DictionaryProperty(propertyName, propertyType);

                case PropertyTag.SingleArray:
                    return new SingleDimensionalArrayProperty(propertyName, propertyType);

                case PropertyTag.MultiArray:
                    return new MultiDimensionalArrayProperty(propertyName, propertyType);

                case PropertyTag.Null:
                    return new NullProperty(propertyName);
            }
            return null;
        }

        private void parseSimpleProperty(IXmlReader reader, SimpleProperty property)
        {
            property.Value = this._reader.GetAttributeAsObject("value", property.Type);
        }

        private void parseMultiDimensionalArrayProperty(MultiDimensionalArrayProperty property)
        {
            property.ElementType = this._reader.GetAttributeAsType("elementType");
            foreach (string local1 in this._reader.ReadSubElements())
            {
                if (local1 == "Dimensions")
                {
                    this.readDimensionInfos(property.DimensionInfos);
                }
                if (local1 == "Items")
                {
                    this.readMultiDimensionalArrayItems(property.Items, property.ElementType);
                }
            }
        }

        private void readDimensionInfos(IList<DimensionInfo> dimensionInfos)
        {
            using (IEnumerator<string> enumerator = this._reader.ReadSubElements().GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current == "Dimension")
                    {
                        this.readDimensionInfo(dimensionInfos);
                    }
                }
            }
        }

        private void readDimensionInfo(IList<DimensionInfo> dimensionInfos)
        {
            DimensionInfo item = new DimensionInfo
            {
                Length = this._reader.GetAttributeAsInt("length"),
                LowerBound = this._reader.GetAttributeAsInt("lowerBound")
            };
            dimensionInfos.Add(item);
        }

        private void readMultiDimensionalArrayItems(IList<MultiDimensionalArrayItem> items, Type expectedElementType)
        {
            using (IEnumerator<string> enumerator = this._reader.ReadSubElements().GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current == "Item")
                    {
                        this.readMultiDimensionalArrayItem(items, expectedElementType);
                    }
                }
            }
        }

        private void readMultiDimensionalArrayItem(IList<MultiDimensionalArrayItem> items, Type expectedElementType)
        {
            int[] attributeAsArrayOfInt = this._reader.GetAttributeAsArrayOfInt("indexes");
            using (IEnumerator<string> enumerator = this._reader.ReadSubElements().GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    PropertyTag propertyTag = getPropertyTag(enumerator.Current);
                    if (propertyTag != PropertyTag.Unknown)
                    {
                        Property property = this.deserialize(propertyTag, expectedElementType);
                        MultiDimensionalArrayItem item = new MultiDimensionalArrayItem(attributeAsArrayOfInt, property);
                        items.Add(item);
                    }
                }
            }
        }

        private void parseSingleDimensionalArrayProperty(SingleDimensionalArrayProperty property)
        {
            property.ElementType = this._reader.GetAttributeAsType("elementType");
            property.LowerBound = this._reader.GetAttributeAsInt("lowerBound");
            using (IEnumerator<string> enumerator = this._reader.ReadSubElements().GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current == "Items")
                    {
                        this.readItems(property.Items, property.ElementType);
                    }
                }
            }
        }

        private void readItems(ICollection<Property> items, Type expectedElementType)
        {
            using (IEnumerator<string> enumerator = this._reader.ReadSubElements().GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    PropertyTag propertyTag = getPropertyTag(enumerator.Current);
                    if (propertyTag != PropertyTag.Unknown)
                    {
                        Property item = this.deserialize(propertyTag, expectedElementType);
                        if (item != null)
                        {
                            items.Add(item);
                        }
                    }
                }
            }
        }

        private void parseDictionaryProperty(DictionaryProperty property)
        {
            property.KeyType = this._reader.GetAttributeAsType("keyType");
            property.ValueType = this._reader.GetAttributeAsType("valueType");
            foreach (string str in this._reader.ReadSubElements())
            {
                switch (str)
                {
                    case "Properties":
                        this.readProperties(property.Properties, property.Type);
                        break;

                    case "Items":
                        this.readDictionaryItems(property.Items, property.KeyType, property.ValueType);
                        break;
                }
            }
        }

        private void readProperties(PropertyCollection properties, Type ownerType)
        {
            using (IEnumerator<string> enumerator = this._reader.ReadSubElements().GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    PropertyTag propertyTag = getPropertyTag(enumerator.Current);
                    if (propertyTag != PropertyTag.Unknown)
                    {
                        string attributeAsString = this._reader.GetAttributeAsString("name");
                        if (!string.IsNullOrEmpty(attributeAsString))
                        {
                            PropertyInfo info = DXFastProperty.From(ownerType, attributeAsString);
                            if (info != null)
                            {
                                Property item = null;
                                item = this.deserialize(propertyTag, info.PropertyType);
                                if (item != null)
                                {
                                    properties.Add(item);
                                }
                            }
                            else
                            {
                                Property item = null;
                                item = this.deserialize(propertyTag, typeof(string));
                                if (item != null)
                                {
                                    properties.Add(item);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void readDictionaryItems(IList<KeyValueItem> items, Type expectedKeyType, Type expectedValueType)
        {
            using (IEnumerator<string> enumerator = this._reader.ReadSubElements().GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current == "Item")
                    {
                        this.readDictionaryItem(items, expectedKeyType, expectedValueType);
                    }
                }
            }
        }

        private void readDictionaryItem(IList<KeyValueItem> items, Type expectedKeyType, Type expectedValueType)
        {
            Property key = null;
            Property property2 = null;
            foreach (string str in this._reader.ReadSubElements())
            {
                if ((key != null) && (property2 != null))
                {
                    break;
                }
                PropertyTag propertyTag = getPropertyTag(str);
                if (propertyTag != PropertyTag.Unknown)
                {
                    if (key == null)
                    {
                        key = this.deserialize(propertyTag, expectedKeyType);
                    }
                    else
                    {
                        property2 = this.deserialize(propertyTag, expectedValueType);
                    }
                }
            }
            KeyValueItem item = new KeyValueItem(key, property2);
            items.Add(item);
        }

        private void parseCollectionProperty(CollectionProperty property)
        {
            property.ElementType = this._reader.GetAttributeAsType("elementType");
            foreach (string str in this._reader.ReadSubElements())
            {
                switch (str)
                {
                    case "Properties":
                        this.readProperties(property.Properties, property.Type);
                        break;

                    case "Items":
                        this.readItems(property.Items, property.ElementType);
                        break;
                }
            }
        }

        private void parseComplexProperty(ComplexProperty property)
        {
            using (IEnumerator<string> enumerator = this._reader.ReadSubElements().GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current == "Properties")
                    {
                        this.readProperties(property.Properties, property.Type);
                    }
                }
            }
        }

    }
}
