﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZX.Data.View
{
    public partial class frmMapEdit : Form
    {
        private bool LayerTerrainVisible = true;
        private bool LayerObjectsVisible = false;
        private bool LayerRoadsVisible = false;
        private bool LayerZombiesVisible = false;

        private bool UseTerrainResourceCells = false;


        SaveHelper SaveHelper;
        MapInfo Map;
        int OSize;

        int MouseX, MouseY;

        bool RMouseDown = false;
        bool LMouseDown = false;
        Point RMousePoint;
        Point PanlScrollMargin;
        decimal PicZoom = 1.0M;
        Size PicSize;

        int SelectToolValue;

        public frmMapEdit()
        {
            InitializeComponent();

            this.picMap.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.picMap_MouseWheel);

        }

        public static frmMapEdit Show(SaveHelper saveHelper)
        {
            var frm = new frmMapEdit();

            frm.Show();
            frm.LayerTerrainVisible = true;
            frm.LayerObjectsVisible = true;

            frm.SaveHelper = saveHelper;
            frm.Map = saveHelper.GetMapInfo();
            frm.Draw();
            frm.SetZoom();

            frm.Text = saveHelper.FilePath;
            frm.InitTool();

            return frm;
        }



        private object Draw(int type = 0, int updateX = -1, int updateY = -1)
        {
            var cSize = Map.CellSize;   //单元大小
            var bSize = 1;              //边线大小
            var oSize = cSize + bSize;  //间距大小
            this.OSize = oSize;

            var pBorder = new Pen(new SolidBrush(Color.Gray), 1) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot };
            var pCurrent = new Pen(new SolidBrush(Color.Blue), 1) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot };

            var bEarth = new SolidBrush(Color.White);
            var bWater = new SolidBrush(Color.DarkBlue);
            var bGrass = new SolidBrush(Color.GreenYellow);
            var bSky = new SolidBrush(Color.LightSkyBlue);
            var bAbyss = new SolidBrush(Color.Black);

            var ftObject = new Font("微软雅黑", cSize - 1, FontStyle.Bold, GraphicsUnit.Pixel);
            var sfCenter = new StringFormat(StringFormatFlags.NoWrap)
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var bMountain = new SolidBrush(Color.SaddleBrown);
            var bTree = new SolidBrush(Color.DarkGreen);
            var bMineralGold = new SolidBrush(Color.Gold);
            var bMineralStone = new SolidBrush(Color.LightSlateGray);
            var bMineralIron = new SolidBrush(Color.Purple);
            var bMineralOil = new SolidBrush(Color.Olive);

            var bHighway = new SolidBrush(Color.Gray);

            var bZombies = new SolidBrush(Color.FromArgb(0xC0, Color.Red));

            var pRect = new Pen(new SolidBrush(Color.FromArgb(0xC0, 0xC0, 0xC0)), 1);

            var bCommandCenter = new SolidBrush(Color.Green);
            Graphics g;
            if (type != 0)
            {
                var icon = new Bitmap(oSize + bSize, oSize + bSize);
                g = Graphics.FromImage(icon);
                switch (type)
                {
                    case -99:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        //g.FillRectangle(bSky, bSize, bSize, cSize, cSize);
                        break;
                    case -10:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.FillRectangle(bSky, bSize, bSize, cSize, cSize);
                        break;
                    case -20:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.FillRectangle(bAbyss, bSize, bSize, cSize, cSize);
                        break;
                    case 20:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.FillRectangle(bEarth, bSize, bSize, cSize, cSize);
                        break;
                    case 10:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.FillRectangle(bWater, bSize, bSize, cSize, cSize);
                        break;
                    case 30:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.FillRectangle(bGrass, bSize, bSize, cSize, cSize);
                        break;
                    case 90:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.DrawString("木", ftObject, bTree, cSize / 2, cSize / 2, sfCenter);
                        break;
                    case 100:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.DrawString("山", ftObject, bMountain, cSize / 2, cSize / 2, sfCenter);
                        break;
                    case 80:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.FillRectangle(bHighway, bSize, bSize, cSize, cSize);
                        break;
                    case 40:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.DrawString("石", ftObject, bMineralStone, cSize / 2, cSize / 2, sfCenter);
                        break;
                    case 50:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.DrawString("铁", ftObject, bMineralIron, cSize / 2, cSize / 2, sfCenter);
                        break;
                    case 60:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.DrawString("油", ftObject, bMineralOil, cSize / 2, cSize / 2, sfCenter);
                        break;
                    case 70:
                        g.DrawRectangle(pRect, 0, 0, oSize + bSize, oSize + bSize);
                        g.DrawString("金", ftObject, bMineralGold, cSize / 2, cSize / 2, sfCenter);
                        break;
                }
                g.Dispose();
                return icon;
            }

            Bitmap bmp = null;


            if (updateX == -1 && updateY == -1)
            {
                //边框1像素
                this.PicSize = new Size(Map.NCells * (oSize) + bSize, Map.NCells * (oSize) + bSize);
                this.picMap.Size = new Size((int)(this.PicZoom * PicSize.Width), (int)(this.PicZoom * PicSize.Height));
                bmp = new Bitmap(PicSize.Width, PicSize.Height);
                g = Graphics.FromImage(bmp);
                g.Clear(Color.White);
            }
            else
            {
                g = Graphics.FromImage(this.picMap.Image);
            }

            //边框
            if (updateX == -1 && updateY == -1)
            {
                if (bSize > 0)
                {
                    for (var f = 0; f <= Map.NCells; f++)
                    {
                        //行
                        g.DrawLine(pBorder, 0, f * (oSize), PicSize.Width, f * (oSize));
                        //列
                        g.DrawLine(pBorder, f * (oSize), 0, f * (oSize), PicSize.Height);
                    }
                }
            }


            if (!UseTerrainResourceCells)
            {
                //LayerTerrain
                if (this.LayerTerrainVisible)
                {
                    int x1 = 0, x2 = Map.LayerTerrain.GetLength(0);
                    int y1 = 0, y2 = Map.LayerTerrain.GetLength(1);

                    if (updateX != -1 && updateY != -1)
                    {
                        x1 = updateX;
                        x2 = updateX + 1;
                        y1 = updateY;
                        y2 = updateY + 1;
                    }

                    for (var x = x1; x < x2; x++)
                    {
                        for (var y = y1; y < y2; y++)
                        {
                            switch (Map.LayerTerrain[x, y])
                            {
                                case ZXMapLayerTerrainType.Earth:
                                    g.FillRectangle(bEarth, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerTerrainType.Water:
                                    g.FillRectangle(bWater, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerTerrainType.Grass:
                                    g.FillRectangle(bGrass, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerTerrainType.Sky:
                                    g.FillRectangle(bSky, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerTerrainType.Abyss:
                                    g.FillRectangle(bAbyss, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                            }
                        }
                    }
                }

                //LayerObjects
                if (this.LayerObjectsVisible)
                {
                    int x1 = 0, x2 = Map.LayerObjects.GetLength(0);
                    int y1 = 0, y2 = Map.LayerObjects.GetLength(1);

                    if (updateX != -1 && updateY != -1)
                    {
                        x1 = updateX;
                        x2 = updateX + 1;
                        y1 = updateY;
                        y2 = updateY + 1;
                    }

                    for (var x = x1; x < x2; x++)
                    {
                        for (var y = y1; y < y2; y++)
                        {
                            switch (Map.LayerObjects[x, y])
                            {
                                case ZXMapLayerObjectType.Mountain:
                                    //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                                    g.DrawString("山", ftObject, bMountain, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                                    //g.FillRectangle(bMountain, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerObjectType.Tree:
                                    //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                                    g.DrawString("木", ftObject, bTree, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                                    //g.FillRectangle(bTree, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerObjectType.MineralGold:
                                    //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                                    g.DrawString("金", ftObject, bMineralGold, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                                    //g.FillRectangle(bMineralGold, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerObjectType.MineralStone:
                                    //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                                    g.DrawString("石", ftObject, bMineralStone, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                                    //g.FillRectangle(bMineralStone, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerObjectType.MineralIron:
                                    //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                                    g.DrawString("铁", ftObject, bMineralIron, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                                    //g.FillRectangle(bMineralIron, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerObjectType.None:
                                    break;
                            }
                        }
                    }
                }


                //LayerRoads
                if (this.LayerRoadsVisible)
                {
                    int x1 = 0, x2 = Map.LayerRoads.GetLength(0);
                    int y1 = 0, y2 = Map.LayerRoads.GetLength(1);

                    if (updateX != -1 && updateY != -1)
                    {
                        x1 = updateX;
                        x2 = updateX + 1;
                        y1 = updateY;
                        y2 = updateY + 1;
                    }

                    for (var x = x1; x < x2; x++)
                    {
                        for (var y = y1; y < y2; y++)
                        {
                            switch (Map.LayerRoads[x, y])
                            {
                                case ZXMapLayerRoad.Highway:
                                    g.FillRectangle(bHighway, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                                    break;
                                case ZXMapLayerRoad.None:
                                    break;
                            }
                        }
                    }
                }

                //LayerZombies
                if (this.LayerZombiesVisible)
                {
                    int x1 = 0, x2 = Map.LayerZombies.GetLength(0);
                    int y1 = 0, y2 = Map.LayerZombies.GetLength(1);

                    if (updateX != -1 && updateY != -1)
                    {
                        x1 = updateX;
                        x2 = updateX + 1;
                        y1 = updateY;
                        y2 = updateY + 1;
                    }

                    for (var x = x1; x < x2; x++)
                    {
                        for (var y = y1; y < y2; y++)
                        {
                            switch (Map.LayerZombies[x, y])
                            {
                                case ZXMapLayerZombies.Weak1:
                                case ZXMapLayerZombies.Weak2:
                                case ZXMapLayerZombies.Weak3:
                                case ZXMapLayerZombies.Medium1:
                                case ZXMapLayerZombies.Medium2:
                                case ZXMapLayerZombies.Medium3:
                                case ZXMapLayerZombies.Strong1:
                                case ZXMapLayerZombies.Strong2:
                                case ZXMapLayerZombies.Strong3:
                                case ZXMapLayerZombies.Powerful1:
                                case ZXMapLayerZombies.Powerful2:
                                case ZXMapLayerZombies.Ultra1:
                                    g.FillEllipse(bZombies, x * oSize + bSize + cSize / 6, y * oSize + bSize + cSize / 6, cSize - cSize / 6 * 2, cSize - cSize / 6 * 2);
                                    break;
                                case ZXMapLayerZombies.None:
                                    break;
                            }
                        }
                    }
                }
            }
            else
            {
                //int x1 = 0, x2 = Map.TerrainResourceCells.GetLength(0);
                //int y1 = 0, y2 = Map.TerrainResourceCells.GetLength(1);

                //if (updateX != -1 && updateY != -1)
                //{
                //    x1 = updateX;
                //    x2 = updateX + 1;
                //    y1 = updateY;
                //    y2 = updateY + 1;
                //}

                //for (var x = x1; x < x2; x++)
                //{
                //    for (var y = y1; y < y2; y++)
                //    {
                //        switch ((ZXTerrainResourceType)Map.TerrainResourceCells[x, y])
                //        {
                //            case ZXTerrainResourceType.Sea:
                //                g.FillRectangle(bWater, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                //                break;
                //            case ZXTerrainResourceType.Earth:
                //                g.FillRectangle(bEarth, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                //                break;
                //            case ZXTerrainResourceType.Grass:
                //                g.FillRectangle(bGrass, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                //                break;
                //            case ZXTerrainResourceType.Wood:
                //                g.FillRectangle(bTree, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                //                break;
                //            case ZXTerrainResourceType.Mountain:
                //                g.FillRectangle(bMountain, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                //                break;
                //            case ZXTerrainResourceType.Road:
                //                g.FillRectangle(bAbyss, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                //                break;
                //            case ZXTerrainResourceType.Stone:
                //                //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                //                g.DrawString("石", ftObject, bMineralStone, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                //                break;
                //            case ZXTerrainResourceType.Iron:
                //                //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                //                g.DrawString("铁", ftObject, bMineralIron, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                //                break;
                //            case ZXTerrainResourceType.Oil:
                //                //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                //                g.DrawString("油", ftObject, bMineralOil, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                //                break;
                //            case ZXTerrainResourceType.Gold:
                //                //g.DrawRectangle(pRect, x * oSize, y * oSize, oSize, oSize);
                //                g.DrawString("金", ftObject, bMineralGold, x * oSize + bSize + cSize / 2, y * oSize + bSize + cSize / 2, sfCenter);
                //                break;
                //            case ZXTerrainResourceType.GenericNotBuildable:
                //                //g.FillRectangle(bAbyss, x * oSize + bSize, y * oSize + bSize, cSize, cSize);
                //                break;
                //            case ZXTerrainResourceType.None:
                //            default:
                //                //g.FillRectangle(bAbyss, x * oSize + bSize, y * oSize + bSize, cSize, cSize);

                //                break;

                //        }
                //    }
                //}
            }

            //TerrainResourceCells

            //CommandCenterCell
            {
                //PointF[] pt = new PointF[10];
                //PointF ptCenter = new PointF(Map.CommandCenterCell.X * oSize + bSize + cSize / 2, Map.CommandCenterCell.Y * oSize + bSize + cSize / 2);
                //for (var f = 0; f < 10; f++)
                //{
                //    pt[f] = GetAnglePoint(ptCenter, (cSize - 2) / (3 - f % 2), 360 / 10 * f);
                //}
                //g.DrawRectangle(pRect, Map.CommandCenterCell.X * oSize, Map.CommandCenterCell.Y * oSize, oSize, oSize);
                //g.FillPolygon(bCommandCenter, pt);
                g.DrawString("★", ftObject, bCommandCenter, Map.CommandCenterCell.X * oSize + bSize + cSize / 2, Map.CommandCenterCell.Y * oSize + bSize + cSize / 2, sfCenter);
            }


            g.Dispose();

            if (updateX != -1 && updateY != -1)
            {
                return null;
            }

            this.picMap.Image = bmp;

            this.tsSize.Text = string.Format("MapSize: {0}x{0}", Map.NCells);

            return null;
        }

        private PointF GetAnglePoint(PointF pt, float length, float angle)
        {
            double r = angle * Math.PI / 180;
            return new PointF(
                (float)(pt.X + length * Math.Cos(r))
                , (float)(pt.Y + length * Math.Sin(r))
                );
        }

        private void tsLayerObjects_Click(object sender, EventArgs e)
        {
            this.LayerObjectsVisible = !this.LayerObjectsVisible;

            this.tsLayerObjects.Checked = this.LayerObjectsVisible;
            this.tsLayerObjects.BackColor = this.LayerObjectsVisible ? Color.LightSkyBlue : SystemColors.Control;

            this.Draw();
        }

        private void tsLayerTerrain_Click(object sender, EventArgs e)
        {
            this.LayerTerrainVisible = !this.LayerTerrainVisible;

            this.tsLayerTerrain.Checked = this.LayerTerrainVisible;
            this.tsLayerTerrain.BackColor = this.LayerTerrainVisible ? Color.LightSkyBlue : SystemColors.Control;

            this.Draw();
        }

        private void tsLayerRoads_Click(object sender, EventArgs e)
        {
            this.LayerRoadsVisible = !this.LayerRoadsVisible;

            this.tsLayerRoads.Checked = this.LayerRoadsVisible;
            this.tsLayerRoads.BackColor = this.LayerRoadsVisible ? Color.LightSkyBlue : SystemColors.Control;

            this.Draw();
        }

        private void tsLayerZombies_Click(object sender, EventArgs e)
        {
            this.LayerZombiesVisible = !this.LayerZombiesVisible;

            this.tsLayerZombies.Checked = this.LayerZombiesVisible;
            this.tsLayerZombies.BackColor = this.LayerZombiesVisible ? Color.LightSkyBlue : SystemColors.Control;

            this.Draw();
        }

        private void tsTerrainResource_Click(object sender, EventArgs e)
        {
            this.UseTerrainResourceCells = !this.UseTerrainResourceCells;

            this.tsTerrainResource.Checked = this.UseTerrainResourceCells;
            this.tsTerrainResource.BackColor = this.UseTerrainResourceCells ? Color.LightSkyBlue : SystemColors.Control;

            this.Draw();
        }



        private void picMap_MouseDown(object sender, MouseEventArgs e)
        {
            this.MouseX = (int)(e.X / this.PicZoom / this.OSize);
            this.MouseY = (int)(e.Y / this.PicZoom / this.OSize);


            if (e.Button == MouseButtons.Right)
            {
                this.RMouseDown = true;
                this.RMousePoint = e.Location;
                this.PanlScrollMargin = this.pnlMap.AutoScrollPosition;
            }

            if (e.Button == MouseButtons.Left)
            {
                this.LMouseDown = true;
                SetCell();
            }
        }

        private void SetCell()
        {
            bool changed = false;

            var value = this.SelectToolValue;
            if (value >= 0)
            {
                //this.Map.TerrainResourceCells[this.MouseX, this.MouseY] = value;
            }

            switch (value)
            {
                case -99:
                    {
                        if (this.Map.LayerObjects[this.MouseX, this.MouseY] != ZXMapLayerObjectType.None)
                        {
                            this.Map.LayerObjects[this.MouseX, this.MouseY] = ZXMapLayerObjectType.None;
                            changed = true;
                        }
                    }
                    break;
                case -10:
                    if (this.Map.LayerTerrain[this.MouseX, this.MouseY] != ZXMapLayerTerrainType.Sky)
                    {
                        this.Map.LayerTerrain[this.MouseX, this.MouseY] = ZXMapLayerTerrainType.Sky;
                        changed = true;
                    }
                    break;
                case -20:
                    if (this.Map.LayerTerrain[this.MouseX, this.MouseY] != ZXMapLayerTerrainType.Abyss)
                    {
                        this.Map.LayerTerrain[this.MouseX, this.MouseY] = ZXMapLayerTerrainType.Abyss;
                        changed = true;
                    }
                    break;
                case 10:
                    if (this.Map.LayerTerrain[this.MouseX, this.MouseY] != ZXMapLayerTerrainType.Water)
                    {
                        this.Map.LayerTerrain[this.MouseX, this.MouseY] = ZXMapLayerTerrainType.Water;
                        changed = true;
                    }
                    break;
                case 20:
                    if (this.Map.LayerTerrain[this.MouseX, this.MouseY] != ZXMapLayerTerrainType.Earth)
                    {
                        this.Map.LayerTerrain[this.MouseX, this.MouseY] = ZXMapLayerTerrainType.Earth;
                        changed = true;
                    }
                    break;
                case 30:
                    if (this.Map.LayerTerrain[this.MouseX, this.MouseY] != ZXMapLayerTerrainType.Grass)
                    {
                        this.Map.LayerTerrain[this.MouseX, this.MouseY] = ZXMapLayerTerrainType.Grass;
                        changed = true;
                    }
                    break;
                case 40:
                    if (this.Map.LayerObjects[this.MouseX, this.MouseY] != ZXMapLayerObjectType.MineralStone)
                    {
                        this.Map.LayerObjects[this.MouseX, this.MouseY] = ZXMapLayerObjectType.MineralStone;
                        changed = true;
                    }
                    break;
                case 50:
                    if (this.Map.LayerObjects[this.MouseX, this.MouseY] != ZXMapLayerObjectType.MineralIron)
                    {
                        this.Map.LayerObjects[this.MouseX, this.MouseY] = ZXMapLayerObjectType.MineralIron;
                        changed = true;
                    }
                    break;
                case 60:
                    break;
                case 70:
                    if (this.Map.LayerObjects[this.MouseX, this.MouseY] != ZXMapLayerObjectType.MineralGold)
                    {
                        this.Map.LayerObjects[this.MouseX, this.MouseY] = ZXMapLayerObjectType.MineralGold;
                        changed = true;
                    }
                    break;
                case 80:
                    break;
                case 90:
                    if (this.Map.LayerObjects[this.MouseX, this.MouseY] != ZXMapLayerObjectType.Tree)
                    {
                        this.Map.LayerObjects[this.MouseX, this.MouseY] = ZXMapLayerObjectType.Tree;
                        changed = true;
                    }
                    break;
                case 100:
                    if (this.Map.LayerObjects[this.MouseX, this.MouseY] != ZXMapLayerObjectType.Mountain)
                    {
                        this.Map.LayerObjects[this.MouseX, this.MouseY] = ZXMapLayerObjectType.Mountain;
                        changed = true;
                    }
                    break;
            }


            if (changed)
            {
                this.Draw(0, this.MouseX, this.MouseY);
                this.picMap.Refresh();
            }
        }

        private void picMap_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.RMouseDown = false;
            }
            if (e.Button == MouseButtons.Left)
            {
                this.LMouseDown = false;
            }
        }

        private void picMap_MouseMove(object sender, MouseEventArgs e)
        {
            this.MouseX = (int)(e.X / this.PicZoom / this.OSize);
            this.MouseY = (int)(e.Y / this.PicZoom / this.OSize);

            if (this.RMouseDown)
            {   //移动模式
                var x = this.pnlMap.AutoScrollPosition.X + (e.X - this.RMousePoint.X);
                var y = this.pnlMap.AutoScrollPosition.Y + (e.Y - this.RMousePoint.Y);
                this.pnlMap.AutoScrollPosition = new Point(-x, -y);
            }

            if (this.LMouseDown)
            {
                SetCell();
            }

            this.tsPostion.Text = string.Format("Postion: X={0}, Y={1}", this.MouseX, this.MouseY);
        }

        private void picMap_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e is HandledMouseEventArgs)
            {
                (e as HandledMouseEventArgs).Handled = true;
            }

            if (e.Delta > 0)
            {   //放大
                this.PicZoom += (decimal)0.1;
            }
            else
            {   //缩小
                this.PicZoom -= (decimal)0.1;
            }

            SetZoom();
        }

        private void tsZoom_Click(object sender, EventArgs e)
        {
            this.PicZoom = 1;
            SetZoom();
        }

        private void InitTool()
        {
            this.dgvTool.Rows.Clear();

            Dictionary<int, string> dicName = new Dictionary<int, string>
            {
                { -10, "天空" }
                , { -20, "深渊" }
                , { 10, "河" }
                , { 20, "地面" }
                , { 30, "草地" }
                , { -99, "清除[矿/林/山]" }
                , { 40, "石矿" }
                , { 50, "铁矿" }
                //, { 60, "石油" }
                , { 70, "金矿" }
                //, { 80, "道路" }
                , { 90, "森林" }
                , { 100, "山" }
            };

            foreach (var i in dicName)
            {
                var picIcon = this.Draw(i.Key);

                this.dgvTool.Rows.Add(picIcon, i.Value, i.Key);
            }
        }

        private void dgvTool_CurrentCellChanged(object sender, EventArgs e)
        {
            if (this.dgvTool.CurrentCell != null)
            {
                int.TryParse(string.Format("{0}", this.dgvTool[2, this.dgvTool.CurrentCell.RowIndex].Value), out this.SelectToolValue);
            }
        }

        private void tsSave_Click(object sender, EventArgs e)
        {
            this.SaveHelper.Save();
            MessageBox.Show("OK");
        }

        private void SetZoom()
        {
            this.picMap.Size = new Size((int)(this.PicZoom * PicSize.Width), (int)(this.PicZoom * PicSize.Height));
            this.tsZoom.Text = string.Format("Zoom: {0}", this.PicZoom);
        }
    }
}
