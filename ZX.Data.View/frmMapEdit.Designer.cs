﻿namespace ZX.Data.View
{
    partial class frmMapEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMapEdit));
            this.TSC = new System.Windows.Forms.ToolStripContainer();
            this.SS = new System.Windows.Forms.StatusStrip();
            this.tsSize = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsPostion = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsZoom = new System.Windows.Forms.ToolStripStatusLabel();
            this.SC = new System.Windows.Forms.SplitContainer();
            this.pnlMap = new System.Windows.Forms.Panel();
            this.picMap = new System.Windows.Forms.PictureBox();
            this.dgvTool = new System.Windows.Forms.DataGridView();
            this.colIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TS = new System.Windows.Forms.ToolStrip();
            this.tsLayerTerrain = new System.Windows.Forms.ToolStripButton();
            this.tsLayerObjects = new System.Windows.Forms.ToolStripButton();
            this.tsLayerRoads = new System.Windows.Forms.ToolStripButton();
            this.tsLayerZombies = new System.Windows.Forms.ToolStripButton();
            this.tsTerrainResource = new System.Windows.Forms.ToolStripButton();
            this.tsSave = new System.Windows.Forms.ToolStripButton();
            this.TSC.BottomToolStripPanel.SuspendLayout();
            this.TSC.ContentPanel.SuspendLayout();
            this.TSC.TopToolStripPanel.SuspendLayout();
            this.TSC.SuspendLayout();
            this.SS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SC)).BeginInit();
            this.SC.Panel1.SuspendLayout();
            this.SC.Panel2.SuspendLayout();
            this.SC.SuspendLayout();
            this.pnlMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTool)).BeginInit();
            this.TS.SuspendLayout();
            this.SuspendLayout();
            // 
            // TSC
            // 
            // 
            // TSC.BottomToolStripPanel
            // 
            this.TSC.BottomToolStripPanel.Controls.Add(this.SS);
            // 
            // TSC.ContentPanel
            // 
            this.TSC.ContentPanel.Controls.Add(this.SC);
            this.TSC.ContentPanel.Size = new System.Drawing.Size(884, 414);
            this.TSC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TSC.Location = new System.Drawing.Point(0, 0);
            this.TSC.Name = "TSC";
            this.TSC.Size = new System.Drawing.Size(884, 461);
            this.TSC.TabIndex = 0;
            this.TSC.Text = "toolStripContainer1";
            // 
            // TSC.TopToolStripPanel
            // 
            this.TSC.TopToolStripPanel.Controls.Add(this.TS);
            // 
            // SS
            // 
            this.SS.Dock = System.Windows.Forms.DockStyle.None;
            this.SS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSize,
            this.tsPostion,
            this.tsZoom});
            this.SS.Location = new System.Drawing.Point(0, 0);
            this.SS.Name = "SS";
            this.SS.Size = new System.Drawing.Size(884, 22);
            this.SS.TabIndex = 1;
            this.SS.Text = "statusStrip1";
            // 
            // tsSize
            // 
            this.tsSize.Name = "tsSize";
            this.tsSize.Size = new System.Drawing.Size(0, 17);
            // 
            // tsPostion
            // 
            this.tsPostion.Name = "tsPostion";
            this.tsPostion.Size = new System.Drawing.Size(0, 17);
            // 
            // tsZoom
            // 
            this.tsZoom.Name = "tsZoom";
            this.tsZoom.Size = new System.Drawing.Size(0, 17);
            this.tsZoom.Click += new System.EventHandler(this.tsZoom_Click);
            // 
            // SC
            // 
            this.SC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SC.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.SC.IsSplitterFixed = true;
            this.SC.Location = new System.Drawing.Point(0, 0);
            this.SC.Name = "SC";
            // 
            // SC.Panel1
            // 
            this.SC.Panel1.Controls.Add(this.pnlMap);
            // 
            // SC.Panel2
            // 
            this.SC.Panel2.Controls.Add(this.dgvTool);
            this.SC.Size = new System.Drawing.Size(884, 414);
            this.SC.SplitterDistance = 728;
            this.SC.SplitterWidth = 3;
            this.SC.TabIndex = 1;
            // 
            // pnlMap
            // 
            this.pnlMap.AutoScroll = true;
            this.pnlMap.Controls.Add(this.picMap);
            this.pnlMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMap.Location = new System.Drawing.Point(0, 0);
            this.pnlMap.Name = "pnlMap";
            this.pnlMap.Size = new System.Drawing.Size(728, 414);
            this.pnlMap.TabIndex = 1;
            // 
            // picMap
            // 
            this.picMap.Location = new System.Drawing.Point(0, 0);
            this.picMap.Name = "picMap";
            this.picMap.Size = new System.Drawing.Size(100, 100);
            this.picMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMap.TabIndex = 0;
            this.picMap.TabStop = false;
            this.picMap.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picMap_MouseDown);
            this.picMap.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picMap_MouseMove);
            this.picMap.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picMap_MouseUp);
            // 
            // dgvTool
            // 
            this.dgvTool.AllowUserToAddRows = false;
            this.dgvTool.AllowUserToDeleteRows = false;
            this.dgvTool.AllowUserToResizeColumns = false;
            this.dgvTool.AllowUserToResizeRows = false;
            this.dgvTool.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIcon,
            this.colName,
            this.colValue});
            this.dgvTool.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvTool.Location = new System.Drawing.Point(0, 0);
            this.dgvTool.MultiSelect = false;
            this.dgvTool.Name = "dgvTool";
            this.dgvTool.ReadOnly = true;
            this.dgvTool.RowHeadersVisible = false;
            this.dgvTool.RowTemplate.Height = 23;
            this.dgvTool.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTool.Size = new System.Drawing.Size(153, 270);
            this.dgvTool.TabIndex = 0;
            this.dgvTool.CurrentCellChanged += new System.EventHandler(this.dgvTool_CurrentCellChanged);
            // 
            // colIcon
            // 
            this.colIcon.HeaderText = "图示";
            this.colIcon.Name = "colIcon";
            this.colIcon.ReadOnly = true;
            this.colIcon.Width = 40;
            // 
            // colName
            // 
            this.colName.HeaderText = "名称";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colValue
            // 
            this.colValue.HeaderText = "Value";
            this.colValue.Name = "colValue";
            this.colValue.ReadOnly = true;
            this.colValue.Visible = false;
            // 
            // TS
            // 
            this.TS.Dock = System.Windows.Forms.DockStyle.None;
            this.TS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLayerTerrain,
            this.tsLayerObjects,
            this.tsLayerRoads,
            this.tsLayerZombies,
            this.tsTerrainResource,
            this.tsSave});
            this.TS.Location = new System.Drawing.Point(0, 0);
            this.TS.Name = "TS";
            this.TS.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.TS.Size = new System.Drawing.Size(884, 25);
            this.TS.Stretch = true;
            this.TS.TabIndex = 0;
            // 
            // tsLayerTerrain
            // 
            this.tsLayerTerrain.BackColor = System.Drawing.Color.LightSkyBlue;
            this.tsLayerTerrain.Checked = true;
            this.tsLayerTerrain.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsLayerTerrain.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsLayerTerrain.Image = ((System.Drawing.Image)(resources.GetObject("tsLayerTerrain.Image")));
            this.tsLayerTerrain.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsLayerTerrain.Name = "tsLayerTerrain";
            this.tsLayerTerrain.Size = new System.Drawing.Size(84, 22);
            this.tsLayerTerrain.Text = "LayerTerrain";
            this.tsLayerTerrain.Click += new System.EventHandler(this.tsLayerTerrain_Click);
            // 
            // tsLayerObjects
            // 
            this.tsLayerObjects.BackColor = System.Drawing.Color.LightSkyBlue;
            this.tsLayerObjects.Checked = true;
            this.tsLayerObjects.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsLayerObjects.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsLayerObjects.Image = ((System.Drawing.Image)(resources.GetObject("tsLayerObjects.Image")));
            this.tsLayerObjects.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsLayerObjects.Name = "tsLayerObjects";
            this.tsLayerObjects.Size = new System.Drawing.Size(87, 22);
            this.tsLayerObjects.Text = "LayerObjects";
            this.tsLayerObjects.Click += new System.EventHandler(this.tsLayerObjects_Click);
            // 
            // tsLayerRoads
            // 
            this.tsLayerRoads.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsLayerRoads.Image = ((System.Drawing.Image)(resources.GetObject("tsLayerRoads.Image")));
            this.tsLayerRoads.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsLayerRoads.Name = "tsLayerRoads";
            this.tsLayerRoads.Size = new System.Drawing.Size(80, 22);
            this.tsLayerRoads.Text = "LayerRoads";
            this.tsLayerRoads.Visible = false;
            this.tsLayerRoads.Click += new System.EventHandler(this.tsLayerRoads_Click);
            // 
            // tsLayerZombies
            // 
            this.tsLayerZombies.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsLayerZombies.Image = ((System.Drawing.Image)(resources.GetObject("tsLayerZombies.Image")));
            this.tsLayerZombies.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsLayerZombies.Name = "tsLayerZombies";
            this.tsLayerZombies.Size = new System.Drawing.Size(93, 22);
            this.tsLayerZombies.Text = "LayerZombies";
            this.tsLayerZombies.Click += new System.EventHandler(this.tsLayerZombies_Click);
            // 
            // tsTerrainResource
            // 
            this.tsTerrainResource.BackColor = System.Drawing.Color.LightSkyBlue;
            this.tsTerrainResource.Checked = true;
            this.tsTerrainResource.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsTerrainResource.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsTerrainResource.Image = ((System.Drawing.Image)(resources.GetObject("tsTerrainResource.Image")));
            this.tsTerrainResource.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsTerrainResource.Name = "tsTerrainResource";
            this.tsTerrainResource.Size = new System.Drawing.Size(107, 22);
            this.tsTerrainResource.Text = "TerrainResource";
            this.tsTerrainResource.Click += new System.EventHandler(this.tsTerrainResource_Click);
            // 
            // tsSave
            // 
            this.tsSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsSave.Image = ((System.Drawing.Image)(resources.GetObject("tsSave.Image")));
            this.tsSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSave.Name = "tsSave";
            this.tsSave.Size = new System.Drawing.Size(39, 22);
            this.tsSave.Text = "Save";
            this.tsSave.Click += new System.EventHandler(this.tsSave_Click);
            // 
            // frmMapEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.TSC);
            this.Name = "frmMapEdit";
            this.Text = "Map";
            this.TSC.BottomToolStripPanel.ResumeLayout(false);
            this.TSC.BottomToolStripPanel.PerformLayout();
            this.TSC.ContentPanel.ResumeLayout(false);
            this.TSC.TopToolStripPanel.ResumeLayout(false);
            this.TSC.TopToolStripPanel.PerformLayout();
            this.TSC.ResumeLayout(false);
            this.TSC.PerformLayout();
            this.SS.ResumeLayout(false);
            this.SS.PerformLayout();
            this.SC.Panel1.ResumeLayout(false);
            this.SC.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SC)).EndInit();
            this.SC.ResumeLayout(false);
            this.pnlMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTool)).EndInit();
            this.TS.ResumeLayout(false);
            this.TS.PerformLayout();
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.ToolStripContainer TSC;
        private System.Windows.Forms.PictureBox picMap;
        private System.Windows.Forms.Panel pnlMap;
        private System.Windows.Forms.StatusStrip SS;
        private System.Windows.Forms.ToolStripStatusLabel tsSize;
        private System.Windows.Forms.ToolStrip TS;
        private System.Windows.Forms.ToolStripButton tsLayerObjects;
        private System.Windows.Forms.ToolStripButton tsLayerTerrain;
        private System.Windows.Forms.ToolStripButton tsLayerRoads;
        private System.Windows.Forms.ToolStripButton tsLayerZombies;
        private System.Windows.Forms.ToolStripStatusLabel tsPostion;
        private System.Windows.Forms.ToolStripButton tsTerrainResource;
        private System.Windows.Forms.ToolStripStatusLabel tsZoom;
        private System.Windows.Forms.SplitContainer SC;
        private System.Windows.Forms.DataGridView dgvTool;
        private System.Windows.Forms.DataGridViewImageColumn colIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValue;
        private System.Windows.Forms.ToolStripButton tsSave;
    }
}