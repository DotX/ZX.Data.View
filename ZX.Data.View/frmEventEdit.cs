﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZX.Data.View
{
    public partial class frmEventEdit : Form
    {
        private int id;
        private EventInfo EventItem
        {
            get
            {
                var result = new EventInfo()
                {
                    id = this.id,
                    StartTimeH = (int)this.nudStartTimeH.Value,
                    RepeatTimeH = (int)this.nudRepeatTimeH.Value,
                    MaxRepetitions = (int)this.nudMaxRepetitions.Value,
                    TimeToNotifyInAdvanceH = (int)this.nudTimeToNotifyInAdvanceH.Value,
                    Message = this.txtMessage.Text,
                    FinalSwarm = this.chkFinalSwarm.Checked,
                    GameWon = this.chkGameWon.Checked,
                    FactorUnitsNumberPerRepetition = (float)this.nudFactorUnitsNumberPerRepetition.Value,
                    MaxFactorUnitsNumberPerRepetition = (float)this.nudMaxFactorUnitsNumberPerRepetition.Value,
                    Generators = this.chkGenerators.Checked ? "N & S & E & W" : "N | S | E | W",
                };

                result.EntityType1 = new UnitCountInfo();
                result.EntityType2 = new UnitCountInfo();
                result.EntityType3 = new UnitCountInfo();
                result.EntityType4 = new UnitCountInfo();
                result.EntityType5 = new UnitCountInfo();
                result.EntityType6 = new UnitCountInfo();
                result.EntityType7 = new UnitCountInfo();
                result.EntityType8 = new UnitCountInfo();
                result.EntityType9 = new UnitCountInfo();
                result.EntityType10 = new UnitCountInfo();

                result.EntityType1.UnitName = this.cboEntityType1.Text;
                result.EntityType2.UnitName = this.cboEntityType2.Text;
                result.EntityType3.UnitName = this.cboEntityType3.Text;
                result.EntityType4.UnitName = this.cboEntityType4.Text;
                result.EntityType5.UnitName = this.cboEntityType5.Text;
                result.EntityType6.UnitName = this.cboEntityType6.Text;
                result.EntityType7.UnitName = this.cboEntityType7.Text;
                result.EntityType8.UnitName = this.cboEntityType8.Text;
                result.EntityType9.UnitName = this.cboEntityType9.Text;
                result.EntityType10.UnitName = this.cboEntityType10.Text;
                result.EntityType1.Min = (int)this.nudEntityType1Min.Value;
                result.EntityType2.Min = (int)this.nudEntityType2Min.Value;
                result.EntityType3.Min = (int)this.nudEntityType3Min.Value;
                result.EntityType4.Min = (int)this.nudEntityType4Min.Value;
                result.EntityType5.Min = (int)this.nudEntityType5Min.Value;
                result.EntityType6.Min = (int)this.nudEntityType6Min.Value;
                result.EntityType7.Min = (int)this.nudEntityType7Min.Value;
                result.EntityType8.Min = (int)this.nudEntityType8Min.Value;
                result.EntityType9.Min = (int)this.nudEntityType9Min.Value;
                result.EntityType10.Min = (int)this.nudEntityType10Min.Value;
                result.EntityType1.Max = (int)this.nudEntityType1Max.Value;
                result.EntityType2.Max = (int)this.nudEntityType2Max.Value;
                result.EntityType3.Max = (int)this.nudEntityType3Max.Value;
                result.EntityType4.Max = (int)this.nudEntityType4Max.Value;
                result.EntityType5.Max = (int)this.nudEntityType5Max.Value;
                result.EntityType6.Max = (int)this.nudEntityType6Max.Value;
                result.EntityType7.Max = (int)this.nudEntityType7Max.Value;
                result.EntityType8.Max = (int)this.nudEntityType8Max.Value;
                result.EntityType9.Max = (int)this.nudEntityType9Max.Value;
                result.EntityType10.Max = (int)this.nudEntityType10Max.Value;

                return result;
            }
            set
            {
                this.id = value.id;
                this.nudStartTimeH.Value = value.StartTimeH;
                this.nudRepeatTimeH.Value = value.RepeatTimeH;
                this.nudMaxRepetitions.Value = value.MaxRepetitions;
                this.nudTimeToNotifyInAdvanceH.Value = value.TimeToNotifyInAdvanceH;
                this.txtMessage.Text = value.Message;
                this.chkFinalSwarm.Checked = value.FinalSwarm;
                this.chkGameWon.Checked = value.GameWon;
                this.nudFactorUnitsNumberPerRepetition.Value = (decimal)value.FactorUnitsNumberPerRepetition;
                this.nudMaxFactorUnitsNumberPerRepetition.Value = (decimal)value.MaxFactorUnitsNumberPerRepetition;
                this.chkGenerators.Checked = value.Generators?.IndexOf("|") == -1;

                this.cboEntityType1.Text = value.EntityType1.UnitName;
                this.cboEntityType2.Text = value.EntityType2.UnitName;
                this.cboEntityType3.Text = value.EntityType3.UnitName;
                this.cboEntityType4.Text = value.EntityType4.UnitName;
                this.cboEntityType5.Text = value.EntityType5.UnitName;
                this.cboEntityType6.Text = value.EntityType6.UnitName;
                this.cboEntityType7.Text = value.EntityType7.UnitName;
                this.cboEntityType8.Text = value.EntityType8.UnitName;
                this.cboEntityType9.Text = value.EntityType9.UnitName;
                this.cboEntityType10.Text = value.EntityType10.UnitName;

                this.nudEntityType1Min.Value = value.EntityType1.Min;
                this.nudEntityType2Min.Value = value.EntityType2.Min;
                this.nudEntityType3Min.Value = value.EntityType3.Min;
                this.nudEntityType4Min.Value = value.EntityType4.Min;
                this.nudEntityType5Min.Value = value.EntityType5.Min;
                this.nudEntityType6Min.Value = value.EntityType6.Min;
                this.nudEntityType7Min.Value = value.EntityType7.Min;
                this.nudEntityType8Min.Value = value.EntityType8.Min;
                this.nudEntityType9Min.Value = value.EntityType9.Min;
                this.nudEntityType10Min.Value = value.EntityType10.Min;

                this.nudEntityType1Max.Value = value.EntityType1.Max;
                this.nudEntityType2Max.Value = value.EntityType2.Max;
                this.nudEntityType3Max.Value = value.EntityType3.Max;
                this.nudEntityType4Max.Value = value.EntityType4.Max;
                this.nudEntityType5Max.Value = value.EntityType5.Max;
                this.nudEntityType6Max.Value = value.EntityType6.Max;
                this.nudEntityType7Max.Value = value.EntityType7.Max;
                this.nudEntityType8Max.Value = value.EntityType8.Max;
                this.nudEntityType9Max.Value = value.EntityType9.Max;
                this.nudEntityType10Max.Value = value.EntityType10.Max;
            }
        }

        public frmEventEdit()
        {
            InitializeComponent();
        }

        public static EventInfo Show(EventInfo eventItem)
        {
            var frm = new frmEventEdit();

            frm.EventItem = eventItem;

            frm.ShowDialog();

            return frm.DialogResult == DialogResult.OK ? frm.EventItem : eventItem;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void Changed(object sender, EventArgs e)
        {
            var sb = new StringBuilder();
            string s;
            int starth;
            int repeath = 0;
            int repeat;
            int i = 0;
            int min, max;
            decimal d = 1;

            starth = i = (int)this.nudStartTimeH.Value;

            sb.AppendFormat("事件在 [{0}天{1}时]({2}H) 触发", i / 24, i % 24, i);



            i = (int)this.nudTimeToNotifyInAdvanceH.Value;
            if (i > 0)
            {
                sb.AppendFormat(", 在触发之前 [{0}] 小时提醒", i);

                s = this.txtMessage.Text;
                if (!string.IsNullOrWhiteSpace(s))
                {
                    sb.AppendFormat(", 提醒内容:[{0}]", s);
                }
            }

            sb.AppendLine();

            if (this.chkGameWon.Checked)
            {
                sb.AppendFormat("游戏胜利");
                sb.AppendLine();
            }
            else
            {
                repeat = i = (int)this.nudMaxRepetitions.Value;
                sb.AppendFormat("事件共触发 [{0}] 次", i);
                sb.AppendLine();

                if (repeat > 1)
                {
                    repeath = i = (int)this.nudRepeatTimeH.Value;
                    sb.AppendFormat("  每间隔 [{0}天{1}时]({2}H) 重复触发", i / 24, i % 24, i);

                    d = this.nudFactorUnitsNumberPerRepetition.Value;
                    if (d > 0)
                    {
                        sb.AppendFormat(", 每次增加 [{0:0%}] 单位数量", d);
                    }

                    if (this.nudMaxFactorUnitsNumberPerRepetition.Value > 0)
                    {
                        sb.AppendFormat(", 最大增加 [{0:0%}]", this.nudMaxFactorUnitsNumberPerRepetition.Value);
                    }

                    sb.AppendLine();
                }

                if (this.chkFinalSwarm.Checked)
                {
                    sb.AppendFormat("[最终事件]地图中所有僵尸会一起进攻!");
                    sb.AppendLine();
                }

                sb.AppendLine();

                sb.AppendFormat("单位内容:");
                sb.AppendLine();
                for (var f = 0; f < repeat; f++)
                {
                    sb.AppendFormat("  {0}天{1}时  ", (starth + f * repeath) / 24, (starth + f * repeath) % 24);

                    s = this.cboEntityType1.Text;
                    min = (int)this.nudEntityType1Min.Value;
                    max = (int)this.nudEntityType1Max.Value;

                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType2.Text;
                    min = (int)this.nudEntityType2Min.Value;
                    max = (int)this.nudEntityType2Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType3.Text;
                    min = (int)this.nudEntityType3Min.Value;
                    max = (int)this.nudEntityType3Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType4.Text;
                    min = (int)this.nudEntityType4Min.Value;
                    max = (int)this.nudEntityType4Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType5.Text;
                    min = (int)this.nudEntityType5Min.Value;
                    max = (int)this.nudEntityType5Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType6.Text;
                    min = (int)this.nudEntityType6Min.Value;
                    max = (int)this.nudEntityType6Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType7.Text;
                    min = (int)this.nudEntityType7Min.Value;
                    max = (int)this.nudEntityType7Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType8.Text;
                    min = (int)this.nudEntityType8Min.Value;
                    max = (int)this.nudEntityType8Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType9.Text;
                    min = (int)this.nudEntityType9Min.Value;
                    max = (int)this.nudEntityType9Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    s = this.cboEntityType10.Text;
                    min = (int)this.nudEntityType10Min.Value;
                    max = (int)this.nudEntityType10Max.Value;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        sb.AppendFormat("[{0}({1},{2})],", s, min * (1 + f * d), max * (1 + f * d));
                    }

                    sb.AppendLine();
                }
            }

            this.txtInfo.Text = sb.ToString();
        }
    }
}
