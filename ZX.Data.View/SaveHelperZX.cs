﻿using DXVision;
using DXVision.Serialization.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DXVision.Serialization.Deserializing;
using System.Drawing;

namespace ZX.Data.View
{
    public partial class SaveHelper
    {
        ///<summary>取得属性派生类名称</summary>
        public string GetPropertyType(Property property)
        {
            if (property is SimpleProperty)
            {
                return "SimpleProperty";
            }
            else if (property is CollectionProperty)
            {
                return "CollectionProperty";
            }
            else if (property is SingleDimensionalArrayProperty)
            {
                return "SingleDimensionalArrayProperty";
            }
            else if (property is NullProperty)
            {
                return "NullProperty";
            }
            else if (property is DictionaryProperty)
            {
                return "DictionaryProperty";
            }
            else if (property is MultiDimensionalArrayProperty)
            {
                return "MultiDimensionalArrayProperty";
            }
            else if (property is ComplexProperty)
            {
                return "ComplexProperty";
            }
            else
            {
                return "Unknow";
            }
        }

        ///<summary>根据属性路径取得属性</summary>
        public Property GetProperty(string propertyPath = "")
        {
            Property p = this.RootProperty;

            if (string.IsNullOrWhiteSpace(propertyPath))
            {
                return p;
            }

            var ts = propertyPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var i in ts)
            {
                if (p is DictionaryProperty)
                {   //字典集
                    if (int.TryParse(i, out int index))
                    {
                        p = ((DictionaryProperty)p).Items[index].Value;
                    }
                }
                else if (p is CollectionProperty)
                {   //集合
                    if (int.TryParse(i, out int index))
                    {
                        p = ((CollectionProperty)p).Items[index];
                    }
                }
                else if (p is ComplexProperty)
                {   //复合属性
                    p = ((ComplexProperty)p).Properties.FirstOrDefault(a => a.Name == i);
                }
                else
                {
                    break;
                }
            }
            return p;
        }

        ///<summary>根据属性路径取得简单属性的值</summary>
        public object GetPropertyValue(string propertyPath)
        {
            var p = GetProperty(propertyPath);
            if (p != null && p is SimpleProperty)
            {
                return ((SimpleProperty)p).Value;
            }
            return null;
        }

        ///<summary>取得属性信息表</summary>
        public DataTable GetChildDataTable(Property property, bool isCanEdit = false)
        {
            var dt = new DataTable();
            dt.TableName = property.Name;
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Property", typeof(string));
            dt.Columns.Add("Type", typeof(string));
            dt.Columns.Add("Value", typeof(string));

            Action<DataTable, string, Property, string> actDataTableAddRow = delegate (DataTable _dt, string _name, Property _property, string _value)
            {
                if (_property is SimpleProperty)
                {
                    dt.Rows.Add(_name, GetPropertyType(_property), _property.Type, ((SimpleProperty)_property).Value);
                }
                else if (_property is NullProperty)
                {
                    dt.Rows.Add(_name, GetPropertyType(_property), _property.Type, _value);
                }
                else if (_property is DictionaryProperty && !isCanEdit)
                {
                    dt.Rows.Add(_name, GetPropertyType(_property), _property.Type, ((DictionaryProperty)_property).Items.Count);
                }
                else if (_property is CollectionProperty && !isCanEdit)
                {
                    dt.Rows.Add(_name, GetPropertyType(_property), _property.Type, ((CollectionProperty)_property).Items.Count);
                }
                else if (!isCanEdit)
                {
                    dt.Rows.Add(_name, GetPropertyType(_property), _property.Type, _value);
                }
            };

            if (property is DictionaryProperty)
            {   //字典集
                int c = 0;
                foreach (var i in ((DictionaryProperty)property).Items)
                {
                    actDataTableAddRow(dt, c++.ToString(), i.Value, string.Format("{0}", i.Key));
                }
                return dt;
            }
            else if (property is CollectionProperty)
            {   //集合
                int c = 0;
                foreach (var i in ((CollectionProperty)property).Items)
                {
                    actDataTableAddRow(dt, c++.ToString(), i, "");
                }
                return dt;
            }
            else if (property is SingleDimensionalArrayProperty)
            {
                int c = 0;
                foreach (var i in ((SingleDimensionalArrayProperty)property).Items)
                {
                    actDataTableAddRow(dt, c++.ToString(), i, "");
                }
                return dt;
            }
            else if (property is ComplexProperty)
            {   //复合属性
                foreach (Property i in ((ComplexProperty)property).Properties)
                {
                    actDataTableAddRow(dt, i.Name, i, "");
                }
                return dt;
            }
            else
            {
                return null;
            }
        }

        ///<summary>取得地图相关信息</summary>
        public MapInfo GetMapInfo()
        {
            string s;
            string[] ts;
            int l1, l2;
            byte[] bytes;


            var map = new MapInfo();

            map.NCells = int.Parse(string.Format("{0}", GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\NCells")));

            //map.LayerTerrain = GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerTerrain\Cells") as ZXMapLayerTerrainType[,];

            s = GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerTerrain\Cells") as string;
            ts = s.Split(new char[] { '|' }, 3);
            int.TryParse(ts[0], out l1);
            int.TryParse(ts[1], out l2);
            bytes = Convert.FromBase64String(ts[2]);

            map.LayerTerrain = new ZXMapLayerTerrainType[l1, l2];
            for (var f1 = 0; f1 < l1; f1++)
            {
                for (var f2 = 0; f2 < l2; f2++)
                {
                    map.LayerTerrain[f1, f2] = (ZXMapLayerTerrainType)BitConverter.ToUInt32(bytes, f1 * 4 * l1 + f2 * 4);
                }
            }

            //map.LayerObjects = GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerObjects\Cells") as ZXMapLayerObjectType[,];
            s = GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerObjects\Cells") as string;
            ts = s.Split(new char[] { '|' }, 3);
            int.TryParse(ts[0], out l1);
            int.TryParse(ts[1], out l2);
            bytes = Convert.FromBase64String(ts[2]);

            map.LayerObjects = new ZXMapLayerObjectType[l1, l2];
            for (var f1 = 0; f1 < l1; f1++)
            {
                for (var f2 = 0; f2 < l2; f2++)
                {
                    map.LayerObjects[f1, f2] = (ZXMapLayerObjectType)BitConverter.ToUInt32(bytes, f1 * 4 * l1 + f2 * 4);
                }
            }

            //map.LayerRoads = GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerRoads\Cells") as ZXMapLayerRoad[,];
            s = GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerRoads\Cells") as string;
            ts = s.Split(new char[] { '|' }, 3);
            int.TryParse(ts[0], out l1);
            int.TryParse(ts[1], out l2);
            bytes = Convert.FromBase64String(ts[2]);

            map.LayerRoads = new ZXMapLayerRoad[l1, l2];
            for (var f1 = 0; f1 < l1; f1++)
            {
                for (var f2 = 0; f2 < l2; f2++)
                {
                    map.LayerRoads[f1, f2] = (ZXMapLayerRoad)BitConverter.ToUInt32(bytes, f1 * 4 * l1 + f2 * 4);
                }
            }

            //map.LayerZombies = GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerZombies\Cells") as ZXMapLayerZombies[,];
            s = GetPropertyValue(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerRoads\Cells") as string;
            ts = s.Split(new char[] { '|' }, 3);
            int.TryParse(ts[0], out l1);
            int.TryParse(ts[1], out l2);
            bytes = Convert.FromBase64String(ts[2]);

            map.LayerZombies = new ZXMapLayerZombies[l1, l2];
            for (var f1 = 0; f1 < l1; f1++)
            {
                for (var f2 = 0; f2 < l2; f2++)
                {
                    map.LayerZombies[f1, f2] = (ZXMapLayerZombies)BitConverter.ToUInt32(bytes, f1 * 4 * l1 + f2 * 4);
                }
            }

            //map.TerrainResourceType = GetPropertyValue(@"\LevelState\SerTerrainResourceCells") as int[,];
            s = GetPropertyValue(@"\LevelState\SerTerrainResourceCells") as string;
            ts = s.Split(new char[] { '|' }, 3);
            int.TryParse(ts[0], out l1);
            int.TryParse(ts[1], out l2);
            bytes = Convert.FromBase64String(ts[2]);

            map.TerrainResourceType = new int[l1, l2];
            for (var f1 = 0; f1 < l1; f1++)
            {
                for (var f2 = 0; f2 < l2; f2++)
                {
                    map.TerrainResourceType[f1, f2] = (int)BitConverter.ToUInt32(bytes, f1 * 4 * l1 + f2 * 4);
                }
            }

            //OilSource
            var extraEntities = GetProperty(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\ExtraEntities") as CollectionProperty;
            map.OilSource = new List<ComplexProperty>();
            //foreach (var i in extraEntities.Items.Where(a => a.Type.FullName == "ZX.Entities.OilSource"))
            foreach (var i in extraEntities.Items)
            {
                if (i is ComplexProperty && GetOriginalType((ComplexProperty)i) == "ZX.Entities.OilSource, TheyAreBillions")
                {
                    map.OilSource.Add(i as ComplexProperty);
                }
            }

            map.OilSourceTemplete = ComplexPropertyClone(map.OilSource[0]);

            //EntitySource 
            map.EntitySource = this.GetLevelEntitiesList();


            map.CommandCenterCell = (System.Drawing.Point)GetPropertyValue(@"\LevelState\CurrentCommandCenterCell");


            map.EventList = this.GetEventList();

            return map;
        }

        private string GetOriginalType(ComplexProperty p)
        {
            SimpleProperty pOType = p.Properties.FirstOrDefault(a => a.Name == "oType") as SimpleProperty;

            if (pOType != null)
            {
                return string.Format("{0}", pOType.Value);
            }
            else
            {
                return p.Type.FullName;
            }
        }


        ///<summary>创建复合属性浅拷贝</summary>
        public ComplexProperty ComplexPropertyClone(ComplexProperty p)
        {
            ComplexProperty newProperty = new ComplexProperty(p.Name, p.Type);
            foreach (var i in p.Properties)
            {
                if (i is SimpleProperty)
                {
                    if (i.Name != "ID")
                    {
                        newProperty.Properties.Add(new SimpleProperty(i.Name, i.Type) { Value = ((SimpleProperty)i).Value });
                    }
                    else
                    {
                        newProperty.Properties.Add(new SimpleProperty(i.Name, i.Type) { Value = new Random().Next() });
                    }
                }
                else
                {
                    newProperty.Properties.Add(i);
                }
            }
            return newProperty;
        }

        /////<summary>查找第一个与区域有交集的复合属性</summary>
        //public ComplexProperty FindComplexPropertyWithIn(List<ComplexProperty> list, RectangleF rect)
        //{
        //    foreach (var i in list)
        //    {
        //        SimpleProperty pPosition = i.Properties.FirstOrDefault(a => a.Name == "Position") as SimpleProperty;
        //        SimpleProperty pSize = i.Properties.FirstOrDefault(a => a.Name == "Size") as SimpleProperty;

        //        if (pPosition != null && pSize != null)
        //        {
        //            PointF pt = (PointF)pPosition.Value;
        //            SizeF sz = (SizeF)pSize.Value;
        //            if (new RectangleF(pt, sz).IntersectsWith(rect))
        //            {
        //                return i;
        //            }
        //        }
        //    }
        //    return null;
        //}

        ///<summary>取得复合属性的区域</summary>
        public RectangleF GetComplexPropertyRectangle(ComplexProperty p)
        {
            SimpleProperty pPosition = p.Properties.FirstOrDefault(a => a.Name == "Position") as SimpleProperty;
            SimpleProperty pSize = p.Properties.FirstOrDefault(a => a.Name == "Size") as SimpleProperty;

            if (pPosition != null && pSize != null)
            {
                var rect = new RectangleF((PointF)pPosition.Value, (SizeF)pSize.Value);
                rect.Offset(-rect.Width / 2, -rect.Height / 2);
                return rect;
            }
            return RectangleF.Empty;
        }

        ///<summary>设置复合属性的坐标</summary>
        public void SetComplexPropertyPoint(ComplexProperty p, PointF point)
        {
            SimpleProperty pPosition = p.Properties.FirstOrDefault(a => a.Name == "Position") as SimpleProperty;
            SimpleProperty pSize = p.Properties.FirstOrDefault(a => a.Name == "Size") as SimpleProperty;

            var size = (SizeF)pSize.Value;

            if ((int)size.Width == size.Width
                && (int)size.Height == size.Height
                )
            {   //整数
                if (size.Width % 2 == 1)
                {
                    point.X = (int)point.X + 0.5f;
                }
                else
                {
                    point.X = (int)point.X;
                }

                if (size.Height % 2 == 1)
                {
                    point.Y = (int)point.Y + 0.5f;
                }
                else
                {
                    point.Y = (int)point.Y;
                }
            }

            if (pPosition != null && point != null)
            {
                pPosition.Value = point;
            }
        }

        public void SetPropertyValue(ComplexProperty p, string name, object value)
        {
            if (p != null)
            {
                for (var f = 0; f < p.Properties.Count; f++)
                {
                    var i = p.Properties[f];

                    if (i.Name == name)
                    {
                        if (i is SimpleProperty)
                        {
                            ((SimpleProperty)i).Value = value;
                        }
                        else if (i is NullProperty)
                        {
                            i = p.Properties[f] = new SimpleProperty(name, null);
                            ((SimpleProperty)i).Value = value;
                        }
                    }
                }
            }
        }

        public object GetPropertyValue(ComplexProperty p, string name)
        {
            foreach (var i in p.Properties)
            {
                if (i.Name == name)
                {
                    if (i is SimpleProperty)
                    {
                        return ((SimpleProperty)i).Value;
                    }
                }
            }
            return null;
        }

        ///<summary>取得事件信息表</summary>
        public DataTable ConvertToEventData(List<EventInfo> list)
        {
            var dt = new DataTable();

            dt.Columns.Add("Index", typeof(int));
            dt.Columns.Add("StartTimeH", typeof(int));
            dt.Columns.Add("RepeatTimeH", typeof(int));
            dt.Columns.Add("MaxRepetitions", typeof(int));
            dt.Columns.Add("TimeToNotifyInAdvanceH", typeof(int));
            dt.Columns.Add("Message", typeof(string));
            dt.Columns.Add("FinalSwarm", typeof(bool));
            dt.Columns.Add("GameWon", typeof(bool));
            dt.Columns.Add("FactorUnitsNumberPerRepetition", typeof(float));
            dt.Columns.Add("MaxFactorUnitsNumberPerRepetition", typeof(float));
            dt.Columns.Add("Generators", typeof(string));
            dt.Columns.Add("EntityType1", typeof(string));
            dt.Columns.Add("EntityType2", typeof(string));
            dt.Columns.Add("EntityType3", typeof(string));
            dt.Columns.Add("EntityType4", typeof(string));
            dt.Columns.Add("EntityType5", typeof(string));
            dt.Columns.Add("EntityType6", typeof(string));
            dt.Columns.Add("EntityType7", typeof(string));
            dt.Columns.Add("EntityType8", typeof(string));
            dt.Columns.Add("EntityType9", typeof(string));
            dt.Columns.Add("EntityType10", typeof(string));

            foreach (var i in list)
            {
                dt.Rows.Add(
                    i.id
                    , i.StartTimeH
                    , i.RepeatTimeH
                    , i.MaxRepetitions
                    , i.TimeToNotifyInAdvanceH
                    , i.Message
                    , i.FinalSwarm
                    , i.GameWon
                    , i.FactorUnitsNumberPerRepetition
                    , i.MaxFactorUnitsNumberPerRepetition
                    , i.Generators
                    , i.EntityType1.ToString()
                    , i.EntityType2.ToString()
                    , i.EntityType3.ToString()
                    , i.EntityType4.ToString()
                    , i.EntityType5.ToString()
                    , i.EntityType6.ToString()
                    , i.EntityType7.ToString()
                    , i.EntityType8.ToString()
                    , i.EntityType9.ToString()
                    , i.EntityType10.ToString()
                );
            }
            return dt;
        }

        public List<EventInfo> GetEventList()
        {
            var levelEvents = GetProperty(@"\LevelState\LevelEvents") as CollectionProperty;

            var result = new List<EventInfo>();
            int c = 0;
            foreach (ComplexProperty i in levelEvents.Items)
            {
                var eventItem = new EventInfo();
                eventItem.id = c++;

                int.TryParse($"{this.GetPropertyValue(i, "StartTimeH")}", out eventItem.StartTimeH);
                int.TryParse($"{this.GetPropertyValue(i, "RepeatTimeH")}", out eventItem.RepeatTimeH);
                int.TryParse($"{this.GetPropertyValue(i, "MaxRepetitions")}", out eventItem.MaxRepetitions);
                int.TryParse($"{this.GetPropertyValue(i, "TimeToNotifyInAdvanceH")}", out eventItem.TimeToNotifyInAdvanceH);

                eventItem.Message = $"{this.GetPropertyValue(i, "Message")}";

                bool.TryParse($"{this.GetPropertyValue(i, "FinalSwarm")}", out eventItem.FinalSwarm);
                bool.TryParse($"{this.GetPropertyValue(i, "GameWon")}", out eventItem.GameWon);

                float.TryParse($"{this.GetPropertyValue(i, "FactorUnitsNumberPerRepetition")}", out eventItem.FactorUnitsNumberPerRepetition);
                float.TryParse($"{this.GetPropertyValue(i, "MaxFactorUnitsNumberPerRepetition")}", out eventItem.MaxFactorUnitsNumberPerRepetition);

                eventItem.Generators = $"{this.GetPropertyValue(i, "Generators")}";

                eventItem.EntityType1 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType1")}");
                eventItem.EntityType2 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType2")}");
                eventItem.EntityType3 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType3")}");
                eventItem.EntityType4 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType4")}");
                eventItem.EntityType5 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType5")}");
                eventItem.EntityType6 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType6")}");
                eventItem.EntityType7 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType7")}");
                eventItem.EntityType8 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType8")}");
                eventItem.EntityType9 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType9")}");
                eventItem.EntityType10 = UnitCountInfo.Parse($"{this.GetPropertyValue(i, "EntityType10")}");
                result.Add(eventItem);
            }
            return result;
        }

        public void Update(MapInfo map)
        {
            ComplexProperty p;
            var sb = new StringBuilder();
            byte[] bytes;
            int l1, l2;

            sb.Clear();
            l1 = map.LayerTerrain.GetLength(0);
            l2 = map.LayerTerrain.GetLength(1);
            sb.Append($"{l1}|{l2}|");
            bytes = new byte[l1 * l2 * 4];
            for (var f1 = 0; f1 < l1; f1++)
            {
                for (var f2 = 0; f2 < l2; f2++)
                {
                    var b = BitConverter.GetBytes((UInt32)map.LayerTerrain[f1, f2]);
                    b.CopyTo(bytes, f1 * 4 * l1 + f2 * 4);
                }
            }
            sb.Append(Convert.ToBase64String(bytes));
            p = this.GetProperty(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerTerrain") as ComplexProperty;
            this.SetPropertyValue(p, "Cells", sb.ToString());

            sb.Clear();
            l1 = map.LayerObjects.GetLength(0);
            l2 = map.LayerObjects.GetLength(1);
            sb.Append($"{l1}|{l2}|");
            bytes = new byte[l1 * l2 * 4];
            for (var f1 = 0; f1 < l1; f1++)
            {
                for (var f2 = 0; f2 < l2; f2++)
                {
                    var b = BitConverter.GetBytes((UInt32)map.LayerObjects[f1, f2]);
                    b.CopyTo(bytes, f1 * 4 * l1 + f2 * 4);
                }
            }
            sb.Append(Convert.ToBase64String(bytes));
            p = this.GetProperty(@"\LevelState\CurrentGeneratedLevel\Data\Extension\MapDrawer\LayerObjects") as ComplexProperty;
            this.SetPropertyValue(p, "Cells", sb.ToString());

            //map.LayerRoads 不处理
            //map.LayerZombies 不处理

            sb.Clear();
            l1 = map.TerrainResourceType.GetLength(0);
            l2 = map.TerrainResourceType.GetLength(1);
            sb.Append($"{l1}|{l2}|");
            bytes = new byte[l1 * l2 * 4];
            for (var f1 = 0; f1 < l1; f1++)
            {
                for (var f2 = 0; f2 < l2; f2++)
                {
                    var b = BitConverter.GetBytes((UInt32)map.TerrainResourceType[f1, f2]);
                    b.CopyTo(bytes, f1 * 4 * l1 + f2 * 4);
                }
            }
            sb.Append(Convert.ToBase64String(bytes));
            p = this.GetProperty(@"\LevelState") as ComplexProperty;
            this.SetPropertyValue(p, "SerTerrainResourceCells", sb.ToString());

            var list = map.EventList;

            var levelEvents = GetProperty(@"\LevelState\LevelEvents") as CollectionProperty;
            var eventTemplete = this.ComplexPropertyClone(levelEvents.Items[0] as ComplexProperty);

            levelEvents.Items.Clear();
            int c = 1;
            foreach (var i in list)
            {
                p = this.ComplexPropertyClone(eventTemplete);

                var rand = new DXRandom();

                this.SetPropertyValue(p, "Created", true);
                this.SetPropertyValue(p, "Random", rand.Mt);
                this.SetPropertyValue(p, "NTimesTriggered", 0);
                this.SetPropertyValue(p, "StartTimeH", i.StartTimeH);
                this.SetPropertyValue(p, "GameTimeH", i.StartTimeH);

                this.SetPropertyValue(p, "RepeatTimeH", i.MaxRepetitions == 1 ? 0 : i.RepeatTimeH);
                this.SetPropertyValue(p, "GameTimeRandomOffsetH", 0);

                this.SetPropertyValue(p, "StartNotifyTimeH", i.TimeToNotifyInAdvanceH == 0 ? 0 : i.StartTimeH - i.TimeToNotifyInAdvanceH);
                this.SetPropertyValue(p, "TimeToNotifyInAdvanceH", i.TimeToNotifyInAdvanceH);
                this.SetPropertyValue(p, "NTimesNotified", 0);

                this.SetPropertyValue(p, "Repeteable", i.MaxRepetitions != 1);
                this.SetPropertyValue(p, "ID", c++);
                //this.SetPropertyValue(p, "LevelName", null);
                this.SetPropertyValue(p, "MaxRepetitions", i.MaxRepetitions);
                this.SetPropertyValue(p, "Message", i.Message);
                this.SetPropertyValue(p, "FinalSwarm", i.FinalSwarm);

                this.SetPropertyValue(p, "FactorUnitsNumberPerRepetition", i.FactorUnitsNumberPerRepetition);
                this.SetPropertyValue(p, "MaxFactorUnitsNumberPerRepetition", i.MaxFactorUnitsNumberPerRepetition);

                this.SetPropertyValue(p, "AutoNotifyPlayer", i.TimeToNotifyInAdvanceH != 0);
                this.SetPropertyValue(p, "ShowCountdown", i.TimeToNotifyInAdvanceH != 0);
                this.SetPropertyValue(p, "ShowMiniMapIndicator", i.TimeToNotifyInAdvanceH != 0);

                //this.SetPropertyValue(p, "GameOver", false);

                this.SetPropertyValue(p, "GameWon", i.GameWon);

                this.SetPropertyValue(p, "Generators"
                                        , i.GameWon ? ""                            //胜利事件, 不包含方向信息
                                        : i.FinalSwarm ? "N & S & E & W"            //最终波, 全方位
                                        : i.Generators);                            //生成设定

                this.SetPropertyValue(p, "AttackCommandCenter", !i.GameWon);
                this.SetPropertyValue(p, "AllInfectedToCommandCenter", i.FinalSwarm);
                this.SetPropertyValue(p, "MaxCellDispersionGeneration", 0);

                this.SetPropertyValue(p, "EntityType1", i.EntityType1.ToString());
                this.SetPropertyValue(p, "EntityType2", i.EntityType2.ToString());
                this.SetPropertyValue(p, "EntityType3", i.EntityType3.ToString());
                this.SetPropertyValue(p, "EntityType4", i.EntityType4.ToString());
                this.SetPropertyValue(p, "EntityType5", i.EntityType5.ToString());
                this.SetPropertyValue(p, "EntityType6", i.EntityType6.ToString());
                this.SetPropertyValue(p, "EntityType7", i.EntityType7.ToString());
                this.SetPropertyValue(p, "EntityType8", i.EntityType8.ToString());
                this.SetPropertyValue(p, "EntityType9", i.EntityType9.ToString());
                this.SetPropertyValue(p, "EntityType10", i.EntityType10.ToString());

                //this.SetPropertyValue(p, "Music", "");

                levelEvents.Items.Add(p);
            }
        }

        ///<summary>取得实体信息表</summary>
        public List<EntityInfo> GetLevelEntitiesList()
        {
            var levelEvents = GetProperty(@"\LevelState\LevelEntities") as DictionaryProperty;
            var list = new List<EntityInfo>();

            int c = 0;
            foreach (var pair in levelEvents.Items)
            {
                var i = pair.Value as ComplexProperty;
                SimpleProperty pPosition = i.Properties.FirstOrDefault(a => a.Name == "Position") as SimpleProperty;
                var pt = (PointF)pPosition.Value;

                list.Add(new EntityInfo()
                {
                    Name = i.Type.Name,
                    Key = string.Format("{0}", c++),
                    Visible = true,
                    Rect = this.GetComplexPropertyRectangle(i),
                    Property = i
                });
            }
            return list;
        }
    }
}
