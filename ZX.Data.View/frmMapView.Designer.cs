﻿namespace ZX.Data.View
{
    partial class frmMapView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TSC = new System.Windows.Forms.ToolStripContainer();
            this.SS = new System.Windows.Forms.StatusStrip();
            this.tsMapSize = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsMousePostion = new System.Windows.Forms.ToolStripStatusLabel();
            this.TC = new System.Windows.Forms.TabControl();
            this.tpMap = new System.Windows.Forms.TabPage();
            this.SC = new System.Windows.Forms.SplitContainer();
            this.picTerrainResource = new System.Windows.Forms.PictureBox();
            this.chkGridVisible = new System.Windows.Forms.CheckBox();
            this.chkLayer2Visible = new System.Windows.Forms.CheckBox();
            this.nudToolSize = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nudCellSize = new System.Windows.Forms.NumericUpDown();
            this.dgvTool = new System.Windows.Forms.DataGridView();
            this.colIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scMap = new System.Windows.Forms.SplitContainer();
            this.pnlMap = new System.Windows.Forms.Panel();
            this.lblTag = new System.Windows.Forms.Label();
            this.picMap = new System.Windows.Forms.PictureBox();
            this.tvEntity = new System.Windows.Forms.TreeView();
            this.tpEvent = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tpData = new System.Windows.Forms.TabPage();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tvSaveData = new System.Windows.Forms.TreeView();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.MS = new System.Windows.Forms.MenuStrip();
            this.tsSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.timerCellChange = new System.Windows.Forms.Timer(this.components);
            this.timerTerrainResourceRefresh = new System.Windows.Forms.Timer(this.components);
            this.dgvSchedule = new System.Windows.Forms.DataGridView();
            this.dgvEvent = new System.Windows.Forms.DataGridView();
            this.cmsEvent = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsInsertEvent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDeleteEvent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsEditEvent = new System.Windows.Forms.ToolStripMenuItem();
            this.TSC.BottomToolStripPanel.SuspendLayout();
            this.TSC.ContentPanel.SuspendLayout();
            this.TSC.TopToolStripPanel.SuspendLayout();
            this.TSC.SuspendLayout();
            this.SS.SuspendLayout();
            this.TC.SuspendLayout();
            this.tpMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SC)).BeginInit();
            this.SC.Panel1.SuspendLayout();
            this.SC.Panel2.SuspendLayout();
            this.SC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTerrainResource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudToolSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCellSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scMap)).BeginInit();
            this.scMap.Panel1.SuspendLayout();
            this.scMap.Panel2.SuspendLayout();
            this.scMap.SuspendLayout();
            this.pnlMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMap)).BeginInit();
            this.tpEvent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tpData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.MS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEvent)).BeginInit();
            this.cmsEvent.SuspendLayout();
            this.SuspendLayout();
            // 
            // TSC
            // 
            // 
            // TSC.BottomToolStripPanel
            // 
            this.TSC.BottomToolStripPanel.Controls.Add(this.SS);
            // 
            // TSC.ContentPanel
            // 
            this.TSC.ContentPanel.Controls.Add(this.TC);
            this.TSC.ContentPanel.Size = new System.Drawing.Size(884, 714);
            this.TSC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TSC.Location = new System.Drawing.Point(0, 0);
            this.TSC.Name = "TSC";
            this.TSC.Size = new System.Drawing.Size(884, 761);
            this.TSC.TabIndex = 0;
            this.TSC.Text = "toolStripContainer1";
            // 
            // TSC.TopToolStripPanel
            // 
            this.TSC.TopToolStripPanel.Controls.Add(this.MS);
            // 
            // SS
            // 
            this.SS.Dock = System.Windows.Forms.DockStyle.None;
            this.SS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMapSize,
            this.tsMousePostion});
            this.SS.Location = new System.Drawing.Point(0, 0);
            this.SS.Name = "SS";
            this.SS.Size = new System.Drawing.Size(884, 22);
            this.SS.TabIndex = 1;
            this.SS.Text = "statusStrip1";
            // 
            // tsMapSize
            // 
            this.tsMapSize.Name = "tsMapSize";
            this.tsMapSize.Size = new System.Drawing.Size(0, 17);
            // 
            // tsMousePostion
            // 
            this.tsMousePostion.Name = "tsMousePostion";
            this.tsMousePostion.Size = new System.Drawing.Size(0, 17);
            // 
            // TC
            // 
            this.TC.Controls.Add(this.tpMap);
            this.TC.Controls.Add(this.tpEvent);
            this.TC.Controls.Add(this.tpData);
            this.TC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TC.Location = new System.Drawing.Point(0, 0);
            this.TC.Name = "TC";
            this.TC.SelectedIndex = 0;
            this.TC.Size = new System.Drawing.Size(884, 714);
            this.TC.TabIndex = 0;
            this.TC.SelectedIndexChanged += new System.EventHandler(this.TC_SelectedIndexChanged);
            // 
            // tpMap
            // 
            this.tpMap.Controls.Add(this.SC);
            this.tpMap.Location = new System.Drawing.Point(4, 22);
            this.tpMap.Name = "tpMap";
            this.tpMap.Padding = new System.Windows.Forms.Padding(3);
            this.tpMap.Size = new System.Drawing.Size(876, 688);
            this.tpMap.TabIndex = 0;
            this.tpMap.Text = "地图";
            this.tpMap.UseVisualStyleBackColor = true;
            // 
            // SC
            // 
            this.SC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SC.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SC.IsSplitterFixed = true;
            this.SC.Location = new System.Drawing.Point(3, 3);
            this.SC.Name = "SC";
            // 
            // SC.Panel1
            // 
            this.SC.Panel1.Controls.Add(this.picTerrainResource);
            this.SC.Panel1.Controls.Add(this.chkGridVisible);
            this.SC.Panel1.Controls.Add(this.chkLayer2Visible);
            this.SC.Panel1.Controls.Add(this.nudToolSize);
            this.SC.Panel1.Controls.Add(this.label2);
            this.SC.Panel1.Controls.Add(this.label1);
            this.SC.Panel1.Controls.Add(this.nudCellSize);
            this.SC.Panel1.Controls.Add(this.dgvTool);
            // 
            // SC.Panel2
            // 
            this.SC.Panel2.Controls.Add(this.scMap);
            this.SC.Size = new System.Drawing.Size(870, 682);
            this.SC.SplitterDistance = 200;
            this.SC.SplitterWidth = 1;
            this.SC.TabIndex = 1;
            // 
            // picTerrainResource
            // 
            this.picTerrainResource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picTerrainResource.Location = new System.Drawing.Point(0, 499);
            this.picTerrainResource.Name = "picTerrainResource";
            this.picTerrainResource.Size = new System.Drawing.Size(200, 180);
            this.picTerrainResource.TabIndex = 8;
            this.picTerrainResource.TabStop = false;
            this.picTerrainResource.Paint += new System.Windows.Forms.PaintEventHandler(this.picTerrainResource_Paint);
            this.picTerrainResource.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picTerrainResource_MouseDown);
            // 
            // chkGridVisible
            // 
            this.chkGridVisible.AutoSize = true;
            this.chkGridVisible.Checked = true;
            this.chkGridVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGridVisible.Location = new System.Drawing.Point(69, 462);
            this.chkGridVisible.Name = "chkGridVisible";
            this.chkGridVisible.Size = new System.Drawing.Size(72, 16);
            this.chkGridVisible.TabIndex = 6;
            this.chkGridVisible.Text = "显示网络";
            this.chkGridVisible.UseVisualStyleBackColor = true;
            this.chkGridVisible.CheckedChanged += new System.EventHandler(this.chkGridVisible_CheckedChanged);
            // 
            // chkLayer2Visible
            // 
            this.chkLayer2Visible.AutoSize = true;
            this.chkLayer2Visible.Checked = true;
            this.chkLayer2Visible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLayer2Visible.Location = new System.Drawing.Point(69, 440);
            this.chkLayer2Visible.Name = "chkLayer2Visible";
            this.chkLayer2Visible.Size = new System.Drawing.Size(72, 16);
            this.chkLayer2Visible.TabIndex = 5;
            this.chkLayer2Visible.Text = "显示对象";
            this.chkLayer2Visible.UseVisualStyleBackColor = true;
            this.chkLayer2Visible.Click += new System.EventHandler(this.chkLayer2Visible_Click);
            // 
            // nudToolSize
            // 
            this.nudToolSize.Location = new System.Drawing.Point(88, 413);
            this.nudToolSize.Maximum = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.nudToolSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudToolSize.Name = "nudToolSize";
            this.nudToolSize.Size = new System.Drawing.Size(107, 21);
            this.nudToolSize.TabIndex = 4;
            this.nudToolSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudToolSize.ValueChanged += new System.EventHandler(this.nudToolSize_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 415);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "绘制单元大小";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 384);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "显示单元大小";
            // 
            // nudCellSize
            // 
            this.nudCellSize.Location = new System.Drawing.Point(88, 382);
            this.nudCellSize.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.nudCellSize.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nudCellSize.Name = "nudCellSize";
            this.nudCellSize.Size = new System.Drawing.Size(107, 21);
            this.nudCellSize.TabIndex = 1;
            this.nudCellSize.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudCellSize.ValueChanged += new System.EventHandler(this.nudCellSize_ValueChanged);
            // 
            // dgvTool
            // 
            this.dgvTool.AllowUserToAddRows = false;
            this.dgvTool.AllowUserToDeleteRows = false;
            this.dgvTool.AllowUserToResizeColumns = false;
            this.dgvTool.AllowUserToResizeRows = false;
            this.dgvTool.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTool.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIcon,
            this.colName,
            this.colValue});
            this.dgvTool.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvTool.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvTool.Location = new System.Drawing.Point(0, 0);
            this.dgvTool.MultiSelect = false;
            this.dgvTool.Name = "dgvTool";
            this.dgvTool.ReadOnly = true;
            this.dgvTool.RowHeadersVisible = false;
            this.dgvTool.RowTemplate.Height = 23;
            this.dgvTool.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTool.Size = new System.Drawing.Size(200, 367);
            this.dgvTool.TabIndex = 0;
            this.dgvTool.CurrentCellChanged += new System.EventHandler(this.dgvTool_CurrentCellChanged);
            // 
            // colIcon
            // 
            this.colIcon.HeaderText = "图";
            this.colIcon.Name = "colIcon";
            this.colIcon.ReadOnly = true;
            this.colIcon.Width = 30;
            // 
            // colName
            // 
            this.colName.HeaderText = "名称";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 160;
            // 
            // colValue
            // 
            this.colValue.HeaderText = "";
            this.colValue.Name = "colValue";
            this.colValue.ReadOnly = true;
            this.colValue.Visible = false;
            // 
            // scMap
            // 
            this.scMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMap.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.scMap.Location = new System.Drawing.Point(0, 0);
            this.scMap.Name = "scMap";
            // 
            // scMap.Panel1
            // 
            this.scMap.Panel1.Controls.Add(this.pnlMap);
            this.scMap.Panel1MinSize = 400;
            // 
            // scMap.Panel2
            // 
            this.scMap.Panel2.Controls.Add(this.tvEntity);
            this.scMap.Size = new System.Drawing.Size(669, 682);
            this.scMap.SplitterDistance = 404;
            this.scMap.SplitterWidth = 3;
            this.scMap.TabIndex = 1;
            // 
            // pnlMap
            // 
            this.pnlMap.AutoScroll = true;
            this.pnlMap.Controls.Add(this.lblTag);
            this.pnlMap.Controls.Add(this.picMap);
            this.pnlMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMap.Location = new System.Drawing.Point(0, 0);
            this.pnlMap.Name = "pnlMap";
            this.pnlMap.Size = new System.Drawing.Size(404, 682);
            this.pnlMap.TabIndex = 0;
            this.pnlMap.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlMap_Scroll);
            // 
            // lblTag
            // 
            this.lblTag.AutoSize = true;
            this.lblTag.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblTag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTag.Location = new System.Drawing.Point(0, 0);
            this.lblTag.Name = "lblTag";
            this.lblTag.Size = new System.Drawing.Size(2, 14);
            this.lblTag.TabIndex = 1;
            // 
            // picMap
            // 
            this.picMap.Location = new System.Drawing.Point(0, 0);
            this.picMap.Name = "picMap";
            this.picMap.Size = new System.Drawing.Size(100, 100);
            this.picMap.TabIndex = 0;
            this.picMap.TabStop = false;
            this.picMap.Paint += new System.Windows.Forms.PaintEventHandler(this.picMap_Paint);
            this.picMap.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picMap_MouseDown);
            this.picMap.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picMap_MouseMove);
            this.picMap.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picMap_MouseUp);
            // 
            // tvEntity
            // 
            this.tvEntity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvEntity.Location = new System.Drawing.Point(0, 0);
            this.tvEntity.Name = "tvEntity";
            this.tvEntity.Size = new System.Drawing.Size(262, 682);
            this.tvEntity.TabIndex = 0;
            this.tvEntity.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvEntity_AfterSelect);
            this.tvEntity.DoubleClick += new System.EventHandler(this.tvEntity_DoubleClick);
            // 
            // tpEvent
            // 
            this.tpEvent.Controls.Add(this.splitContainer2);
            this.tpEvent.Location = new System.Drawing.Point(4, 22);
            this.tpEvent.Name = "tpEvent";
            this.tpEvent.Padding = new System.Windows.Forms.Padding(3);
            this.tpEvent.Size = new System.Drawing.Size(876, 688);
            this.tpEvent.TabIndex = 1;
            this.tpEvent.Text = "事件";
            this.tpEvent.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgvEvent);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvSchedule);
            this.splitContainer2.Size = new System.Drawing.Size(870, 682);
            this.splitContainer2.SplitterDistance = 570;
            this.splitContainer2.TabIndex = 1;
            // 
            // tpData
            // 
            this.tpData.Controls.Add(this.txtPath);
            this.tpData.Controls.Add(this.splitContainer1);
            this.tpData.Location = new System.Drawing.Point(4, 22);
            this.tpData.Name = "tpData";
            this.tpData.Padding = new System.Windows.Forms.Padding(3);
            this.tpData.Size = new System.Drawing.Size(876, 688);
            this.tpData.TabIndex = 2;
            this.tpData.Text = "存档数据";
            this.tpData.UseVisualStyleBackColor = true;
            // 
            // txtPath
            // 
            this.txtPath.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtPath.Location = new System.Drawing.Point(3, 3);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(870, 21);
            this.txtPath.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(3, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tvSaveData);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvData);
            this.splitContainer1.Size = new System.Drawing.Size(870, 670);
            this.splitContainer1.SplitterDistance = 385;
            this.splitContainer1.TabIndex = 0;
            // 
            // tvSaveData
            // 
            this.tvSaveData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvSaveData.Location = new System.Drawing.Point(0, 0);
            this.tvSaveData.Name = "tvSaveData";
            this.tvSaveData.Size = new System.Drawing.Size(385, 670);
            this.tvSaveData.TabIndex = 0;
            this.tvSaveData.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvSaveData_AfterSelect);
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.AllowUserToResizeRows = false;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.Location = new System.Drawing.Point(0, 0);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowTemplate.Height = 23;
            this.dgvData.Size = new System.Drawing.Size(481, 670);
            this.dgvData.TabIndex = 0;
            this.dgvData.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellValueChanged);
            this.dgvData.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvData_DataBindingComplete);
            // 
            // MS
            // 
            this.MS.Dock = System.Windows.Forms.DockStyle.None;
            this.MS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSave,
            this.tsRefresh,
            this.toolStripMenuItem1});
            this.MS.Location = new System.Drawing.Point(0, 0);
            this.MS.Name = "MS";
            this.MS.Size = new System.Drawing.Size(884, 25);
            this.MS.TabIndex = 0;
            this.MS.Text = "menuStrip1";
            // 
            // tsSave
            // 
            this.tsSave.Name = "tsSave";
            this.tsSave.Size = new System.Drawing.Size(44, 21);
            this.tsSave.Text = "保存";
            this.tsSave.Click += new System.EventHandler(this.tsSave_Click);
            // 
            // tsRefresh
            // 
            this.tsRefresh.Name = "tsRefresh";
            this.tsRefresh.Size = new System.Drawing.Size(68, 21);
            this.tsRefresh.Text = "刷新地图";
            this.tsRefresh.Click += new System.EventHandler(this.tsRefresh_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.Red;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(274, 21);
            this.toolStripMenuItem1.Text = "注意: 关闭窗口不会保存文件, 注意备份原始文件";
            // 
            // timerCellChange
            // 
            this.timerCellChange.Interval = 800;
            this.timerCellChange.Tick += new System.EventHandler(this.timerCellChange_Tick);
            // 
            // timerTerrainResourceRefresh
            // 
            this.timerTerrainResourceRefresh.Interval = 500;
            this.timerTerrainResourceRefresh.Tick += new System.EventHandler(this.timerTerrainResourceRefresh_Tick);
            // 
            // dgvSchedule
            // 
            this.dgvSchedule.AllowUserToAddRows = false;
            this.dgvSchedule.AllowUserToDeleteRows = false;
            this.dgvSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSchedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSchedule.Location = new System.Drawing.Point(0, 0);
            this.dgvSchedule.Name = "dgvSchedule";
            this.dgvSchedule.ReadOnly = true;
            this.dgvSchedule.RowHeadersVisible = false;
            this.dgvSchedule.RowTemplate.Height = 23;
            this.dgvSchedule.Size = new System.Drawing.Size(296, 682);
            this.dgvSchedule.TabIndex = 0;
            // 
            // dgvEvent
            // 
            this.dgvEvent.AllowUserToAddRows = false;
            this.dgvEvent.AllowUserToDeleteRows = false;
            this.dgvEvent.AllowUserToResizeRows = false;
            this.dgvEvent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEvent.Location = new System.Drawing.Point(0, 0);
            this.dgvEvent.MultiSelect = false;
            this.dgvEvent.Name = "dgvEvent";
            this.dgvEvent.ReadOnly = true;
            this.dgvEvent.RowHeadersVisible = false;
            this.dgvEvent.RowTemplate.Height = 23;
            this.dgvEvent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEvent.Size = new System.Drawing.Size(570, 682);
            this.dgvEvent.TabIndex = 1;
            this.dgvEvent.CurrentCellChanged += new System.EventHandler(this.dgvEvent_CurrentCellChanged);
            this.dgvEvent.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvEvent_DataBindingComplete);
            // 
            // cmsEvent
            // 
            this.cmsEvent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsEditEvent,
            this.tsInsertEvent,
            this.tsDeleteEvent});
            this.cmsEvent.Name = "cmsEvent";
            this.cmsEvent.Size = new System.Drawing.Size(125, 70);
            // 
            // tsInsertEvent
            // 
            this.tsInsertEvent.Name = "tsInsertEvent";
            this.tsInsertEvent.Size = new System.Drawing.Size(152, 22);
            this.tsInsertEvent.Text = "插入事件";
            this.tsInsertEvent.Click += new System.EventHandler(this.tsInsertEvent_Click);
            // 
            // tsDeleteEvent
            // 
            this.tsDeleteEvent.Name = "tsDeleteEvent";
            this.tsDeleteEvent.Size = new System.Drawing.Size(152, 22);
            this.tsDeleteEvent.Text = "删除事件";
            this.tsDeleteEvent.Click += new System.EventHandler(this.tsDeleteEvent_Click);
            // 
            // tsEditEvent
            // 
            this.tsEditEvent.Name = "tsEditEvent";
            this.tsEditEvent.Size = new System.Drawing.Size(152, 22);
            this.tsEditEvent.Text = "编辑事件";
            this.tsEditEvent.Click += new System.EventHandler(this.tsEditEvent_Click);
            // 
            // frmMapView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 761);
            this.Controls.Add(this.TSC);
            this.MainMenuStrip = this.MS;
            this.Name = "frmMapView";
            this.Text = "MapView";
            this.TSC.BottomToolStripPanel.ResumeLayout(false);
            this.TSC.BottomToolStripPanel.PerformLayout();
            this.TSC.ContentPanel.ResumeLayout(false);
            this.TSC.TopToolStripPanel.ResumeLayout(false);
            this.TSC.TopToolStripPanel.PerformLayout();
            this.TSC.ResumeLayout(false);
            this.TSC.PerformLayout();
            this.SS.ResumeLayout(false);
            this.SS.PerformLayout();
            this.TC.ResumeLayout(false);
            this.tpMap.ResumeLayout(false);
            this.SC.Panel1.ResumeLayout(false);
            this.SC.Panel1.PerformLayout();
            this.SC.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SC)).EndInit();
            this.SC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picTerrainResource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudToolSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCellSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTool)).EndInit();
            this.scMap.Panel1.ResumeLayout(false);
            this.scMap.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMap)).EndInit();
            this.scMap.ResumeLayout(false);
            this.pnlMap.ResumeLayout(false);
            this.pnlMap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMap)).EndInit();
            this.tpEvent.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tpData.ResumeLayout(false);
            this.tpData.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.MS.ResumeLayout(false);
            this.MS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEvent)).EndInit();
            this.cmsEvent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer TSC;
        private System.Windows.Forms.TabControl TC;
        private System.Windows.Forms.TabPage tpMap;
        private System.Windows.Forms.Panel pnlMap;
        private System.Windows.Forms.PictureBox picMap;
        private System.Windows.Forms.StatusStrip SS;
        private System.Windows.Forms.ToolStripStatusLabel tsMousePostion;
        private System.Windows.Forms.ToolStripStatusLabel tsMapSize;
        private System.Windows.Forms.TabPage tpEvent;
        private System.Windows.Forms.MenuStrip MS;
        private System.Windows.Forms.SplitContainer SC;
        private System.Windows.Forms.DataGridView dgvTool;
        private System.Windows.Forms.DataGridViewImageColumn colIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudCellSize;
        private System.Windows.Forms.ToolStripMenuItem tsSave;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.TabPage tpData;
        private System.Windows.Forms.NumericUpDown nudToolSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem tsRefresh;
        private System.Windows.Forms.SplitContainer scMap;
        private System.Windows.Forms.CheckBox chkLayer2Visible;
        private System.Windows.Forms.TreeView tvEntity;
        private System.Windows.Forms.CheckBox chkGridVisible;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tvSaveData;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.PictureBox picTerrainResource;
        private System.Windows.Forms.Label lblTag;
        private System.Windows.Forms.Timer timerCellChange;
        private System.Windows.Forms.Timer timerTerrainResourceRefresh;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dgvEvent;
        private System.Windows.Forms.DataGridView dgvSchedule;
        private System.Windows.Forms.ContextMenuStrip cmsEvent;
        private System.Windows.Forms.ToolStripMenuItem tsEditEvent;
        private System.Windows.Forms.ToolStripMenuItem tsInsertEvent;
        private System.Windows.Forms.ToolStripMenuItem tsDeleteEvent;
    }
}