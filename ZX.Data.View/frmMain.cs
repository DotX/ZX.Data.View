﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using DXVision;
using Ionic.Zip;

namespace ZX.Data.View
{
    public partial class frmMain : Form
    {
        ///<summary>Dat,当前表管理员</summary>
        DXTableManager DXTableManagerCurrent;
        ///<summary>Dat,原始表管理员</summary>
        DXTableManager DXTableManagerOriginal;

        Form frmChanged;
        Form frmSteamConfig;

        static readonly DataGridViewCellStyle CSytleNoFound = new DataGridViewCellStyle() { BackColor = Color.WhiteSmoke };
        static readonly DataGridViewCellStyle CSytleMatch = new DataGridViewCellStyle() { BackColor = Color.Honeydew };
        static readonly DataGridViewCellStyle CSytleNoMatch = new DataGridViewCellStyle() { BackColor = Color.PeachPuff };

        string SteamConfigPath;
        Dictionary<string, uint> DicSteamConfig;
        HashSet<string> HSetIncorrectFile;
        DataTable DtSteamConfig;
        string ZXRulesName = "";
        uint ZXRulesSum = 0;
        bool ZXRulesIncorrect = false;


        public frmMain()
        {
            InitializeComponent();

#if !DEBUG
            this.tsTest.Visible = false;
#endif
        }

        private void tsLoad_Click(object sender, EventArgs e)
        {
            LoadFile(0);
        }

        private void tsLoadOriginal_Click(object sender, EventArgs e)
        {
            LoadFile(1);
        }

        private void tsLoadSteamConfig_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog()
            {
                Filter = "Data files(*.dat)|*.dat"
            };
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            LoadSteamConfig(ofd.FileName);
        }

        private void LoadFile(int loadType)
        {
            var ofd = new OpenFileDialog()
            {
                Filter = "Data files(*.dat)|*.dat"
            };
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            var path = ofd.FileName;

            if (System.IO.Path.GetFileName(path).ToUpper() == "STEAMCONFIG.DAT")
            {
                this.LoadSteamConfig(path);
                return;
            }

            try
            {
                switch (loadType)
                {
                    case 0:
                        this.DXTableManagerCurrent = null;
                        this.DXTableManagerCurrent = DXTableManager.FromDatFile(path);

                        if (this.DXTableManagerOriginal == null)
                        {
                            this.DXTableManagerOriginal = DXTableManager.FromDatFile(path);
                        }
                        break;
                    case 1:
                        this.DXTableManagerOriginal = null;
                        this.DXTableManagerOriginal = DXTableManager.FromDatFile(path);

                        break;
                }
                this.tsViewChanged.Enabled = this.DXTableManagerCurrent != null;

                if (this.DXTableManagerCurrent != null)
                {
                    this.TC.TabPages.Clear();
                    foreach (var i in this.DXTableManagerCurrent.Tables)
                    {
                        var dt = new DataTable();
                        foreach (var i2 in i.Value.Cols)
                        {
                            dt.Columns.Add(i2.Key);
                        }

                        foreach (var i2 in i.Value.Rows)
                        {
                            dt.Rows.Add(i2.Value);
                        }

                        var tp = new TabPage(i.Key);
                        var dgv = new DataGridView()
                        {
                            DataSource = dt,
                            //RowHeadersVisible = false,
                            Dock = DockStyle.Fill,
                            AllowUserToOrderColumns = false,
                            AllowUserToAddRows = false,
                            AllowUserToDeleteRows = false,
                        };

                        typeof(DataGridView).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)?.SetValue(dgv, true, null);

                        dgv.DefaultCellStyle = CSytleNoFound;
                        dgv.CellValueChanged += this.Dgv_CellValueChanged;
                        dgv.DataBindingComplete += this.Dgv_DataBindingComplete;
                        dgv.CurrentCellChanged += this.Dgv_CurrentCellChanged;

                        tp.Controls.Add(dgv);
                        this.TC.TabPages.Add(tp);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("读取文件发生错误/r" + ex.Message);
            }
        }


        private void LoadSteamConfig(string path)
        {
            try
            {
                var zipFile = new ZipFile(path);
                var entity = zipFile.Entries.FirstOrDefault();

                var ms = new System.IO.MemoryStream();
                entity.Extract(ms);
                ms.Seek(0, SeekOrigin.Begin);

                this.DicSteamConfig = new DXVision.Serialization.SharpSerializer().Deserialize(ms) as Dictionary<string, uint>;
                this.HSetIncorrectFile = new HashSet<string>();

                ms.Dispose();
                zipFile.Dispose();


                var folder = System.IO.Path.GetDirectoryName(path);

                this.DtSteamConfig = new DataTable();
                this.DtSteamConfig.Columns.Add("FileName", typeof(string));
                this.DtSteamConfig.Columns.Add("CheckValue", typeof(string));
                this.DtSteamConfig.Columns.Add("ActualValue", typeof(string));
                this.DtSteamConfig.Columns.Add("Result", typeof(string));

                foreach (var i in this.DicSteamConfig)
                {
                    var filepath = System.IO.Path.Combine(folder, i.Key);
                    uint sum = 0;

                    if (System.IO.File.Exists(filepath))
                    {
                        foreach (byte i2 in System.IO.File.ReadAllBytes(filepath))
                        {
                            sum += i2;
                        }
                        this.DtSteamConfig.Rows.Add(
                            i.Key
                            , i.Value
                            , sum
                            , i.Value == sum ? "file match" : "file incorrect");

                        if (i.Key.ToUpper() == "ZXRULDS.DAT")
                        {
                            this.ZXRulesName = i.Key;
                            this.ZXRulesSum = sum;
                            this.ZXRulesIncorrect = i.Value != sum;
                        }
                    }
                    else
                    {
                        this.DtSteamConfig.Rows.Add(
                           i.Key
                           , i.Value
                           , -1
                           , "no found file");
                    }
                    if (i.Value != sum)
                    {
                        this.HSetIncorrectFile.Add(i.Key);
                    }
                }

                if (this.frmSteamConfig != null)
                {
                    this.frmSteamConfig.Dispose();
                }

                this.frmSteamConfig = new Form()
                {
                    Size = new Size() { Width = 800, Height = 600 },
                    Text = "已删除不匹配的文件纪录, 请使用\"" + this.tsDeleteAndSaveSteamConfig.Text + "\"保存, 请注意备份原始文件"
                };

                var dgv = new DataGridView()
                {
                    DataSource = this.DtSteamConfig,
                    RowHeadersVisible = false,
                    Dock = DockStyle.Fill,
                    AllowUserToOrderColumns = false,
                    AllowUserToAddRows = false,
                    AllowUserToDeleteRows = false,
                };

                this.frmSteamConfig.Controls.Add(dgv);
                this.frmSteamConfig.Show();

                dgv.AutoResizeColumns();

                this.tsDeleteAndSaveSteamConfig.Enabled = true;

                if (this.ZXRulesIncorrect)
                {
                    this.tsFixFileAndSaveSteamConfig.Enabled = true;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("读取文件发生错误/r" + ex.Message);
            }

            this.SteamConfigPath = path;
        }

        private void tsDeleteAndSaveSteamConfig_Click(object sender, EventArgs e)
        {
            if (this.DicSteamConfig != null)
            {
                foreach (var i in this.HSetIncorrectFile)
                {
                    this.DicSteamConfig.Remove(i);
                }
                DXVision.Serialization.ZipSerializer.Write(this.SteamConfigPath, this.DicSteamConfig);
                MessageBox.Show("OK");
            }
        }

        private void tsFixFileAndSaveSteamConfig_Click(object sender, EventArgs e)
        {
            if (this.DicSteamConfig != null)
            {
                if (this.ZXRulesSum == 0)
                {
                    MessageBox.Show("SteamConfig.dat中没有ZXRule相关信息, 或者没有在SteamConfig.dat所在目录中发现ZXRules.dat");
                    return;
                }

                this.DicSteamConfig[this.ZXRulesName] = this.ZXRulesSum;
                DXVision.Serialization.ZipSerializer.Write(this.SteamConfigPath, this.DicSteamConfig);
                MessageBox.Show("OK");
            }
        }

        private void Dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;

            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                var tableName = dgv.Parent.Text;
                var id = $"{dgv[0, e.RowIndex].Value}";
                var value = $"{dgv[e.ColumnIndex, e.RowIndex].Value}";

                this.DXTableManagerCurrent[tableName].Rows[id][e.ColumnIndex] = value;

                if (this.DXTableManagerOriginal != null
                    && this.DXTableManagerOriginal.Tables.ContainsKey(tableName)
                    && this.DXTableManagerOriginal[tableName].Rows.ContainsKey(id))
                {
                    if ($"{this.DXTableManagerOriginal[tableName].Rows[id][e.ColumnIndex]}" == value)
                    {
                        dgv[e.ColumnIndex, e.RowIndex].Style = CSytleMatch;
                    }
                    else
                    {
                        dgv[e.ColumnIndex, e.RowIndex].Style = CSytleNoMatch;
                    }
                }
                else
                {
                    dgv[e.ColumnIndex, e.RowIndex].Style = CSytleNoFound;
                }
            }
        }

        private void Dgv_CurrentCellChanged(object sender, EventArgs e)
        {
            var dgv = (DataGridView)sender;

            if (dgv.CurrentCell != null
                && dgv.CurrentCell.ColumnIndex >= 0
                && dgv.CurrentCell.RowIndex >= 0
                )
            {
                var tableName = dgv.Parent.Text;
                var id = $"{dgv[0, dgv.CurrentCell.RowIndex].Value}";
                var value = $"{dgv[dgv.CurrentCell.ColumnIndex, dgv.CurrentCell.RowIndex].Value}";

                if (this.DXTableManagerOriginal != null
                    && this.DXTableManagerOriginal.Tables.ContainsKey(tableName)
                    && this.DXTableManagerOriginal[tableName].Rows.ContainsKey(id)
                    && this.ContainsFocus
                    )
                {
                    var valueOriginal = $"{this.DXTableManagerOriginal[tableName].Rows[id][dgv.CurrentCell.ColumnIndex]}";
                    if (valueOriginal == value)
                    {

                    }
                    else
                    {
                        var rect = dgv.GetCellDisplayRectangle(dgv.CurrentCell.ColumnIndex, dgv.CurrentCell.RowIndex, true);
                        this.cmsTipText.Tag = dgv[dgv.CurrentCell.ColumnIndex, dgv.CurrentCell.RowIndex];
                        this.cmsTipText.Text = valueOriginal;
                        this.cmsTip.Show(dgv, rect.GetBottomLeft());
                    }
                }
            }
        }

        private void cmsTipText_Click(object sender, EventArgs e)
        {
            var ctl = (ToolStripMenuItem)sender;
            var cell = (ctl.Tag as DataGridViewCell);
            if (cell != null)
            {
                cell.Value = ctl.Text;
            }
        }

        private void Dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.Reset)
            {
                var dgv = (DataGridView)sender;
                var tableName = dgv.Parent.Text;

                //ColumnHeader
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    r.HeaderCell.Value = r.Cells[0].Value;
                    r.Cells[0].ReadOnly = true;
                }

                //Cell
                if (this.DXTableManagerOriginal != null && this.DXTableManagerOriginal.Tables.ContainsKey(tableName))
                {
                    var tbOri = this.DXTableManagerOriginal.Tables[tableName];
                    var dicColMap = new Dictionary<int, int>();

                    //Map
                    for (var f = 1; f < dgv.Columns.Count; f++)
                    {
                        var name = dgv.Columns[f].Name;
                        if (name != "ID" && tbOri.Cols.ContainsKey(name))
                        {
                            dicColMap.Add(f, tbOri.Cols[name]);
                        }
                    }

                    //Match
                    foreach (DataGridViewRow r in dgv.Rows)
                    {
                        var id = $"{r.Cells[0].Value}";
                        if (tbOri.Rows.ContainsKey(id))
                        {
                            foreach (var i in dicColMap)
                            {
                                if ($"{r.Cells[i.Key].Value}" == $"{tbOri.Rows[id][i.Value]}")
                                {
                                    r.Cells[i.Key].Style = CSytleMatch;
                                }
                                else
                                {
                                    r.Cells[i.Key].Style = CSytleNoMatch;
                                }
                            }
                        }
                        else
                        {
                            for (var f = 1; f < r.Cells.Count; f++)
                            {
                                r.Cells[f].Style = CSytleNoFound;
                            }
                        }
                    }
                }

                if (dgv.Tag == null)
                {
                    dgv.AutoResizeColumns();
                    dgv.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
                    dgv.Tag = "Resized";
                }
            }
        }

        private void tsSaveAs_Click(object sender, EventArgs e)
        {
            var sfd = new SaveFileDialog()
            {
                Filter = "Data files(*.dat)|*.dat"
            };
            if (sfd.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            DXTableManager.ToDatFile(this.DXTableManagerCurrent, sfd.FileName);
            MessageBox.Show("OK");
        }

        private void tsViewChanged_Click(object sender, EventArgs e)
        {
            if (this.DXTableManagerCurrent == null || this.DXTableManagerOriginal == null)
            {
                return;
            }

            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("Table", typeof(string));
            dtResult.Columns.Add("ID", typeof(string));
            dtResult.Columns.Add("Column", typeof(string));
            dtResult.Columns.Add("Original", typeof(string));
            dtResult.Columns.Add("Current", typeof(string));

            foreach (var i in this.DXTableManagerCurrent.Tables)
            {
                var tableName = i.Key;

                if (!this.DXTableManagerOriginal.Tables.ContainsKey(tableName))
                {
                    continue;
                }

                var tbCur = this.DXTableManagerCurrent.Tables[tableName];
                var tbOri = this.DXTableManagerOriginal.Tables[tableName];

                var dicColMap = new Dictionary<int, int>();

                //Map
                foreach (var i2 in tbCur.Cols)
                {
                    var name = i2.Key;
                    if (name != "ID" && tbOri.Cols.ContainsKey(name))
                    {
                        dicColMap.Add(i2.Value, tbOri.Cols[name]);
                    }
                }

                //Match
                foreach (var i2 in tbCur.Rows)
                {
                    var id = i2.Key;
                    if (tbOri.Rows.ContainsKey(id))
                    {
                        foreach (var i3 in dicColMap)
                        {
                            if (tbCur.Rows[id][i3.Key] == tbOri.Rows[id][i3.Value])
                            {

                            }
                            else
                            {
                                dtResult.Rows.Add(tableName, id, tbCur.Cols.Keys.ElementAt(i3.Key), tbOri.Rows[id][i3.Value], tbCur.Rows[id][i3.Key]);
                            }
                        }
                    }
                    else
                    {
                        dtResult.Rows.Add(tableName, id, "No found id");
                    }
                }
            }

            if (this.frmChanged != null)
            {
                this.frmChanged.Dispose();
            }

            this.frmChanged = new Form()
            {
                Size = new Size() { Width = 800, Height = 600 },
                Text = "共计" + dtResult.Rows.Count + "项记录不同"
            };

            var dgv = new DataGridView()
            {
                DataSource = dtResult,
                RowHeadersVisible = false,
                Dock = DockStyle.Fill,
                AllowUserToOrderColumns = false,
                AllowUserToAddRows = false,
                AllowUserToDeleteRows = false,
            };

            dgv.CurrentCellChanged += delegate (object _sender, EventArgs _e)
            {
                if (dgv.CurrentCell != null
                    && dgv.CurrentCell.ColumnIndex >= 0
                    && dgv.CurrentCell.RowIndex >= 0
                    )
                {
                    var tableName = $"{dgv[0, dgv.CurrentCell.RowIndex].Value}";
                    var id = $"{dgv[1, dgv.CurrentCell.RowIndex].Value}";
                    var column = $"{dgv[2, dgv.CurrentCell.RowIndex].Value}";

                    foreach (TabPage tp in this.TC.TabPages)
                    {
                        if (tp.Text == tableName)
                        {
                            this.TC.SelectedTab = tp;
                            var tpdgv = tp.Controls[0] as DataGridView;
                            if (tpdgv != null && tpdgv.Columns[column] != null)
                            {
                                foreach (DataGridViewRow r in tpdgv.Rows)
                                {
                                    if ($"{r.Cells[0].Value}" == id)
                                    {
                                        tpdgv.CurrentCell = tpdgv[column, r.Index];
                                    }
                                }
                            }
                        }
                    }
                }
            };

            this.frmChanged.Controls.Add(dgv);
            this.frmChanged.Show();

            dgv.AutoResizeColumns();
        }

        private void tsLoadSave_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog()
            {
                Filter = "Save files(*.zxsav)|*.zxsav"
            };
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            var path = ofd.FileName;

            var saveHelper = new SaveHelper(path);
            saveHelper.Read();

            frmMapView.Show(saveHelper);
        }

        private void tsTest_Click(object sender, EventArgs e)
        {
            //var cp =  DXProject.LoadFromFile("ZXGame.dxprj", "TheyAreBillions.exe");

            //var aa = DXProject.Current.EntityTemplates[3210470986436757950];
        }
    }

}

