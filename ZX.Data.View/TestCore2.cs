﻿using DXVision.Serialization;
using DXVision.Serialization.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ZX.Data.View
{
    public partial class TestCore
    {
        public object CreateObject(Property property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            if (property is NullProperty)
            {
                return null;
            }
            if (property.Type == null)
            {
                throw new InvalidOperationException($"Property type is not defined. Property: { property.Name }");
            }
            SimpleProperty property2 = property as SimpleProperty;
            if (property2 != null)
            {
                return createObjectFromSimpleProperty(property2);
            }
            MultiDimensionalArrayProperty property3 = property as MultiDimensionalArrayProperty;
            if (property3 != null)
            {
                return this.createObjectFromMultidimensionalArrayProperty(property3);
            }
            SingleDimensionalArrayProperty property4 = property as SingleDimensionalArrayProperty;
            if (property4 != null)
            {
                return this.createObjectFromSingleDimensionalArrayProperty(property4);
            }
            DictionaryProperty property5 = property as DictionaryProperty;
            if (property5 != null)
            {
                return this.createObjectFromDictionaryProperty(property5);
            }
            CollectionProperty property6 = property as CollectionProperty;
            if (property6 != null)
            {
                return this.createObjectFromCollectionProperty(property6);
            }
            ComplexProperty property7 = property as ComplexProperty;
            if (property7 == null)
            {
                throw new InvalidOperationException($"Unknown Property type: {property.GetType().Name}");
            }
            return this.createObjectFromComplexProperty(property7);
        }

        private static object createObjectFromSimpleProperty(SimpleProperty property) => property.Value;

        private object createObjectFromMultidimensionalArrayProperty(MultiDimensionalArrayProperty property)
        {
            MultiDimensionalArrayCreatingInfo info = getMultiDimensionalArrayCreatingInfo(property.DimensionInfos);
            Array array = createArrayInstance(property.ElementType, info.Lengths, info.LowerBounds);
            foreach (MultiDimensionalArrayItem item in property.Items)
            {
                object obj2 = this.CreateObject(item.Value);
                if (obj2 != null)
                {
                    array.SetValue(obj2, item.Indexes);
                }
            }
            return array;
        }

        private class MultiDimensionalArrayCreatingInfo
        {
            // Methods
            public MultiDimensionalArrayCreatingInfo()
            {
            }

            // Properties
            public int[] Lengths { get; [CompilerGenerated] set; }
            public int[] LowerBounds { get; [CompilerGenerated] set; }
        }

        private static MultiDimensionalArrayCreatingInfo getMultiDimensionalArrayCreatingInfo(IEnumerable<DimensionInfo> infos)
        {
            List<int> list = new List<int>();
            List<int> list2 = new List<int>();
            foreach (DimensionInfo info in infos)
            {
                list.Add(info.Length);
                list2.Add(info.LowerBound);
            }
            return new MultiDimensionalArrayCreatingInfo
            {
                Lengths = list.ToArray(),
                LowerBounds = list2.ToArray()
            };
        }

        private static Array createArrayInstance(Type elementType, int[] lengths, int[] lowerBounds) => Array.CreateInstance(elementType, lengths, lowerBounds);

        private object createObjectFromSingleDimensionalArrayProperty(SingleDimensionalArrayProperty property)
        {
            int count = property.Items.Count;
            int[] lengths = new int[] { count };
            int[] lowerBounds = new int[] { property.LowerBound };
            Array array = createArrayInstance(property.ElementType, lengths, lowerBounds);
            for (int i = property.LowerBound; i < (property.LowerBound + count); i++)
            {
                Property property2 = property.Items[i];
                object obj2 = this.CreateObject(property2);
                if (obj2 != null)
                {
                    array.SetValue(obj2, i);
                }
            }
            return array;
        }

        private object createObjectFromDictionaryProperty(DictionaryProperty property)
        {
            object obj2 = CreateInstance(property.Type);
            this.fillProperties(ref obj2, property.Properties);
            MethodInfo method = obj2.GetType().GetMethod("Add");
            if ((method != null) && (method.GetParameters().Length == 2))
            {
                foreach (KeyValueItem item in property.Items)
                {
                    object obj3 = this.CreateObject(item.Key);
                    object obj4 = this.CreateObject(item.Value);
                    try
                    {
                        object[] parameters = new object[] { obj3, obj4 };
                        method.Invoke(obj2, parameters);
                    }
                    catch
                    {
                    }
                }
            }
            return obj2;
        }

        public object CreateInstance(Type type)
        {
            object obj2;
            if (type == null)
            {
                return null;
            }
            try
            {
                obj2 = Activator.CreateInstance(type);
            }
            catch (Exception exception)
            {
                throw new CreatingInstanceException($"Error during creating an object. Please check if the type { type.AssemblyQualifiedName } has public parameterless constructor. Details are in the inner exception.", exception);
            }
            return obj2;
        }

        private void fillProperties(ref object obj, IEnumerable<Property> properties)
        {
            Type type = obj.GetType();
            bool isValueType = type.IsValueType;
            foreach (Property property in properties)
            {
                PropertyInfo info = DXFastProperty.From(type, property.Name);
                if ((info != null) && info.CanWrite)
                {
                    object obj2 = this.CreateObject(property);
                    if (obj2 != null)
                    {
                        DXGetterSetter.SetValue(ref obj, info, obj2);
                    }
                }
            }
        }

        private object createObjectFromCollectionProperty(CollectionProperty property)
        {
            object obj2 = CreateInstance(property.Type);
            this.fillProperties(ref obj2, property.Properties);
            MethodInfo method = obj2.GetType().GetMethod("Add");
            if ((method != null) && (method.GetParameters().Length == 1))
            {
                foreach (Property property2 in property.Items)
                {
                    object obj3 = this.CreateObject(property2);
                    object[] parameters = new object[] { obj3 };
                    method.Invoke(obj2, parameters);
                }
            }
            return obj2;
        }

        private object createObjectFromComplexProperty(ComplexProperty property)
        {
            object obj2 = CreateInstance(property.Type);
            this.fillProperties(ref obj2, property.Properties);
            return obj2;
        }

    }
}
