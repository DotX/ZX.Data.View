﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Ionic.Zip;
using System.IO;
using DXVision.Serialization;
using DXVision.Serialization.Core;
using DXVision.Serialization.Advanced;
using DXVision.Serialization.Advanced.Xml;
using DXVision.Serialization.Advanced.Serializing;
using System.Xml;
using DXVision.Serialization.Deserializing;
using DXVision;

namespace ZX.Data.View
{
    public partial class TestCore
    {
        private static Assembly assembly;

        public XmlPropertySerializer _serializer;
        public XmlPropertyDeserializer _deserializer;


        public TestCore()
        {
            //初始化反序列化解析器
            SharpSerializerXmlSettings settings = new SharpSerializerXmlSettings();
            //this.PropertyProvider.PropertiesToIgnore = settings.AdvancedSettings.PropertiesToIgnore;
            //this.RootName = settings.AdvancedSettings.RootName;
            ISimpleValueConverter valueConverter = settings.AdvancedSettings.SimpleValueConverter ?? new SimpleValueConverter(settings.Culture);
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings() { Encoding = settings.Encoding, Indent = true, OmitXmlDeclaration = true };
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true };
            ITypeNameConverter typeNameConverter = settings.AdvancedSettings.TypeNameConverter ?? new TypeNameConverter(settings.IncludeAssemblyVersionInTypeName, settings.IncludeCultureInTypeName, settings.IncludePublicKeyTokenInTypeName);
            this._reader = new DefaultXmlReader(typeNameConverter, valueConverter, xmlReaderSettings);
            this._writer = new DefaultXmlWriter(typeNameConverter, valueConverter, xmlWriterSettings);
            this._serializer = new XmlPropertySerializer(this._writer);
            this._deserializer = new XmlPropertyDeserializer(this._reader);
        }

        
    }
}
