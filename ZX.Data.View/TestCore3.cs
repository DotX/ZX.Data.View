﻿using DXVision.Serialization;
using DXVision.Serialization.Core;
using DXVision.Serialization.Serializing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ZX.Data.View
{
    //序列化
    public partial class TestCore
    {
        public void Serialize(Property property)
        {
            this.SerializeCore(new PropertyTypeInfo<Property>(property, null));
        }

        protected void SerializeCore(PropertyTypeInfo<Property> property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            NullProperty property2 = property.Property as NullProperty;
            if (property2 != null)
            {
                this.SerializeNullProperty(new PropertyTypeInfo<NullProperty>(property2, property.ExpectedPropertyType, property.ValueType));
            }
            else
            {
                if ((property.ExpectedPropertyType != null) && (property.ExpectedPropertyType == property.ValueType))
                {
                    property.ValueType = null;
                }
                SimpleProperty property3 = property.Property as SimpleProperty;
                if (property3 != null)
                {
                    this.SerializeSimpleProperty(new PropertyTypeInfo<SimpleProperty>(property3, property.ExpectedPropertyType, property.ValueType));
                }
                else
                {
                    MultiDimensionalArrayProperty property4 = property.Property as MultiDimensionalArrayProperty;
                    if (property4 != null)
                    {
                        this.SerializeMultiDimensionalArrayProperty(new PropertyTypeInfo<MultiDimensionalArrayProperty>(property4, property.ExpectedPropertyType, property.ValueType));
                    }
                    else
                    {
                        SingleDimensionalArrayProperty property5 = property.Property as SingleDimensionalArrayProperty;
                        if (property5 != null)
                        {
                            this.SerializeSingleDimensionalArrayProperty(new PropertyTypeInfo<SingleDimensionalArrayProperty>(property5, property.ExpectedPropertyType, property.ValueType));
                        }
                        else
                        {
                            DictionaryProperty property6 = property.Property as DictionaryProperty;
                            if (property6 != null)
                            {
                                this.SerializeDictionaryProperty(new PropertyTypeInfo<DictionaryProperty>(property6, property.ExpectedPropertyType, property.ValueType));
                            }
                            else
                            {
                                CollectionProperty property7 = property.Property as CollectionProperty;
                                if (property7 != null)
                                {
                                    this.SerializeCollectionProperty(new PropertyTypeInfo<CollectionProperty>(property7, property.ExpectedPropertyType, property.ValueType));
                                }
                                else
                                {
                                    ComplexProperty property8 = property.Property as ComplexProperty;
                                    if (property8 == null)
                                    {
                                        throw new InvalidOperationException(string.Format("Unknown Property: {0}", property.Property.GetType()));
                                    }
                                    this.SerializeComplexProperty(new PropertyTypeInfo<ComplexProperty>(property8, property.ExpectedPropertyType, property.ValueType));
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void SerializeNullProperty(PropertyTypeInfo<NullProperty> property)
        {
            this.writeStartProperty("Null", property.Name, property.ValueType);
            this.writeEndProperty();
        }

        protected void SerializeSimpleProperty(PropertyTypeInfo<SimpleProperty> property)
        {
            if (property.Property.Value != null)
            {
                this.writeStartProperty("Simple", property.Name, property.ValueType);
                this._writer.WriteAttribute("value", property.Property.Value);
                this.writeEndProperty();
            }
        }

        protected void SerializeMultiDimensionalArrayProperty(PropertyTypeInfo<MultiDimensionalArrayProperty> property)
        {
            this.writeStartProperty("MultiArray", property.Name, property.ValueType);
            this.writeElementType(property.Property.ElementType);
            this.writeDimensionInfos(property.Property.DimensionInfos);
            this.writeMultiDimensionalArrayItems(property.Property.Items, property.Property.ElementType);
            this.writeEndProperty();
        }

        protected void SerializeSingleDimensionalArrayProperty(PropertyTypeInfo<SingleDimensionalArrayProperty> property)
        {
            this.writeStartProperty("SingleArray", property.Name, property.ValueType);
            this.writeElementType(property.Property.ElementType);
            if (property.Property.LowerBound != 0)
            {
                this._writer.WriteAttribute("lowerBound", property.Property.LowerBound);
            }
            this.writeItems(property.Property.Items, property.Property.ElementType);
            this.writeEndProperty();
        }

        protected void SerializeDictionaryProperty(PropertyTypeInfo<DictionaryProperty> property)
        {
            this.writeStartProperty("Dictionary", property.Name, property.ValueType);
            this.writeKeyType(property.Property.KeyType);
            this.writeValueType(property.Property.ValueType);
            this.writeProperties(property.Property.Properties, property.Property.Type);
            this.writeDictionaryItems(property.Property.Items, property.Property.KeyType, property.Property.ValueType);
            this.writeEndProperty();
        }

        protected void SerializeCollectionProperty(PropertyTypeInfo<CollectionProperty> property)
        {
            var pType = property.Property.Properties.FirstOrDefault(a => a.Name == "oElementType") as SimpleProperty;
            var oType = string.Format("{0}", pType?.Value);

            this.writeStartProperty("Collection", property.Name, property.ValueType);

            if (string.IsNullOrWhiteSpace(oType))
            {
                this.writeElementType(property.Property.ElementType);
            }
            else
            {
                property.Property.Properties.Remove(pType);
                this.writeElementType(oType);
            }

            this.writeProperties(property.Property.Properties, property.Property.Type);
            this.writeItems(property.Property.Items, property.Property.ElementType);
            this.writeEndProperty();
        }

        protected void SerializeComplexProperty(PropertyTypeInfo<ComplexProperty> property)
        {
            var pType = property.Property.Properties.FirstOrDefault(a => a.Name == "oType") as SimpleProperty;
            var oType = string.Format("{0}", pType?.Value);

            if (string.IsNullOrWhiteSpace(oType))
            {
                this.writeStartProperty("Complex", property.Name, property.ValueType);
            }
            else
            {
                property.Property.Properties.Remove(pType);
                this.writeStartProperty("Complex", property.Name, oType);
            }

            this.writeProperties(property.Property.Properties, property.Property.Type);
            this.writeEndProperty();
        }

        private void writeStartProperty(string elementId, string propertyName, Type propertyType)
        {
            this._writer.WriteStartElement(elementId);
            if (!string.IsNullOrEmpty(propertyName))
            {
                this._writer.WriteAttribute("name", propertyName);
            }
            if (propertyType != null && propertyType.FullName != "System.String" && propertyType.FullName != "System.Object")
            {
                this._writer.WriteAttribute("type", propertyType);
            }
        }

        private void writeStartProperty(string elementId, string propertyName, string propertyType)
        {
            this._writer.WriteStartElement(elementId);
            if (!string.IsNullOrEmpty(propertyName))
            {
                this._writer.WriteAttribute("name", propertyName);
            }
            if (!string.IsNullOrEmpty(propertyType))
            {
                this._writer.WriteAttribute("type", propertyType);
            }
        }

        private void writeEndProperty()
        {
            this._writer.WriteEndElement();
        }

        private void writeElementType(Type type)
        {
            if (type != null)
            {
                this._writer.WriteAttribute("elementType", type);
            }
        }

        private void writeElementType(string type)
        {
            if (!string.IsNullOrWhiteSpace(type))
            {
                this._writer.WriteAttribute("elementType", type);
            }
        }

        private void writeDimensionInfos(IEnumerable<DimensionInfo> infos)
        {
            this._writer.WriteStartElement("Dimensions");
            foreach (DimensionInfo info in infos)
            {
                this.writeDimensionInfo(info);
            }
            this._writer.WriteEndElement();
        }

        private void writeMultiDimensionalArrayItems(IEnumerable<MultiDimensionalArrayItem> items, Type defaultItemType)
        {
            this._writer.WriteStartElement("Items");
            foreach (MultiDimensionalArrayItem item in items)
            {
                this.writeMultiDimensionalArrayItem(item, defaultItemType);
            }
            this._writer.WriteEndElement();
        }

        private void writeItems(IEnumerable<Property> properties, Type defaultItemType)
        {
            this._writer.WriteStartElement("Items");
            foreach (Property property in properties)
            {
                this.SerializeCore(new PropertyTypeInfo<Property>(property, defaultItemType));
            }
            this._writer.WriteEndElement();
        }

        private void writeKeyType(Type type)
        {
            if (type != null)
            {
                this._writer.WriteAttribute("keyType", type);
            }
        }

        private void writeValueType(Type type)
        {
            if (type != null)
            {
                this._writer.WriteAttribute("valueType", type);
            }
        }

        private void writeProperties(ICollection<Property> properties, Type ownerType)
        {
            if (properties.Count != 0)
            {
                this._writer.WriteStartElement("Properties");
                foreach (Property property in properties)
                {
                    PropertyInfo info = DXFastProperty.From(ownerType, property.Name);
                    if (info != null)
                    {
                        this.SerializeCore(new PropertyTypeInfo<Property>(property, info.PropertyType));
                    }
                    else
                    {
                        this.SerializeCore(new PropertyTypeInfo<Property>(property, null));
                    }
                }
                this._writer.WriteEndElement();
            }
        }

        private void writeDictionaryItems(IEnumerable<KeyValueItem> items, Type defaultKeyType, Type defaultValueType)
        {
            this._writer.WriteStartElement("Items");
            foreach (KeyValueItem item in items)
            {
                this.writeDictionaryItem(item, defaultKeyType, defaultValueType);
            }
            this._writer.WriteEndElement();
        }

        private void writeDimensionInfo(DimensionInfo info)
        {
            this._writer.WriteStartElement("Dimension");
            if (info.Length != 0)
            {
                this._writer.WriteAttribute("length", info.Length);
            }
            if (info.LowerBound != 0)
            {
                this._writer.WriteAttribute("lowerBound", info.LowerBound);
            }
            this._writer.WriteEndElement();
        }

        private void writeMultiDimensionalArrayItem(MultiDimensionalArrayItem item, Type defaultTypeOfItemValue)
        {
            this._writer.WriteStartElement("Item");
            this._writer.WriteAttribute("indexes", item.Indexes);
            this.SerializeCore(new PropertyTypeInfo<Property>(item.Value, defaultTypeOfItemValue));
            this._writer.WriteEndElement();
        }

        private void writeDictionaryItem(KeyValueItem item, Type defaultKeyType, Type defaultValueType)
        {
            this._writer.WriteStartElement("Item");
            this.SerializeCore(new PropertyTypeInfo<Property>(item.Key, defaultKeyType));
            this.SerializeCore(new PropertyTypeInfo<Property>(item.Value, defaultValueType));
            this._writer.WriteEndElement();
        }



    }
}
