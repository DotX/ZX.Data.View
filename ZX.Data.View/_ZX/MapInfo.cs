﻿using DXVision;
using DXVision.Serialization.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZX.Data.View
{
    ///<summary>地图信息</summary>
    public class MapInfo
    {
        ///<summary>绘制单元大小</summary>
        public int CellSize = 16;

        ///<summary>地图单元数量(纵横)</summary>
        public int NCells;

        ///<summary>地形层</summary>
        public ZXMapLayerTerrainType[,] LayerTerrain;
        ///<summary>对像层</summary>
        public ZXMapLayerObjectType[,] LayerObjects;
        ///<summary>道路层(不使用)</summary>
        public ZXMapLayerRoad[,] LayerRoads;
        ///<summary>僵尸层(生成时使用)</summary>
        public ZXMapLayerZombies[,] LayerZombies;

        ///<summary>地形资源类型</summary>
        public int[,] TerrainResourceType;

        ///<summary>指挥中心位置(生成时使用)</summary>
        public Point CommandCenterCell;

        ///<summary>实体(石油)</summary>
        public List<ComplexProperty> OilSource;
        ///<summary>实体(单位,建筑,僵尸,其它)</summary>
        public List<EntityInfo> EntitySource;

        ///<summary>石油模版</summary>
        public ComplexProperty OilSourceTemplete;

        ///<summary></summary>
        public List<EventInfo> EventList;
    }
}


