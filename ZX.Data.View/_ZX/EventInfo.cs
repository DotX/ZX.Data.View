﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZX.Data.View
{
    public class EventInfo
    {
        ///<summary>id</summary>
        public int id;
        ///<summary>开始时间(小时)</summary>
        public int StartTimeH;
        ///<summary>重复间隔时间(小时)</summary>
        public int RepeatTimeH;
        ///<summary>最高重复次数</summary>
        public int MaxRepetitions;
        ///<summary>提示时间</summary>
        public int TimeToNotifyInAdvanceH;
        ///<summary>显示信息</summary>
        public string Message;
        ///<summary>最终波次</summary>
        public bool FinalSwarm;
        ///<summary>游戏胜利</summary>
        public bool GameWon;
        ///<summary>单位增量(%)</summary>
        public float FactorUnitsNumberPerRepetition;
        ///<summary>最大单位增量(%)</summary>
        public float MaxFactorUnitsNumberPerRepetition;
        ///<summary>生成方位名称</summary>
        public string Generators;


        public UnitCountInfo EntityType1 = new UnitCountInfo();
        public UnitCountInfo EntityType2 = new UnitCountInfo();
        public UnitCountInfo EntityType3 = new UnitCountInfo();
        public UnitCountInfo EntityType4 = new UnitCountInfo();
        public UnitCountInfo EntityType5 = new UnitCountInfo();
        public UnitCountInfo EntityType6 = new UnitCountInfo();
        public UnitCountInfo EntityType7 = new UnitCountInfo();
        public UnitCountInfo EntityType8 = new UnitCountInfo();
        public UnitCountInfo EntityType9 = new UnitCountInfo();
        public UnitCountInfo EntityType10 = new UnitCountInfo();
    }

    public class UnitCountInfo
    {
        public string UnitName;
        public int Min;
        public int Max;

        public static UnitCountInfo Parse(string text)
        {
            int i, i2;

            UnitCountInfo item = new UnitCountInfo();

            i = text.IndexOf("(");

            if (i != -1)
            {
                item.UnitName = text.Substring(0, i).Trim();

                i = text.IndexOf("(", i);
                i2 = text.IndexOf(")", i);

                text = text.Substring(i + 1, i2 - i - 1);

                var ts = text.Split(new char[] { ',' });

                int.TryParse(ts[0], out item.Min);
                int.TryParse(ts[1], out item.Max);
            }
            return item;
        }

        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(this.UnitName))
            {
                return "";
            }
            else
            {
                return string.Format("{0}({1},{2})", this.UnitName, this.Min, this.Max);
            }
        }
    }
}
