﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZX.Data.View
{
    public enum ZXTerrainResourceType : byte
    {
        Earth = 20,
        GenericNotBuildable = 110,
        Gold = 70,
        Grass = 30,
        Iron = 50,
        Mountain = 100,
        None = 0,
        Oil = 60,
        Road = 80,
        Sea = 10,
        Stone = 40,
        Wood = 90
    }

}
