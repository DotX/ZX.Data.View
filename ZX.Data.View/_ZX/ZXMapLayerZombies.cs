﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZX.Data.View
{
    public enum ZXMapLayerZombies
    {
        None,
        Weak1,
        Weak2,
        Weak3,
        Medium1,
        Medium2,
        Medium3,
        Strong1,
        Strong2,
        Strong3,
        Powerful1,
        Powerful2,
        Ultra1
    }
}
