﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZX.Data.View
{
    public enum ZXMapLayerTerrainType
    {
        Earth,
        Water,
        Grass,
        Sky,
        Abyss
    }
}
