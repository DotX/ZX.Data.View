﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Ionic.Zip;
using System.IO;
using DXVision.Serialization;
using DXVision.Serialization.Core;
using DXVision.Serialization.Advanced;
using DXVision.Serialization.Advanced.Xml;
using DXVision.Serialization.Advanced.Serializing;
using System.Xml;
using DXVision.Serialization.Deserializing;
using DXVision;

namespace ZX.Data.View
{
    public partial class TestCore
    {
        private static Assembly assembly;

        private XmlPropertySerializer _serializer;
        private XmlPropertyDeserializer _deserializer;

        static TestCore()
        {
            assembly = Assembly.LoadFrom("TheyAreBillions.exe");
        }

        public void Main()
        {
            var fileName = "E:\\test.zxsav";

            //读取文件, 解压缩
            var file = new ZipFile(fileName);

            var entry = file.Entries.FirstOrDefault<ZipEntry>(ent => ent.FileName == "Data");

            MemoryStream outStream = new MemoryStream();

            MemoryStream stream = new MemoryStream();
            entry.Extract(stream);
            stream.Seek(0L, SeekOrigin.Begin);


            //初始化反序列化解析器
            SharpSerializerXmlSettings settings = new SharpSerializerXmlSettings();
            //this.PropertyProvider.PropertiesToIgnore = settings.AdvancedSettings.PropertiesToIgnore;
            //this.RootName = settings.AdvancedSettings.RootName;
            ISimpleValueConverter valueConverter = settings.AdvancedSettings.SimpleValueConverter ?? new SimpleValueConverter(settings.Culture);
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings() { Encoding = settings.Encoding, Indent = true, OmitXmlDeclaration = true };
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true };
            ITypeNameConverter typeNameConverter = settings.AdvancedSettings.TypeNameConverter ?? new TypeNameConverter(settings.IncludeAssemblyVersionInTypeName, settings.IncludeCultureInTypeName, settings.IncludePublicKeyTokenInTypeName);
            DefaultXmlReader reader = new DefaultXmlReader(typeNameConverter, valueConverter, xmlReaderSettings);
            DefaultXmlWriter write = new DefaultXmlWriter(typeNameConverter, valueConverter, xmlWriterSettings);
            this._serializer = new XmlPropertySerializer(write);
            this._deserializer = new XmlPropertyDeserializer(reader);


            this._deserializer.Open(stream);

            //属性和值
            ComplexProperty property = (ComplexProperty)this._deserializer.Deserialize();

            var sp = property.Properties[0] as SimpleProperty;
            sp.Value = "testppp";


            this._deserializer.Close();


            this._serializer.Open(outStream);
            this._serializer.Serialize(property);
            this._serializer.Close();


            System.IO.File.WriteAllBytes(@"D:\AAAA.ZIPSAV", outStream.GetBuffer());





      





            stream.Close();
            stream.Dispose();
            file.Dispose();
        }






















































    }
}
