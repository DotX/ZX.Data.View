﻿using DXVision.Serialization;
using DXVision.Serialization.Advanced;
using DXVision.Serialization.Advanced.Serializing;
using DXVision.Serialization.Advanced.Xml;
using DXVision.Serialization.Core;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZX.Data.View
{
    public partial class SaveHelper
    {
        //private static Assembly assembly;
        private static DefaultXmlReader reader;
        private static DefaultXmlWriter write;
        private static ISimpleValueConverter valueConverter;

        ///<summary>文件路径</summary>
        public string FilePath;

        ///<summary>序列化器</summary>
        private XmlPropertySerializer _serializer;
        ///<summary>反序列化器</summary>
        private XmlPropertyDeserializer _deserializer;

        ///<summary>根属性</summary>
        public ComplexProperty RootProperty;

        static SaveHelper()
        {
            //assembly = Assembly.LoadFrom("TheyAreBillions.dll");

            //初始化反序列化解析器
            SharpSerializerXmlSettings settings = new SharpSerializerXmlSettings();
            //this.PropertyProvider.PropertiesToIgnore = settings.AdvancedSettings.PropertiesToIgnore;
            //this.RootName = settings.AdvancedSettings.RootName;
            valueConverter = settings.AdvancedSettings.SimpleValueConverter ?? new SimpleValueConverter(settings.Culture);
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings() { Encoding = settings.Encoding, Indent = true, OmitXmlDeclaration = true };
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true };
            ITypeNameConverter typeNameConverter = settings.AdvancedSettings.TypeNameConverter ?? new TypeNameConverter(settings.IncludeAssemblyVersionInTypeName, settings.IncludeCultureInTypeName, settings.IncludePublicKeyTokenInTypeName);
            reader = new DefaultXmlReader(typeNameConverter, valueConverter, xmlReaderSettings);
            write = new DefaultXmlWriter(typeNameConverter, valueConverter, xmlWriterSettings);
        }

        public SaveHelper(string filePath)
        {
            this.FilePath = filePath;
            this._serializer = new XmlPropertySerializer(write);
            this._deserializer = new XmlPropertyDeserializer(reader);
        }

        ///<summary>读取</summary>
        public void Read()
        {
            using (var ms = new MemoryStream())
            {
                //读取文件, 解压缩
                using (var file = new ZipFile(this.FilePath))
                {
                    var entry = file.Entries.FirstOrDefault<ZipEntry>(ent => ent.FileName == "Data");

                    //解压缩
                    entry.Extract(ms);
                    //重新调整流位置
                    ms.Seek(0L, SeekOrigin.Begin);

                    this.RootProperty = (ComplexProperty)this.Deserializer(ms);

                    //反序列化
                    //this._deserializer.Open(ms);
                    //this.RootProperty = (ComplexProperty)this._deserializer.Deserialize();
                    //this._deserializer.Close();
                }
            }
        }

        ///<summary>保存</summary>
        public void Save()
        {
            using (var ms = new MemoryStream())
            {
                //序列化
                this.Serializer(ms, this.RootProperty);

                //this._serializer.Open(ms);
                //this._serializer.Serialize(this.RootProperty);
                //this._serializer.Close();

                //重新调整流位置
                ms.Seek(0L, SeekOrigin.Begin);

                //读取文件
                using (var file = new ZipFile(this.FilePath))
                {
                    var entry = file.Entries.FirstOrDefault<ZipEntry>(ent => ent.FileName == "Data");
                    //删除旧文件
                    file.RemoveEntry(entry);
                    //添加新文件
                    file.AddEntry("Data", ms);
                    file.Save();
                }

                //生成校验文件
                uint c = 0;
                foreach (var i in System.IO.File.ReadAllBytes(this.FilePath))
                {
                    c += i;
                }
                c = c * 0x9D + c;

                var checkPath = System.IO.Path.ChangeExtension(this.FilePath, ".zxcheck");
                System.IO.File.WriteAllText(checkPath, c.ToString());
            }
        }

        public object Deserializer(Stream stm)
        {
            TestCore core = new TestCore();
            core._deserializer.Open(stm);
            var result = (ComplexProperty)core.Deserialize();
            core._deserializer.Close();
            return result;
        }


        public void Serializer(Stream stm, Property p)
        {
            TestCore core = new TestCore();
            core._serializer.Open(stm);
            core.Serialize((Property)p);
            core._serializer.Close();
        }

    }
}
